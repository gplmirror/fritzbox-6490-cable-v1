/*
  GPL LICENSE SUMMARY

  Copyright(c) 2014-2017 Intel Corporation.

  This program is free software; you can redistribute it and/or modify
  it under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
  The full GNU General Public License is included in this distribution
  in the file called LICENSE.GPL.

  Contact Information:
    Intel Corporation
    2200 Mission College Blvd.
    Santa Clara, CA  97052
*/


typedef int (*MCS_START_SESSION)(unsigned int sessionHandle, struct sk_buff* skb);
typedef int (*MCS_DELETE_SESSION)(unsigned int sessionHandle, 
                                     unsigned int sessionPacketsFw,
									 unsigned long long sessionOcttestsFw);

extern MCS_START_SESSION mcs_ctl_start_session_notification_cb;
extern MCS_DELETE_SESSION mcs_ctl_delete_session_notification_cb;

/* Register the start session notification for mcs feature */           
extern int mcs_ctl_register_start_session_notification(MCS_START_SESSION mcs_start_session_notification);

/* UnRegister the start session notification for mcs feature */
extern int mcs_ctl_unregister_start_session_notification(void);

/* Register the delete session notification for mcs feature */
extern int mcs_ctl_register_delete_session_notification(MCS_DELETE_SESSION mcs_delete_session_notification);

/* unRegister the delete session notification for mcs feature */
extern int mcs_ctl_unregister_delete_session_notification(void);

