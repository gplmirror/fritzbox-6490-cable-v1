#ifndef UDMA_SKB_H
#define UDMA_SKB_H

#include <linux/netdevice.h>
#include <linux/udma_api.h>

int udma_register(uint8_t port, struct net_device *netdev,rx_callback_t rx_callback);

#endif 
