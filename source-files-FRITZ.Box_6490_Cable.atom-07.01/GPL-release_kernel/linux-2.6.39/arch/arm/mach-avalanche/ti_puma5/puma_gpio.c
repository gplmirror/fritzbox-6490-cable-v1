/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/spinlock.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <asm/fcntl.h>
#include <asm/ioctl.h>
#include <asm/errno.h>
#include <asm/uaccess.h>
#include <asm/bitops.h>

#include <asm/mach_avm.h>
#include <mach/hw_gpio.h>

static DEFINE_SPINLOCK(puma_gpio_spinlock);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int __init puma_gpio_init(void) {

    printk("[puma_gpio_init]\n");
    spin_lock_init(&puma_gpio_spinlock);
    return 0;
}

arch_initcall(puma_gpio_init);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int puma_gpio_ctrl(unsigned int gpio_pin, enum _hw_gpio_function pin_mode, enum _hw_gpio_direction pin_dir) {
    unsigned long flags;

    spin_lock_irqsave(&puma_gpio_spinlock, flags);
    if(pin_mode == GPIO_PIN) {
        PUMA_GPIO_ENABLE(gpio_pin, PUMA_GPIO_ON);
        if (pin_dir == GPIO_INPUT_PIN)
            PUMA_GPIO_SETDIR(gpio_pin, PUMA_GPIO_AS_INPUT);
        else
            PUMA_GPIO_SETDIR(gpio_pin, PUMA_GPIO_AS_OUTPUT);
    } else {
        PUMA_GPIO_ENABLE(gpio_pin, PUMA_GPIO_ON);
    }
    spin_unlock_irqrestore(&puma_gpio_spinlock, flags);
    return (0);
}
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int puma_gpio_out_bit(unsigned int gpio_pin, int value) {
    unsigned long flags;

    /*--- if((gpio_pin == 29) || (gpio_pin == 31)) { ---*/
        /*--- printk("[puma_gpio_out_bit] gpio=%u value=%u\n", gpio_pin, value); ---*/
    /*--- } ---*/

    if(gpio_pin > 63) {
        printk("[GPIO] invalid gpio pin %d\n", gpio_pin);
        return(-1);
    }
    spin_lock_irqsave(&puma_gpio_spinlock, flags);
    PUMA_GPIO_OUTPUT(gpio_pin, value);
    spin_unlock_irqrestore(&puma_gpio_spinlock, flags);
    return(0);
}
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int puma_gpio_in_bit(unsigned int gpio_pin) {

    /*--- printk("[puma_gpio_in_bit] gpio=%u\n", gpio_pin); ---*/
    if(gpio_pin > 63) {
        printk("[GPIO] invallid gpio pin %d\n", gpio_pin);
        return(-1);
    }
    return PUMA_GPIO_INPUT(gpio_pin);
}
EXPORT_SYMBOL(puma_gpio_ctrl);
EXPORT_SYMBOL(puma_gpio_out_bit);
EXPORT_SYMBOL(puma_gpio_in_bit);
