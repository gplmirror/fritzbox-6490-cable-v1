#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <asm/prom.h>
#include <linux/avm_hw_config.h>


/*------------------------------------------------------------------------------------------*\
 * FRITZ!Box 6360 Cable
\*------------------------------------------------------------------------------------------*/
struct _avm_hw_config avm_hardware_config_hw157[] = {
    { .name = NULL }
};
EXPORT_SYMBOL(avm_hardware_config_hw157);


/*------------------------------------------------------------------------------------------*\
 * FRITZ!Box 6320 Cable
\*------------------------------------------------------------------------------------------*/
struct _avm_hw_config avm_hardware_config_hw176[] = {
    { .name = NULL }
};
EXPORT_SYMBOL(avm_hardware_config_hw176);


/*------------------------------------------------------------------------------------------*\
 * FRITZ!Box 6322 Cable
\*------------------------------------------------------------------------------------------*/
struct _avm_hw_config avm_hardware_config_hw182[] = {
    { .name = NULL }
};
EXPORT_SYMBOL(avm_hardware_config_hw182);


/*------------------------------------------------------------------------------------------*\
 * FRITZ!Box 6340 Cable
\*------------------------------------------------------------------------------------------*/
struct _avm_hw_config avm_hardware_config_hw187[] = {
    { .name = NULL }
};
EXPORT_SYMBOL(avm_hardware_config_hw187);




/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct _avm_hw_config_table avm_hw_config_tables[] = {
    { 157, avm_hardware_config_hw157 }, /*--- FRITZ!Box 6360 Cable ---*/
    { 176, avm_hardware_config_hw176 }, /*--- FRITZ!Box 6320 Cable ---*/
    { 182, avm_hardware_config_hw182 }, /*--- FRITZ!Box 6322 Cable ---*/
    { 187, avm_hardware_config_hw187 }, /*--- FRITZ!Box 6340 Cable ---*/
};
EXPORT_SYMBOL(avm_hw_config_tables);


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct _avm_hw_config *avm_current_hw_config = NULL;
EXPORT_SYMBOL(avm_current_hw_config);

struct _avm_hw_config *avm_get_hw_config_table(void)
{
    unsigned int hwrev, i;
    char *s;

    s = prom_getenv("HWRevision");
    if (s) {
        hwrev = simple_strtoul(s, NULL, 10);
    }     
    if((s == NULL) || (hwrev < 0) || (hwrev > 255)) {
        printk("[%s] error: %s HWRevision detected in environment variables: %s\n", __FUNCTION__, (s ? "invalid" : "no"), s);
        BUG_ON(1);
        return NULL;
    }
    for(i = 0; i < sizeof(avm_hw_config_tables)/sizeof(struct _avm_hw_config *); i++) {
        if(avm_hw_config_tables[i].hwrev == hwrev) 
            return avm_hw_config_tables[i].table;
    }
    printk("[%s] error: No hardware configuration defined for HWRevision %d\n", __FUNCTION__, hwrev);
    BUG_ON(1);
    return NULL;
}
EXPORT_SYMBOL(avm_get_hw_config_table);

