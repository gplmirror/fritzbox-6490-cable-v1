#include <linux/kernel.h>
#include <linux/percpu.h>
#include <linux/avm_power.h>
#include <linux/avm_helpers.h>

#define TEMPERATURE_DEVICE_NAME "/sys/devices/platform/coretemp.0/temp1_input"
#define TEMPERATURE_SENSOR_INDEX 0u

static int cpu_temperature_callback(__maybe_unused void *handle,
				    __maybe_unused void *context, int *value)
{
	char buf[128];
	long temperature;
	int err;

	if (WARN_ON(!value))
		return -EINVAL;

	err = avm_read_from_file(TEMPERATURE_DEVICE_NAME, buf, sizeof(buf));
	if (err <= 0)
		return err;

	err = kstrtol(buf, 10, &temperature);
	if (err)
		return err;

	*value = temperature / 100;
	return 0;
}

static int __init cpu_temperature_init(void)
{
	if (!TemperaturSensorRegister("CPU", cpu_temperature_callback,
				      TEMPERATURE_SENSOR_INDEX)) {
		pr_err("Could not register CPU Temperature sensor to AVM Power driver.\n");
	}
	return 0;
}

arch_initcall(cpu_temperature_init);
