#
# ISDN device configuration
#

menuconfig ISDN
	bool "ISDN support"
	depends on NET
	depends on !S390
	---help---
	  ISDN ("Integrated Services Digital Network", called RNIS in France)
	  is a fully digital telephone service that can be used for voice and
	  data connections.  If your computer is equipped with an ISDN
	  adapter you can use it to connect to your Internet service provider
	  (with SLIP or PPP) faster than via a conventional telephone modem
	  (though still much slower than with DSL) or to make and accept
	  voice calls (eg. turning your PC into a software answering machine
	  or PABX).

	  Select this option if you want your kernel to support ISDN.

if ISDN

menuconfig ISDN_I4L
	tristate "Old ISDN4Linux (deprecated)"
	---help---
	  This driver allows you to use an ISDN adapter for networking
	  connections and as dialin/out device.  The isdn-tty's have a built
	  in AT-compatible modem emulator.  Network devices support autodial,
	  channel-bundling, callback and caller-authentication without having
	  a daemon running.  A reduced T.70 protocol is supported with tty's
	  suitable for German BTX.  On D-Channel, the protocols EDSS1
	  (Euro-ISDN) and 1TR6 (German style) are supported.  See
	  <file:Documentation/isdn/README> for more information.

	  ISDN support in the linux kernel is moving towards a new API,
	  called CAPI (Common ISDN Application Programming Interface).
	  Therefore the old ISDN4Linux layer will eventually become obsolete.
	  It is still available, though, for use with adapters that are not
	  supported by the new CAPI subsystem yet.

source "drivers/isdn/i4l/Kconfig"

menuconfig ISDN_CAPI
	tristate "CAPI 2.0 subsystem"
	help
	  This provides CAPI (the Common ISDN Application Programming
	  Interface) Version 2.0, a standard making it easy for programs to
	  access ISDN hardware in a device independent way. (For details see
	  <http://www.capi.org/>.)  CAPI supports making and accepting voice
	  and data connections, controlling call options and protocols,
	  as well as ISDN supplementary services like call forwarding or
	  three-party conferences (if supported by the specific hardware
	  driver).

	  Select this option and the appropriate hardware driver below if
	  you have an ISDN adapter supported by the CAPI subsystem.

if ISDN_CAPI

config CAPI_OSLIB
	tristate "split CAPI2.0 oslib"
	depends on ISDN
	help
        split oslib for fbox isdn drivers

config CAPI_OSLIB_DEBUG
    bool "capi oslib debug version"
    depends on CAPI_OSLIB
	help
        debug version of oslib 

config CAPI_CODEC
    tristate "capi driver codec support"
    depends on ISDN

config CAPI_CODEC_SPEEX
    bool "capi driver codec SPEEX"
    depends on CAPI_CODEC

config CAPI_CODEC_ILBC
    bool "capi driver codec ILBC"
    depends on CAPI_CODEC

config CAPI_CODEC_G729
    bool "capi driver codec G729"
    depends on CAPI_CODEC

config CAPI_CODEC_G711
    bool "capi driver codec G711"
    depends on CAPI_CODEC

config CAPI_CODEC_G726
    bool "capi driver codec G726"
    depends on CAPI_CODEC

config CAPI_CODEC_G722
    bool "capi driver codec G722"
    depends on CAPI_CODEC

config CAPI_CODEC_VAD
    bool "capi driver codec VAD"
    depends on CAPI_CODEC

config CAPI_CODEC_CNG
    bool "capi driver codec CNG (comfort noise generator)"
    depends on CAPI_CODEC

config CAPI_CODEC_FAX
    bool "capi driver codec FAX (T30/T38)"
    depends on CAPI_CODEC

config AVM_DECT
	tristate "AVM DECT Stack"

config AVM_DECT_DEBUG
    bool "AVM DECT debug version"
    depends on AVM_DECT

source "drivers/isdn/capi/Kconfig"

source "drivers/isdn/hardware/Kconfig"

endif # ISDN_CAPI

source "drivers/isdn/gigaset/Kconfig"

source "drivers/isdn/hysdn/Kconfig"

source "drivers/isdn/mISDN/Kconfig"

config ISDN_HDLC
	tristate
	select CRC_CCITT
	select BITREVERSE

endif # ISDN
