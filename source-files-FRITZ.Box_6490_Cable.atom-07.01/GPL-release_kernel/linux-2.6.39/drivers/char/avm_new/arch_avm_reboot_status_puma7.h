#ifndef __arch_avm_reboot_status_puma7_h__
#define __arch_avm_reboot_status_puma7_h__

#define _repeat3concat(a,b) a a a b

#define UPDATE_REBOOT_STATUS_TEXT \
	_repeat3concat("(c) AVM 2016, Reboot Status is: Firmware-Update", "")
#define NMI_REBOOT_STATUS_TEXT \
        _repeat3concat("(c) AVM 2016, Reboot Status is: NMI-Watchdog", "")
#define SOFTWATCHDOG_REBOOT_STATUS_TEXT \
	_repeat3concat("(c) AVM 2016, Reboot Status is: Software-Watchdog", "")
#define POWERON_REBOOT_STATUS_TEXT \
	_repeat3concat("(c) AVM 2016, Reboot Status is: Power-On-Reboot", "")
#define TEMP_REBOOT_STATUS_TEXT \
        _repeat3concat("(c) AVM 2016, Reboot Status is: Temperature-Reboot", "")
#define SOFT_REBOOT_STATUS_TEXT_PANIC \
	_repeat3concat("(c) AVM 2016, Reboot Status is: Software-Reboot", \
		       "\0(PANIC)")
#define SOFT_REBOOT_STATUS_TEXT_OOM \
	_repeat3concat("(c) AVM 2016, Reboot Status is: Software-Reboot", \
		       "\0(OOM)")
#define SOFT_REBOOT_STATUS_TEXT_OOPS \
	_repeat3concat("(c) AVM 2016, Reboot Status is: Software-Reboot", \
		       "\0(OOPS)")
#define SOFT_REBOOT_STATUS_TEXT_UPDATE \
	_repeat3concat("(c) AVM 2016, Reboot Status is: Software-Reboot", \
		       "\0(RESET-FOR-UPDATE)")

/*--- Achtung! Untermenge von obigen Einträgen: ---*/
#define SOFT_REBOOT_STATUS_TEXT \
        _repeat3concat("(c) AVM 2016, Reboot Status is: Software-Reboot", "")

static inline char *arch_get_mailbox(void) {
	return NULL;
}

#endif

/* vim: set ts=8 sw=8 noet cino=>8\:0l1(0: */
