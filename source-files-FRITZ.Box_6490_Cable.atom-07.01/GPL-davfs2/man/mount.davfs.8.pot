# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2008-05-12 21:41+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: ENCODING"

# type: TH
#: davfs2.conf.5:1 mount.davfs.8:1 umount.davfs.8:1
#, no-wrap
msgid "@PACKAGE_STRING@"
msgstr ""

# type: SH
#: davfs2.conf.5:4 mount.davfs.8:3 umount.davfs.8:3
#, no-wrap
msgid "NAME"
msgstr ""

# type: SH
#: davfs2.conf.5:9 mount.davfs.8:22 umount.davfs.8:20
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

# type: SH
#: davfs2.conf.5:502 mount.davfs.8:545 umount.davfs.8:79
#, no-wrap
msgid "AUTHORS"
msgstr ""

# type: SH
#: davfs2.conf.5:508 mount.davfs.8:562 umount.davfs.8:84
#, no-wrap
msgid "DAVFS2 HOME"
msgstr ""

# type: Plain text
#: davfs2.conf.5:511 mount.davfs.8:565 umount.davfs.8:87
msgid "http://dav.sourceforge.net/"
msgstr ""

# type: SH
#: davfs2.conf.5:513 mount.davfs.8:567 umount.davfs.8:89
#, no-wrap
msgid "SEE ALSO"
msgstr ""

# type: TH
#: mount.davfs.8:1
#, no-wrap
msgid "@PROGRAM_NAME@"
msgstr ""

# type: TH
#: mount.davfs.8:1
#, no-wrap
msgid "2007-11-04"
msgstr ""

# type: Plain text
#: mount.davfs.8:6
msgid "@PROGRAM_NAME@ - Mount a WebDAV resource in a directory"
msgstr ""

# type: SH
#: mount.davfs.8:8 umount.davfs.8:8
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

# type: Plain text
#: mount.davfs.8:11
msgid "B<@PROGRAM_NAME@ [-h | --help] [-V | --version]>"
msgstr ""

# type: Plain text
#: mount.davfs.8:13
msgid "B<mount {>I<dir>B< | >I<webdavserver>B<}>"
msgstr ""

# type: SH
#: mount.davfs.8:15 umount.davfs.8:15
#, no-wrap
msgid "SYNOPSIS (root only)"
msgstr ""

# type: Plain text
#: mount.davfs.8:18
msgid "B<mount -t davfs [-o >I<option>B<[,...]]>I< webdavserver dir>"
msgstr ""

# type: Plain text
#: mount.davfs.8:20
msgid "B<@PROGRAM_NAME@ [-o >I<option>B<[,...]]>I< webdavserver dir>"
msgstr ""

# type: Plain text
#: mount.davfs.8:31
msgid ""
"B<@PROGRAM_NAME@> allows you to mount the WebDAV resource identified by "
"I<webdavserver> into the local filesystem at I<dir.> WebDAV is an extension "
"to HTTP that allows remote, collaborative authoring of Web resources, "
"defined in RFC 4918.  B<@PROGRAM_NAME@> is part of B<@PACKAGE@>."
msgstr ""

# type: Plain text
#: mount.davfs.8:38
msgid ""
"B<@PACKAGE@> allows documents on a remote Web server to be edited using "
"standard applications. For example, a remote Web site could be updated in-"
"place using the same development tools that initially created the site.  Or "
"you may use a WebDAV resource for documents you want to access and edited "
"from different locations."
msgstr ""

# type: Plain text
#: mount.davfs.8:45
msgid ""
"B<@PACKAGE@> supports B<TLS/SSL> (if the neon library supports it) and "
"proxies. B<@PROGRAM_NAME@> runs as a daemon in userspace. It integrates into "
"the virtual file system by either the coda or the fuse kernel files system.  "
"Currently CODA_KERNEL_VERSION 2, CODA_KERNEL_VERSION 3, FUSE_KERNEL_VERSION "
"5 and FUSE_KERNEL_VERSION 7 are supported."
msgstr ""

# type: Plain text
#: mount.davfs.8:50
msgid ""
"B<@PROGRAM_NAME@> is usually invoked by the B<mount>(8) command when using "
"the I<-t davfs> option. After mounting it runs as a daemon. To unmount the "
"B<umount>(8) command is used."
msgstr ""

# type: Plain text
#: mount.davfs.8:58
msgid ""
"I<webdavserver> must be a complete url, including scheme, fully qualified "
"domain name and path. Scheme may be B<http> or B<https>. If the path "
"contains spaces or other characters, that might be interpreted by the shell, "
"the url must be enclosed in double quotes (e.g. I<\"http://foo.bar/name with "
"spaces\">). See B<URLS AND MOUNT POINTS WITH SPACES>."
msgstr ""

# type: Plain text
#: mount.davfs.8:62 umount.davfs.8:30
msgid ""
"I<dir> is the mountpoint where the WebDAV resource is mounted on.  It may be "
"an absolute or relative path."
msgstr ""

# type: Plain text
#: mount.davfs.8:67
msgid ""
"I<fstab> may be used to define mounts and mount options as usual. In place "
"of the device the url of the WebDAV server must be given. There must not be "
"more than one entry in I<fstab> for every mountpoint."
msgstr ""

# type: SH
#: mount.davfs.8:69 umount.davfs.8:51
#, no-wrap
msgid "OPTIONS"
msgstr ""

# type: TP
#: mount.davfs.8:71 umount.davfs.8:53
#, no-wrap
msgid "B<-V --version>"
msgstr ""

# type: Plain text
#: mount.davfs.8:74 umount.davfs.8:56
msgid "Output version."
msgstr ""

# type: TP
#: mount.davfs.8:75 umount.davfs.8:57
#, no-wrap
msgid "B<-h --help>"
msgstr ""

# type: Plain text
#: mount.davfs.8:78 umount.davfs.8:60
msgid "Print a help message."
msgstr ""

# type: TP
#: mount.davfs.8:79
#, no-wrap
msgid "B<-o>"
msgstr ""

# type: Plain text
#: mount.davfs.8:83
msgid ""
"A comma-separated list defines mount options to be used. Available options "
"are:"
msgstr ""

# type: TP
#: mount.davfs.8:85
#, no-wrap
msgid "B<[no]askauth>"
msgstr ""

# type: Plain text
#: mount.davfs.8:89
msgid ""
"(Do not) ask interactivly for creditentials for the WebDAV server or the "
"proxy if they are not found in the secrets file."
msgstr ""

# type: Plain text
#: mount.davfs.8:91
msgid "Default: B<askauth>."
msgstr ""

# type: Plain text
#: mount.davfs.8:95
msgid ""
"B<Deprecated:> This option may be removed in future versions. Use option "
"B<ask_auth> in I<@SYS_CONF_DIR@/@CONFIGFILE@> or I<~/.@PACKAGE@/"
"@CONFIGFILE@> instead."
msgstr ""

# type: TP
#: mount.davfs.8:96
#, no-wrap
msgid "B<[no]auto>"
msgstr ""

# type: Plain text
#: mount.davfs.8:99
msgid "Can (not) be mounted with B<mount -a>."
msgstr ""

# type: Plain text
#: mount.davfs.8:101
msgid "Default: B<auto>."
msgstr ""

# type: TP
#: mount.davfs.8:102
#, no-wrap
msgid "B<conf=>I<absolute path>"
msgstr ""

# type: Plain text
#: mount.davfs.8:107
msgid ""
"An alternative user configuration file. This option is intended for cases "
"where the default user configuration file in the users home directory can "
"not be used."
msgstr ""

# type: Plain text
#: mount.davfs.8:109
msgid "Default: I<~/.@PACKAGE@/@CONFIGFILE@>"
msgstr ""

# type: TP
#: mount.davfs.8:110
#, no-wrap
msgid "B<[no]dev>"
msgstr ""

# type: Plain text
#: mount.davfs.8:115
msgid ""
"(Do not) interpret character or block special devices on the file system.  "
"This option is only included for compatibility with the B<mount>(8)  "
"program. It will allways be set to B<nodev>"
msgstr ""

# type: TP
#: mount.davfs.8:116
#, no-wrap
msgid "B<dir_mode=>I<mode>"
msgstr ""

# type: Plain text
#: mount.davfs.8:120
msgid ""
"The default mode bits for directories in the mounted file system. Value "
"given in octal. s-bits for user and group are allways silently ignored."
msgstr ""

# type: Plain text
#: mount.davfs.8:123
msgid ""
"Default: calculated from the umask of the mounting user; an x-bit is "
"associated to every r-bit in u-g-o."
msgstr ""

# type: TP
#: mount.davfs.8:124
#, no-wrap
msgid "B<[no]exec>"
msgstr ""

# type: Plain text
#: mount.davfs.8:127
msgid "(Do not) allow execution of any binaries on the mounted file system."
msgstr ""

# type: Plain text
#: mount.davfs.8:130
msgid ""
"Default: B<exec>. (When mounting as an ordinary user, the B<mount>(8)  "
"program will set the default to B<noexec>.)"
msgstr ""

# type: TP
#: mount.davfs.8:131
#, no-wrap
msgid "B<file_mode=>I<mode>"
msgstr ""

# type: Plain text
#: mount.davfs.8:135
msgid ""
"The default mode bits for files in the mounted file system. Value given in "
"octal. s-bits for user and group are allways silently ignored."
msgstr ""

# type: Plain text
#: mount.davfs.8:138
msgid ""
"Default: calculated from the umask of the mounting user; no x-bits are set "
"for files."
msgstr ""

# type: TP
#: mount.davfs.8:139
#, no-wrap
msgid "B<gid=>I<group>"
msgstr ""

# type: Plain text
#: mount.davfs.8:143
msgid ""
"The group the mounted file system belongs to. It may be a numeric ID or a "
"group name. The mounting user, if not root, must be member of this group."
msgstr ""

# type: Plain text
#: mount.davfs.8:145
msgid "Default: the primary group of the mounting user."
msgstr ""

# type: TP
#: mount.davfs.8:146
#, no-wrap
msgid "B<[no]locks>"
msgstr ""

# type: Plain text
#: mount.davfs.8:149
msgid "(Do not) lock files on the WebDAV server."
msgstr ""

# type: Plain text
#: mount.davfs.8:151
msgid "Default: B<locks>."
msgstr ""

# type: Plain text
#: mount.davfs.8:155
msgid ""
"B<Deprecated:> This option may be removed in future versions. Use option "
"B<use_locks> in I<@SYS_CONF_DIR@/@CONFIGFILE@> or I<~/.@PACKAGE@/"
"@CONFIGFILE@> instead."
msgstr ""

# type: TP
#: mount.davfs.8:156
#, no-wrap
msgid "B<[no]_netdev>"
msgstr ""

# type: Plain text
#: mount.davfs.8:161
msgid ""
"The file system needs a (no) network connection for operation. This "
"information allows the operating system to handle the file system properly "
"at system start and when the network is shut down."
msgstr ""

# type: Plain text
#: mount.davfs.8:163
msgid "Default: B<_netdev>"
msgstr ""

# type: TP
#: mount.davfs.8:164
#, no-wrap
msgid "B<ro>"
msgstr ""

# type: Plain text
#: mount.davfs.8:167
msgid "Mount the file system read-only."
msgstr ""

# type: Plain text
#: mount.davfs.8:169 mount.davfs.8:175
msgid "Default: B<rw>."
msgstr ""

# type: TP
#: mount.davfs.8:170
#, no-wrap
msgid "B<rw>"
msgstr ""

# type: Plain text
#: mount.davfs.8:173
msgid "Mount the file system read-write."
msgstr ""

# type: TP
#: mount.davfs.8:176
#, no-wrap
msgid "B<[no]suid>"
msgstr ""

# type: Plain text
#: mount.davfs.8:181
msgid ""
"Do not allow set-user-identifier or set-group-identifier bits to take "
"effect.  This option is only included for compatibility with the mount "
"program. It will allways be set to B<nosuid>."
msgstr ""

# type: TP
#: mount.davfs.8:182
#, no-wrap
msgid "B<[no]user>"
msgstr ""

# type: Plain text
#: mount.davfs.8:189
msgid ""
"(Do not) allow an ordinary user to mount the file system. The name of the "
"mounting user is written to I<mtab> so that he can unmount the file system "
"again. Option B<user> implies the options B<noexec>, B<nosuid> and B<nodev> "
"(unless overridden by subsequent options). This option makes only sense when "
"set in I<fstab>."
msgstr ""

# type: Plain text
#: mount.davfs.8:191
msgid "Default: ordinary users are not allowed to mount."
msgstr ""

# type: TP
#: mount.davfs.8:192
#, no-wrap
msgid "B<[no]useproxy>"
msgstr ""

# type: Plain text
#: mount.davfs.8:195
msgid "(Do not) use a proxy."
msgstr ""

# type: Plain text
#: mount.davfs.8:197
msgid "Default: B<useproxy>, if a proxy is specified."
msgstr ""

# type: Plain text
#: mount.davfs.8:200
msgid ""
"B<Deprecated:> This option may be removed in future versions. Use option "
"B<use_proxy> in I<@SYS_CONF_DIR@/@CONFIGFILE@> instead."
msgstr ""

# type: TP
#: mount.davfs.8:201
#, no-wrap
msgid "B<uid=>I<user>"
msgstr ""

# type: Plain text
#: mount.davfs.8:205
msgid ""
"The owner of the mounted file system. It may be a numeric ID or a user "
"name.  Only when mounted by root, this may be different from the mounting "
"user."
msgstr ""

# type: Plain text
#: mount.davfs.8:207
msgid "Default: ID of the mounting user."
msgstr ""

# type: SH
#: mount.davfs.8:210
#, no-wrap
msgid "SECURITY POLICY"
msgstr ""

# type: Plain text
#: mount.davfs.8:215
msgid ""
"B<@PROGRAM_NAME@> needs root privileges for mounting. But running a daemon, "
"that is connected to the internet, with root privileges is a security risk. "
"So B<@PROGRAM_NAME@> will change its uid and gid when entering daemon mode."
msgstr ""

# type: Plain text
#: mount.davfs.8:220
msgid ""
"When invoked by root B<@PROGRAM_NAME@> will run as user B<@USER@> and group "
"B<@GROUP@>. This may be changed in I<@SYS_CONF_DIR@/@CONFIGFILE@>."
msgstr ""

# type: Plain text
#: mount.davfs.8:224
msgid ""
"When invoked by an ordinary user it will run with the id of this user and "
"with group B<@GROUP@>."
msgstr ""

# type: Plain text
#: mount.davfs.8:230
msgid ""
"As the file system may be mounted over an insecure internet connection, this "
"increases the risk that malicious content may be included in the file "
"system. So B<@PROGRAM_NAME@> is slightly more restrictive than B<mount>(8)."
msgstr ""

# type: Plain text
#: mount.davfs.8:235
msgid ""
"Options B<nosuid> and B<nodev> will always be set; even root can not change "
"this."
msgstr ""

# type: Plain text
#: mount.davfs.8:239
msgid ""
"For ordinary users to be able to mount, they must be member of group "
"B<@GROUP@> and there must be an entry in I<fstab>."
msgstr ""

# type: Plain text
#: mount.davfs.8:243
msgid ""
"When mounted by an ordinary user, the mount point must not lie within the "
"home directory of another user."
msgstr ""

# type: Plain text
#: mount.davfs.8:248
msgid ""
"If in I<fstab> option B<uid> and/or B<gid> are given, an ordinary user can "
"only mount, if her uid is the one given in option B<uid> and he belongs to "
"the group given in option B<gid>."
msgstr ""

# type: Plain text
#: mount.davfs.8:256
msgid ""
"B<WARNING:> If root allows an ordinary user to mount a file system (using "
"I<fstab>) this includes the permission to read the associated B<credentials> "
"from I<@SYS_CONF_DIR@/@SECRETSFILE@> as well as the B<private key> of the "
"associated B<client certificate> and the mounting user may get access to "
"this information. You should only do this, if you might as well give this "
"information to the user directly."
msgstr ""

# type: SH
#: mount.davfs.8:257
#, no-wrap
msgid "URLS AND MOUNT POINTS WITH SPACES"
msgstr ""

# type: Plain text
#: mount.davfs.8:262
msgid ""
"Special characters like spaces in pathnames are a mess. They are interpreted "
"differently by different programs and protocols, and there are different "
"rules for escaping."
msgstr ""

# type: Plain text
#: mount.davfs.8:268
msgid ""
"In I<fstab> spaces must be replaced by a three digit octal escape sequence. "
"Write I<http://foo.bar/path\\(rs040with\\(rs040spaces> instead of I<http://"
"foo.bar/path with spaces>. It might also be necessary to replace the '#'-"
"character by \\(rs043."
msgstr ""

# type: Plain text
#: mount.davfs.8:272
msgid ""
"For the I<@CONFIGFILE@> and the I<@SECRETSFILE@> files please see the escape "
"and quotation rules described in the B<@CONFIGFILE@>(5) man page."
msgstr ""

# type: Plain text
#: mount.davfs.8:275
msgid "On I<command line> you must obey the escaping rules of the shell."
msgstr ""

# type: Plain text
#: mount.davfs.8:281
msgid ""
"After escaping and quotation have been removed by the respective program, "
"the url and mount point must resolve to exactly the same string, whether "
"they are taken from I<fstab>, I<@CONFIGFILE@>, I<@SECRETSFILE@> or the "
"command line."
msgstr ""

# type: SH
#: mount.davfs.8:283
#, no-wrap
msgid "CACHING"
msgstr ""

# type: Plain text
#: mount.davfs.8:288
msgid ""
"B<@PROGRAM_NAME@> tries to reduce HTTP-trafic by caching and reusing data.  "
"Information about directories and files are held in memory, while downloaded "
"files are cached on disk."
msgstr ""

# type: Plain text
#: mount.davfs.8:296
msgid ""
"B<@PROGRAM_NAME@> will consider cached information about directories and "
"file attributes valid for a configurable time and look up this information "
"on the server only after this time has expired (or there is other evidence "
"that this information is stale). So if somebody else creates or deletes "
"files on the server it may take some time before the local file system "
"reflects this."
msgstr ""

# type: Plain text
#: mount.davfs.8:302
msgid ""
"This will not affect the content of files and directory listings. Whenever a "
"file is opened, the server is looked up for a newer version of the file.  "
"Please consult the manual B<@CONFIGFILE@>(5) to see how can you configure "
"this according your needs."
msgstr ""

# type: SH
#: mount.davfs.8:304
#, no-wrap
msgid "LOCKS, LOST UPDATE PROBLEM AND BACKUP FILES"
msgstr ""

# type: Plain text
#: mount.davfs.8:309
msgid ""
"WebDAV introduced locks and B<@PROGRAM_NAME@> uses them by default. This "
"will in most cases prevent two people from changing the same file in "
"parallel. But not allways:"
msgstr ""

# type: Plain text
#: mount.davfs.8:314
msgid ""
"You might have disabled locks in I<@SYS_CONF_DIR@/@CONFIGFILE@> or I<~/."
"@PACKAGE@/@CONFIGFILE@>."
msgstr ""

# type: Plain text
#: mount.davfs.8:317
msgid "The server might not support locks (they are not mandatory)."
msgstr ""

# type: Plain text
#: mount.davfs.8:321
msgid ""
"A bad connection might prevent B<@PROGRAM_NAME@> from refreshing the lock in "
"time."
msgstr ""

# type: Plain text
#: mount.davfs.8:325
msgid ""
"Another WebDAV-client might use your lock (that is not too difficult and "
"might even happen without intention)."
msgstr ""

# type: Plain text
#: mount.davfs.8:334
msgid ""
"B<@PROGRAM_NAME@> will therefore allways check if the file has been changed "
"on the the server before it uploads a new version. Unfortunately it has to "
"do this in two separate HTTP requests, as not all servers support "
"conditional PUT. If it finds it impossible to upload the locally changed "
"file, it will store it in the local backup direcotry I<lost+found>. You "
"should check this directory from time to time and decide what to do with "
"this files."
msgstr ""

# type: Plain text
#: mount.davfs.8:342
msgid ""
"Sometimes locks held by some client on the server will not be released. "
"Maybe the client crashes or the network connection fails. When "
"B<@PROGRAM_NAME@> finds a file locked on the server, it will check whether "
"the lock is held by B<@PROGRAM_NAME@> and the current user, and if so tries "
"to reuse and release it. But this will not allways succeed. So servers "
"should automatically release locks after some time, when they are not "
"refreshed by the client."
msgstr ""

# type: Plain text
#: mount.davfs.8:349
msgid ""
"WebDAV allows to lock files that don't exist (to protect the name when a "
"client intends to create a new file). This locks will be displayed as files "
"with size 0 and last modified date of 1970-01-01. If this locks are not "
"released properly B<@PROGRAM_NAME@> may not be able to access this files. "
"You can use B<cadaver>(1) E<lt>I<http://www.webdav.org/cadaver/>E<gt> to "
"remove this locks."
msgstr ""

# type: SH
#: mount.davfs.8:351
#, no-wrap
msgid "FILE OWNER AND PERMISSIONS"
msgstr ""

# type: Plain text
#: mount.davfs.8:357
msgid ""
"B<@PACKAGE@> implements Unix permissions for access control. But changing "
"owner and permissions of a file is only B<local>. It is intended as a means "
"for the owner of the file system, to controll whether other local users may "
"acces this file system."
msgstr ""

# type: Plain text
#: mount.davfs.8:362
msgid ""
"The server does not know about this. From the servers point of view there is "
"just one user (identified by the credentials) connected. Another WebDAV-"
"client, connected to the same server, is not affected by this local changes."
msgstr ""

# type: Plain text
#: mount.davfs.8:368
msgid ""
"There is one exeption: The B<execute bit> on files is stored as a property "
"on the sever. You may think of this property as an information about the "
"type of file rather than a permission. Whether the file is executable on the "
"local system is still controlled by mount options and local permissions."
msgstr ""

# type: Plain text
#: mount.davfs.8:374
msgid ""
"When the file system is unmounted, attributes of cached files (including "
"owner and permissions) are stored in cache, as well as the attributs of the "
"direcotries they are in. But there is no information stored about "
"directories that do not contain cached files."
msgstr ""

# type: SH
#: mount.davfs.8:376 umount.davfs.8:67
#, no-wrap
msgid "FILES"
msgstr ""

# type: TP
#: mount.davfs.8:378
#, no-wrap
msgid "I<@SYS_CONF_DIR@/@CONFIGFILE@>"
msgstr ""

# type: Plain text
#: mount.davfs.8:381
msgid "System wide configuration file."
msgstr ""

# type: TP
#: mount.davfs.8:382
#, no-wrap
msgid "I<~/.@PACKAGE@/@CONFIGFILE@>"
msgstr ""

# type: Plain text
#: mount.davfs.8:387
msgid ""
"Configuration file in the users home directory.The user configuration takes "
"precedence over the system wide configuration. If it does not exist, "
"B<@PROGRAM_NAME@> will will create a template file."
msgstr ""

# type: TP
#: mount.davfs.8:388
#, no-wrap
msgid "I<@SYS_CONF_DIR@/@SECRETSFILE@>"
msgstr ""

# type: Plain text
#: mount.davfs.8:393
msgid ""
"Holds the credentials for WebDAV servers and the proxy, as well as "
"decryption passwords for client certificates. The file must be read-writable "
"by root only."
msgstr ""

# type: TP
#: mount.davfs.8:394
#, no-wrap
msgid "I<~/.@PACKAGE@/@SECRETSFILE@>"
msgstr ""

# type: Plain text
#: mount.davfs.8:404
msgid ""
"Holds credentials for WebDAV servers and proxy, as well as decryption "
"passwords for client certificates. The file must be read-writable by the "
"owner only. Credentials are first looked up in the home directory of the "
"mounting user. If not found there the system wide secrets file is consulted. "
"If no creditentials and passwords are found they are asked from the user "
"interactively (if not disabled). If the file does not exist, "
"B<@PROGRAM_NAME@> will will create a template file."
msgstr ""

# type: TP
#: mount.davfs.8:405
#, no-wrap
msgid "I<@SYS_CONF_DIR@/@CERTS_DIR@>"
msgstr ""

# type: Plain text
#: mount.davfs.8:412
msgid ""
"You may store trusted server certificates here, that can not be verified by "
"use of the system wide CA-Certificates. This is useful when your server uses "
"a selfmade certificate. You must configure the B<servercert> option in "
"I<@SYS_CONF_DIR@/@CONFIGFILE@> or I<~/.@PACKAGE@/@CONFIGFILE@> to use it. "
"Certificates must be in PEM format."
msgstr ""

# type: Plain text
#: mount.davfs.8:414 mount.davfs.8:423
msgid "Be sure to verify the certificate."
msgstr ""

# type: TP
#: mount.davfs.8:415
#, no-wrap
msgid "I<~/.@PACKAGE@/@CERTS_DIR@>"
msgstr ""

# type: Plain text
#: mount.davfs.8:421
msgid ""
"You may store trusted server certificates here, that can not be verified by "
"use of the system wide CA-Certificates. This is useful when your server uses "
"a selfmade certificate. You must configure the B<servercert> option in I<~/."
"@PACKAGE@/@CONFIGFILE@> to use it. Certificates must be in PEM format."
msgstr ""

# type: TP
#: mount.davfs.8:424
#, no-wrap
msgid "I<@SYS_CONF_DIR@/@CERTS_DIR@/@CLICERTS_DIR@>"
msgstr ""

# type: Plain text
#: mount.davfs.8:430
msgid ""
"To store client certificates. Certificates must be in PKCS#12 format. You "
"must configure the B<clientcert> option in I<@SYS_CONF_DIR@/@CONFIGFILE@> or "
"I<~/.@PACKAGE@/@CONFIGFILE@> to use it. This directory must be rwx by root "
"only."
msgstr ""

# type: TP
#: mount.davfs.8:431
#, no-wrap
msgid "I<~/.@PACKAGE@/@CERTS_DIR@/@CLICERTS_DIR@>"
msgstr ""

# type: Plain text
#: mount.davfs.8:436
msgid ""
"To store client certificates. Certificates must be in PKCS#12 format. You "
"must configure the B<clientcert> option in I<~/.@PACKAGE@/@CONFIGFILE@> to "
"use it. This directory must be rwx by the owner only."
msgstr ""

# type: TP
#: mount.davfs.8:437 umount.davfs.8:69
#, no-wrap
msgid "I<@SYS_RUN@>"
msgstr ""

# type: Plain text
#: mount.davfs.8:443
msgid ""
"PID-files of running mount.davfs processes are stored there. This directory "
"must belong to group B<@USER@> with write permissions for the group and the "
"sticky-bit set (mode 1775). The PID-files are named after the mount point of "
"the file system."
msgstr ""

# type: TP
#: mount.davfs.8:444
#, no-wrap
msgid "I<@SYS_CACHE_DIR@>"
msgstr ""

# type: Plain text
#: mount.davfs.8:451
msgid ""
"System wide directory for cached files. Used when the file system is mounted "
"by root. It must belong do group B<@USER@> and read, write and execute bits "
"for group must be set. There is a subdirectory for every mounted file "
"system. The names of this subdirectories are created from url, mount point "
"and user name."
msgstr ""

# type: TP
#: mount.davfs.8:452
#, no-wrap
msgid "I<~/.@PACKAGE@/cache>"
msgstr ""

# type: Plain text
#: mount.davfs.8:456
msgid ""
"Cache directory in the mounting users home directory. For every mounted "
"WebDAV resource a subdirectory is created."
msgstr ""

# type: Plain text
#: mount.davfs.8:460
msgid ""
"B<@PROGRAM_NAME@> will try to create missing directories, but it will B<not> "
"touch I<@SYS_CONF_DIR@>."
msgstr ""

# type: SH
#: mount.davfs.8:461
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

# type: TP
#: mount.davfs.8:463
#, no-wrap
msgid "B<https_proxy http_proxy all_proxy>"
msgstr ""

# type: Plain text
#: mount.davfs.8:468
msgid ""
"If no proxy is defined in the configuration file the value is taken from "
"this environment variables. The proxy may be given with or without scheme "
"and with or without port"
msgstr ""

# type: Plain text
#: mount.davfs.8:470
msgid "http_proxy=[http://]foo.bar[:3218]"
msgstr ""

# type: Plain text
#: mount.davfs.8:472 mount.davfs.8:480
msgid "Only used when the mounting user is root."
msgstr ""

# type: TP
#: mount.davfs.8:473
#, no-wrap
msgid "B<no_proxy>"
msgstr ""

# type: Plain text
#: mount.davfs.8:478
msgid ""
"A comma separated list of domain names that shall be accessed directly.  "
"B<*> matches any domain name. A domain name starting with B<.> (period) "
"matches all subdomains."
msgstr ""

# type: Plain text
#: mount.davfs.8:482
msgid "Not applied when the proxy is defined in I<@SYS_CONF_DIR@>."
msgstr ""

# type: SH
#: mount.davfs.8:484
#, no-wrap
msgid "EXAMPLES"
msgstr ""

# type: Plain text
#: mount.davfs.8:487
msgid "B<Non root user (e.g. filomena):>"
msgstr ""

# type: Plain text
#: mount.davfs.8:490
msgid "To allow an ordinary user to mount there must be an entry in I<fstab>"
msgstr ""

# type: Plain text
#: mount.davfs.8:492
msgid "http://webdav.org/dav /media/dav davfs noauto,user 0 0"
msgstr ""

# type: Plain text
#: mount.davfs.8:497
msgid ""
"If a proxy must be used this should be configured in I<@SYS_CONF_DIR@/"
"@CONFIGFILE@>"
msgstr ""

# type: Plain text
#: mount.davfs.8:499
msgid "proxy proxy.mycompany.com:8080"
msgstr ""

# type: Plain text
#: mount.davfs.8:503
msgid "Credentials are stored in I</home/filomena/.@PACKAGE@/@SECRETSFILE@>"
msgstr ""

# type: Plain text
#: mount.davfs.8:505
msgid "proxy.mycompany.com filomena \"my secret\""
msgstr ""

# type: Plain text
#: mount.davfs.8:507
msgid "/media/dav webdav-username password"
msgstr ""

# type: Plain text
#: mount.davfs.8:511
msgid "Now the WebDAV resource may be mounted by user filomena invoking"
msgstr ""

# type: Plain text
#: mount.davfs.8:513
msgid "B<mount /media/dav>"
msgstr ""

# type: Plain text
#: mount.davfs.8:517
msgid "and unmounted by user filomena invoking"
msgstr ""

# type: Plain text
#: mount.davfs.8:519
msgid "B<umount /media/dav>"
msgstr ""

# type: Plain text
#: mount.davfs.8:523
msgid "B<Root user only:>"
msgstr ""

# type: Plain text
#: mount.davfs.8:529
msgid ""
"Mounts the resource I<https://asciigirl.com/webdav> at mount point I</mount/"
"site>, encrypting all traffic with SSL. Credentials for I<http://webdav.org/"
"dav> will be looked up in I<@SYS_CONF_DIR@/@SECRETSFILE@>, if not found "
"there the user will be asked."
msgstr ""

# type: Plain text
#: mount.davfs.8:531
msgid ""
"B<mount -t davfs -o uid=otto,gid=users,mode=775 https://asciigirl.com/"
"webdav /mount/site>"
msgstr ""

# type: Plain text
#: mount.davfs.8:535
msgid "Mounts the resource I<http://linux.org.ar/repos> at I</dav>."
msgstr ""

# type: Plain text
#: mount.davfs.8:537
msgid ""
"B<mount.davfs -o uid=otto,gid=users,mode=775 http://linux.org.ar/repos/ /dav>"
msgstr ""

# type: SH
#: mount.davfs.8:540 umount.davfs.8:74
#, no-wrap
msgid "BUGS"
msgstr ""

# type: Plain text
#: mount.davfs.8:543
msgid "B<@PACKAGE@> does not support links (neither hard nor soft ones)."
msgstr ""

# type: Plain text
#: mount.davfs.8:549
msgid ""
"This man page was written by Luciano Bello E<lt>luciano@linux.org.arE<gt> "
"for Debian, for version 0.2.3 of davfs2."
msgstr ""

# type: Plain text
#: mount.davfs.8:553
msgid ""
"It has been updated for this version by Werner Baumann E<lt>wbaumann@users."
"sourceforge.netE<gt>."
msgstr ""

# type: Plain text
#: mount.davfs.8:556
msgid "@PACKAGE@ is developed by Sung Kim E<lt>hunkim@gmail.comE<gt>."
msgstr ""

# type: Plain text
#: mount.davfs.8:560
msgid ""
"Version 1.0.0 (and later) of @PACKAGE@ is an almost complete rewrite by "
"Werner Baumann."
msgstr ""

# type: Plain text
#: mount.davfs.8:573
msgid ""
"B<u@PROGRAM_NAME@>(8), B<@CONFIGFILE@>(5), B<mount>(8), B<umount>(8), "
"B<fstab>(5)"
msgstr ""
