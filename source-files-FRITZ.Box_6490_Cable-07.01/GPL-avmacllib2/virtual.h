/*
 * Virtual Filesystem Root
 *
 * Copyright (c) 2009-2018
 *  AVM GmbH, Berlin, Germany
 *
 * License: Free, use with no restriction.
 */
#ifndef AVMACL2_VIRTUAL_H
#define AVMACL2_VIRTUAL_H

#ifdef __cplusplus
extern "C" {
#endif

// set the real path of virtual "/"
void virtual_set_root(const char *real_root);

// change the real path to a virtual client path
void virtual_make_from_real_path(char **ppath);

FILE *virtual_fopen(const char *fn, const char *mode);
int virtual_mkdir(const char *dir, mode_t mode);
int virtual_rename(const char *from, const char *to);
int virtual_stat(const char *path, struct stat *buf);
int virtual_rmdir(const char *pathname);

/**
 * @brief      Remove a directory recursively.
 *             Does not follow links.
 *             On any error, errno will be set to EACCES or ENOTDIR.
 *
 * @param  pathname  Virtual path name.
 *
 * @retval   0  Success.
 * @retval  -1  Not allowed to access directory, directory not existent or no directory. Nothing happend.
 * @retval  -2  Failed to delete a file or directory. Some are still left, others may gone.
 */
int virtual_rmdir_recursive(const char *pathname);

int virtual_unlink(const char *pathname);
int virtual_chdir(const char *path);
DIR *virtual_opendir(const char *name);
int virtual_chmod(const char *path, mode_t mode);

int virtual_is_write_access(const char *path, /*OUT*/char **preal_path);
int virtual_is_read_access(const char *path, /*OUT*/char **preal_path);

#ifdef __cplusplus
}
#endif

#endif /* !AVMACL2_VIRTUAL_H */
