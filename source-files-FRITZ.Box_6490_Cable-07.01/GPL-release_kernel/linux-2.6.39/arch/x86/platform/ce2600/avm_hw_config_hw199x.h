/*------------------------------------------------------------------------------------------*\
 *
 * Hardware Config für FRITZ!Box Cable 6370 SL
 *
\*------------------------------------------------------------------------------------------*/

#undef AVM_HARDWARE_CONFIG
#define AVM_HARDWARE_CONFIG  avm_hardware_config_hw199x


struct _avm_hw_config AVM_HARDWARE_CONFIG[] = {


    /****************************************************************************************\
     *
     * GPIO Config
     *
    \****************************************************************************************/

    /*--------------------------------------------------------------------------------------*\
     * DECT
    \*--------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_dect_rd",
        .value  = 51,
        .param  = avm_hw_param_gpio_out_active_low,
    },

    {
        .name   = "gpio_avm_dect_reset",
        .value  = 57,
        .param  = avm_hw_param_gpio_out_active_low,
    },

    /*--------------------------------------------------------------------------------------*\
     * FPGA
    \*--------------------------------------------------------------------------------------*/
    { 
        .name   = "gpio_avm_piglet_noemif_clk",              
        .value  = 76,
        .param  = avm_hw_param_no_param,
    },
    { 
        .name   = "gpio_avm_piglet_noemif_done",              
        .value  = 88,
        .param  = avm_hw_param_no_param,
    },
    { 
        .name   = "gpio_avm_piglet_noemif_prog",
        .value  = 89,
        .param  = avm_hw_param_no_param,
    },
    { 
        .name   = "gpio_avm_piglet_noemif_data",
        .value  = 98,
        .param  = avm_hw_param_no_param,
    },
    /*--------------------------------------------------------------------------------*\
     * TDM
    \*--------------------------------------------------------------------------------*/
    { 
        .name   = "gpio_avm_tdm_fsc",              
        .value  = 82,
        .param  = avm_hw_param_no_param,
    },
    { 
        .name   = "gpio_avm_tdm_dcl",              
        .value  = 81,
        .param  = avm_hw_param_no_param,
    },



    /*--------------------------------------------------------------------------------------*\
     * USB
    \*--------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_usb_pwr_en0",
        .value  =  99,
    },

    /*--------------------------------------------------------------------------------------*\
     * WLAN
    \*--------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_offload_24ghz_reset",
        .value  =  75,
        .param  = avm_hw_param_gpio_out_active_low,
    },
    {
        .name   = "gpio_avm_offload_5ghz_reset",
        .value  =  103,
        .param  = avm_hw_param_gpio_out_active_low,
    },

    {   .name   = NULL }
};
EXPORT_SYMBOL(AVM_HARDWARE_CONFIG);


