#include <arch-avalanche/generic/pal.h>
#include <arch-avalanche/generic/pal_sys.h>
#include <asm/mach_avm.h>
#include <mach/hw_gpio.h>

static unsigned int c55_mem_start;


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
unsigned int puma_get_clock(enum _avm_clock_id clock_id) {
    unsigned int clk = 0;
    switch(clock_id) {
        /*----------------------------------------------------------------------------------*\
        \*----------------------------------------------------------------------------------*/
        case avm_clock_id_cpu:      clk = PAL_sysClkcGetFreq (PAL_SYS_CLKC_ARM); break;
        case avm_clock_id_system:   clk = PAL_sysClkcGetFreq (PAL_SYS_CLKC_RAM); break;
        /*--- case avm_clock_id_usb:      clk = PAL_sysClkcGetFreq (PAL_SYS_CLKC_USB); break; ---*/
        /*--- case avm_clock_id_docsis:   clk = PAL_sysClkcGetFreq (PAL_SYS_CLKC_DOCSIS); break; ---*/
        /*--- case avm_clock_id_gmii:     clk = PAL_sysClkcGetFreq (PAL_SYS_CLKC_GMII); break; ---*/
        /*--- case avm_clock_id_sflash:   clk = PAL_sysClkcGetFreq (PAL_SYS_CLKC_ARM) / 8; break; ---*/
        case avm_clock_id_tdm:      clk = PAL_sysClkcGetFreq (PAL_SYS_CLKC_TDM); break;
        default:
            printk(KERN_INFO"puma_get_clock: unknown id=%d\n", clock_id);
            break;
    }
    /*--- printk(KERN_INFO"puma_get_clock: %s %u %cHz\n", puma_name_clock_id(clock_id), puma_norm_clock(clk, 0), puma_norm_clock(clk, 1)); ---*/
    return clk;
}        
EXPORT_SYMBOL(puma_get_clock);

/*--- Kernel-Schnittstelle für das neue LED-Modul ---*/
enum _led_event { /* DUMMY DEFINITION */ LastEvent = 0 };
int (*led_event_action)(int, enum _led_event , unsigned int ) = NULL;
EXPORT_SYMBOL(led_event_action);

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int var_is_in_area(unsigned long test, unsigned long range_min, unsigned long range_max) {
    if((test >= range_min) && (test < range_max)) {
        return 1;
    }
    return 0;
}
extern unsigned long _text, _stext, _etext, _sdata, _end, __init_begin, __init_end;
#define ALIGN_16MBYTE(addr) ((addr) &  ~((16 * SZ_1M) - 1))
/*--------------------------------------------------------------------------------*\
 * keine Ueberschneidung mit Kernel!
\*--------------------------------------------------------------------------------*/
struct resource *puma5_alloc_c55_code(unsigned long ramstart, unsigned long ramend) {
#if defined(CONFIG_ARM_PUMA_C55_MEMORY) && (CONFIG_ARM_PUMA_C55_MEMORY != 0)
    static struct resource c55_pram;
    unsigned long c55_codsize  = SZ_1K * CONFIG_ARM_PUMA_C55_MEMORY;
	unsigned long kcode_start  = virt_to_phys(&_text);
	unsigned long kcode_end    = virt_to_phys(&_etext - 1);
	unsigned long kdata_start  = virt_to_phys(&_sdata);
	unsigned long kdata_end    = virt_to_phys(&_end - 1);
    
    ramstart = ALIGN_16MBYTE(ramstart + (16 * SZ_1M));
    c55_mem_start = 0;

    /*--- printk("[c55] kcode_start = 0x%x kcode_end 0x%x kdata_start 0x%x kdata_end 0x%x\n", kcode_start, kcode_end, kdata_start, kdata_end); ---*/
    while(ramstart < ramend) {
        c55_mem_start = ramstart - c55_codsize;
        /*--- printk("[c55] ramend %x ramstart %x c55_mem_start %x\n", ramend, ramstart, c55_mem_start); ---*/
        if(!var_is_in_area(c55_mem_start, kcode_start, kcode_end) && 
           !var_is_in_area(c55_mem_start + c55_codsize, kcode_start, kcode_end) &&
           !var_is_in_area(c55_mem_start, kdata_start, kdata_end) && 
           !var_is_in_area(c55_mem_start + c55_codsize, kdata_start, kdata_end)) {
            break;
        }
        ramstart += (16 * SZ_1M);
        c55_mem_start = 0;
    }
    if(c55_mem_start) {
        printk("[c55] c55_mem_start = 0x%x\n", c55_mem_start);
        c55_pram.name  = "c55 text";
        c55_pram.start = c55_mem_start;
        c55_pram.end   = c55_pram.start + c55_codsize - 1;
        c55_pram.flags = IORESOURCE_MEM | IORESOURCE_BUSY;
        return &c55_pram;
    }
#endif/*--- #if defined(CONFIG_ARM_PUMA_C55_MEMORY) && (CONFIG_ARM_PUMA_C55_MEMORY != 0) ---*/
    return NULL;
}

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
int prom_c55_get_base_memory(unsigned int *base, unsigned int *len) {
    if(len)*len  = CONFIG_ARM_PUMA_C55_MEMORY * (1 << 10);
    if(base)*base = c55_mem_start;
    return 0;
}
EXPORT_SYMBOL(prom_c55_get_base_memory);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int puma_gpio_ctrl(unsigned int gpio_pin, enum _hw_gpio_function pin_mode, enum _hw_gpio_direction pin_dir) {

    /*--- TODO: Function Flag auswerten ---*/
    return PAL_sysGpioCtrlSetDir(gpio_pin, (PAL_SYS_GPIO_PIN_DIRECTION_T)pin_dir);
}
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int puma_gpio_out_bit(unsigned int gpio_pin, int value) {
    return PAL_sysGpioOutBit(gpio_pin, value);
}
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int puma_gpio_out_bit_no_sched(unsigned int gpio_pin, int value) {
    return PAL_sysGpioOutBitNoSched(gpio_pin, value);
}
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int puma_gpio_in_bit(unsigned int gpio_pin) {
    return PAL_sysGpioInBit(gpio_pin);
}
EXPORT_SYMBOL(puma_gpio_ctrl);
EXPORT_SYMBOL(puma_gpio_out_bit);
EXPORT_SYMBOL(puma_gpio_out_bit_no_sched);
EXPORT_SYMBOL(puma_gpio_in_bit);
