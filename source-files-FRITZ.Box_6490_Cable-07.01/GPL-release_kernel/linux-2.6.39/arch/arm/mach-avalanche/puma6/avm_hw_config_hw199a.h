/*------------------------------------------------------------------------------------------*\
 *
 * Hardware Config für FRITZ!Box Cable 6370 SL
 *
\*------------------------------------------------------------------------------------------*/

#undef AVM_HARDWARE_CONFIG
#define AVM_HARDWARE_CONFIG  avm_hardware_config_hw199a


struct _avm_hw_config AVM_HARDWARE_CONFIG[] = {


    /****************************************************************************************\
     *
     * GPIO Config
     *
    \****************************************************************************************/


    /*------------------------------------------------------------------------------------------*\
     * LEDs / Taster
    \*------------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_led_power",
        .value  = 92,
        .param  = avm_hw_param_gpio_out_active_low,
    },
    {
        .name   = "gpio_avm_led_info_red",
        .value  = 93,
        .param  = avm_hw_param_gpio_out_active_low,
    },
#if 1
    {
        .name   = "gpio_avm_led_internet",
        .value  = 101,
        .param  = avm_hw_param_gpio_out_active_low,
    },
#endif
    {
        .name   = "gpio_avm_led_dect",
        .value  = 91,
        .param  = avm_hw_param_gpio_out_active_low,
    },
    {
        .name   = "gpio_avm_led_wlan",
        .value  = 102,
        .param  = avm_hw_param_gpio_out_active_low,
    },
    {
        .name   = "gpio_avm_led_info",
        .value  = 104,
        .param  = avm_hw_param_gpio_out_active_low,
    },

    {
        .name   = "gpio_avm_button_dect",
        .value  = 69,
        .param  = avm_hw_param_gpio_in_active_low,
    },
    {
        .name   = "gpio_avm_button_wlan",
        .value  = 53,
        .param  = avm_hw_param_gpio_in_active_low,
    },


    /*--------------------------------------------------------------------------------------*\
     * DECT
    \*--------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_spi_dect_clk",
        .value  = 77,
        .param  = avm_hw_param_no_param,
    },
    {
        .name   = "gpio_avm_spi_dect_cs",
        .value  = 78,
        .param  = avm_hw_param_no_param,
    },
    {
        .name   = "gpio_avm_spi_dect_miso",
        .value  = 79,
        .param  = avm_hw_param_no_param,
    },
    {
        .name   = "gpio_avm_spi_dect_mosi",
        .value  = 80,
        .param  = avm_hw_param_no_param,
    },


    /*--------------------------------------------------------------------------------------*\
     * SPI
    \*--------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_spi_clk",
        .value  = 62,
        .param  = avm_hw_param_no_param,
    },
    {
        .name   = "gpio_avm_spi_dio0",
        .value  = 63,
        .param  = avm_hw_param_no_param,
    },
    {
        .name   = "gpio_avm_spi_dio1",
        .value  = 64,
        .param  = avm_hw_param_no_param,
    },
    {
        .name   = "gpio_avm_spi_dio2",
        .value  = 67,
        .param  = avm_hw_param_no_param,
    },
    {
        .name   = "gpio_avm_spi_dio3",
        .value  = 68,
        .param  = avm_hw_param_no_param,
    },
    {
        .name   = "gpio_avm_spi_cs0",
        .value  = 65,
        .param  = avm_hw_param_gpio_out_active_high,
    },
    {
        .name   = "gpio_avm_spi_cs1",
        .value  = 66,
        .param  = avm_hw_param_gpio_out_active_high,
    },


    /*--------------------------------------------------------------------------------------*\
     * USB
    \*--------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_usb_pwr_en0",
        .value  =  99,
    },


    /*--------------------------------------------------------------------------------------*\
     * ETHERNET
    \*--------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_extphy1_reset",
        .value  = 96,
        .param  = avm_hw_param_gpio_out_active_low,
    },
    {
        .name   = "gpio_avm_extphy_int",
        .value  = 28,
        .param  = avm_hw_param_no_param,
    },


    /*--------------------------------------------------------------------------------------*\
     * PCIE / WLAN / Ext. WASP
    \*--------------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_pcie_reset",
        .value  = 100,
        .param  = avm_hw_param_gpio_out_active_low,
    },

    /*--------------------------------------------------------------------------------*\
     * TDM
    \*--------------------------------------------------------------------------------*/
    {
        .name   = "gpio_avm_tdm_fsc",
        .value  = 82,
        .param  = avm_hw_param_no_param,
    },
    {
        .name   = "gpio_avm_tdm_dcl",
        .value  = 81,
        .param  = avm_hw_param_no_param,
    },

    {   .name   = NULL }
};
EXPORT_SYMBOL(AVM_HARDWARE_CONFIG);


