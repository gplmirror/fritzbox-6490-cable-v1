#include <linux/kernel.h>


extern void internal_memcpy(void *dest, const void *src, size_t n); 

void *memcpy(void *dest, const void *src, size_t n) {
    internal_memcpy(dest, src, n);
    return dest;
}

extern void internal_memmove(void *dest, const void *src, size_t n);

void *memmove(void *dest, const void *src, size_t n) {
    internal_memmove(dest, src, n);
    return dest;
}

extern void *internal_arm_memset(void *s, int c, size_t n);

void *arm_memset(void *s, int c, size_t n) {
    internal_arm_memset(s, c, n);
    return s;
}
