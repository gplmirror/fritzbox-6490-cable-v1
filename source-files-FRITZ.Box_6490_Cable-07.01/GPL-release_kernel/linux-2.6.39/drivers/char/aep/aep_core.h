/*
* File Name: aep_core.h
*/

/*
  This file is provided under a dual BSD/GPLv2 license.  When using or
  redistributing this file, you may do so under either license.

  GPL LICENSE SUMMARY

  Copyright(c) 2007-2012 Intel Corporation. All rights reserved.

  This program is free software; you can redistribute it and/or modify
  it under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
  The full GNU General Public License is included in this distribution
  in the file called LICENSE.GPL.

  Contact Information:
  Intel Corporation
  2200 Mission College Blvd.
  Santa Clara, CA  97052

*/

#ifndef __AEP_CORE_H__
#define __AEP_CORE_H__
#include <linux/init.h>
#include <linux/cdev.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/printk.h>
#include <linux/kernel.h>
#include <linux/interrupt.h>
#include <linux/pci.h>
#include <linux/aep.h>
#include <linux/atomic.h>
#include "aep_types.h"
#include "aep_ipc_config.h"


#define INTELCE_AEP_DRV_NAME		"intelce_aep"
#define PCI_INTELCE_AEP_DEVICE_ID  0x0956
#define INTELCE_AEP_BAR        0
#define C0_REV_ID                0x08


/*Decorator for functions shared outside of a code file (i.e. not static)*/
#ifdef VERBOSE_DEBUG
#define DEBUG_OUT (...) do {OS_INFO("DEBUG: "__VA_ARGS__); } while (0)
#else
#define DEBUG_OUT (...)  
#endif


#define AEP_LOG_DEBUG 3
#define AEP_LOG_MSG 2
#define AEP_LOG_WARNING 2
#define AEP_LOG_ERROR 1
#define AEP_LOG_NONE 0

extern int AEP_global_log_level;

#define OS_INFO(arg...)                                    \
			do {   \
				printk(KERN_ERR "%s:%s:", \
				(strrchr(__FILE__, '/') \
				 ? (char *)(strrchr(__FILE__, '/')+1)	\
				  : __FILE__), __func__);  \
				printk(arg);                                \
			} while (0);


#define AEP_STRINGIFY(x) #x
#define AEP_TOSTRING(x) AEP_STRINGIFY(x)
#define LOG_MSG(msg, ...)            \
			do { \
				if (AEP_LOG_MSG <= AEP_global_log_level) \
					OS_INFO("AEP: " msg, ##__VA_ARGS__); \
				} while (0);

#define LOG_DEBUG(msg, ...) \
	do { \
			if (AEP_LOG_DEBUG <= AEP_global_log_level) \
				OS_INFO("%d AEP DEBUG: " msg,\
				__LINE__, ##__VA_ARGS__); \
		} while (0);

#define LOG_WARNING(msg, ...)   \
	do { \
			if (AEP_LOG_WARNING <= AEP_global_log_level) \
				OS_INFO("%d AEP WARNING: " msg, \
				__LINE__, ##__VA_ARGS__);\
		} while (0);

#define LOG_ERROR(msg, ...) \
		do { \
				if (AEP_LOG_ERROR <= AEP_global_log_level) \
					printk(KERN_ERR "%d AEP ERROR: " msg,\
					 __LINE__, ##__VA_ARGS__); \
			} while (0);


#ifdef AEP_TRACE
#define AEP_FUNC_ENTER()   \
			do { \
				printk(KERN_INFO  "AEP >> %s:%s(%d)\n",\
				 __FILE__, __func__, __LINE__);\
			} while (0);

#define AEP_FUNC_EXIT() \
			do { \
				printk(KERN_INFO "AEP << %s:%s(%d)\n", \
				__FILE__, __func__, __LINE__);\
			} while (0);

#define AEP_TRACE_EVENT(msg, ...) \
			do { \
				printk(KERN_INFO "AEP :: %s:%s(%d) "msg"\n",\
				 __FILE__, __func__, __LINE__, ##__VA_ARGS__); \
			} while (0);

#ifdef AEP_TRACE_VERBOSE
#define AEP_FUNC_ENTER_V() \
			do { \
				printk(KERN_INFO "AEP >> %s:%s(%d)\n", \
				__FILE__, __func__, __LINE__); \
			} while	(0);

#define AEP_FUNC_EXIT_V() \
			do { \
				printk(KERN_INFO "AEP << %s:%s(%d)\n", \
				__FILE__, __func__, __LINE__); \
			} while	(0);
#else
#define AEP_FUNC_ENTER_V()
#define AEP_FUNC_EXIT_V()
#endif
#else
#define AEP_FUNC_ENTER()
#define AEP_FUNC_EXIT()
#define AEP_TRACE_EVENT(...)
#define AEP_FUNC_ENTER_V()
#define AEP_FUNC_EXIT_V()
#endif


#define VERIFY_QUICK(expression, label) \
			if (!(expression)) {\
				goto label;\
			}

#define VERIFY(expression, label, rc, val) \
			if (!(expression)) {           \
				rc = val;                  \
				goto label;                \
			}

#define PV_TO_HOST_INTERRUPT_ENABLE 0x1410

#define AEP_HOST_INTERRUPT_ENABLE_HOST_Doorbell_Event 0x4

#define CE_SOC_AEP_DEVICE_ID 0x0956
#define CE_SOC_VENDOR_ID_INTEL        0x8086


#define PCI_BUS_AEP   1
#define PCI_DEV_AEP   7
#define PCI_FUNC_AEP  0

#define AEP_DEVICE_NAME "aep"

#define AEP_ATM_MAILBOX_SIZE              0x40

/**
aep_internal_global_event_t defines the global  events
supported by AEP.
*/
typedef enum aep_request_ipc_t {
	AEP_NP_ATOM_IPC,
	/**
	Used internally to determine the number of IPC requests from AEP
	*/
	AEP_IPC_REQUEST_COUNT,
} aep_request_ipc_t;


/*
   * Structure describing the current power state of the device and
   * also providing a lock which can be used to synchronize any
   * software operations that affect the power state.
   */
typedef enum { D0, D1, D2, D3 } power_state_t;

typedef struct aep_power_status_t {
	/** Current power state of the hardware device. */
	power_state_t     state;
} aep_power_status_t;

typedef struct {
	struct work_struct aep_work;
	aep_fw_cmd_t ipc_cmd;
	void __iomem *aep_io_addr;
	host_ipc_handler  *hipc;
	ipl_t *ipl;
} aep_work_t;

/**
Stuct used to contain values global to the driver.
*/
typedef struct aep_globals_t {
	/**
	the current power settings for the underlying HW device.
	*/
	aep_power_status_t power;

	/**
	Mem i/o address
	*/

	void __iomem *io_addr;

	int irq;
	struct workqueue_struct	*aep_work_queue;
	aep_work_t aep_req_work;

	struct tasklet_struct finish_tasklet;

} aep_globals_t;


/**
container for variables global to the driver.
*/
extern aep_globals_t aep_globals;

/**
main ISR entry point function
*/

bool aep_hw_device_suspended(void);

void aep_workq_func(struct work_struct *work);

#endif
