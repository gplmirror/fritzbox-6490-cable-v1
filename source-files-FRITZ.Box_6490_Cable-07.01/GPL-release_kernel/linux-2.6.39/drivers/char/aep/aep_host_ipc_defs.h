/*
* File Name: aep_host_ipc_defs.h
*/

/*
  This file is provided under a dual BSD/GPLv2 license.  When using or
  redistributing this file, you may do so under either license.

  GPL LICENSE SUMMARY

  Copyright(c) 2007-2012 Intel Corporation. All rights reserved.

  This program is free software; you can redistribute it and/or modify
  it under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
  The full GNU General Public License is included in this distribution
  in the file called LICENSE.GPL.

  Contact Information:
  Intel Corporation
  2200 Mission College Blvd.
  Santa Clara, CA  97052

*/
#include "aep_core.h"
#include "aep_types.h"

aep_ipc_ret_t hostIPC_SendCmd(host_ipc_handler  *hipc,
								aep_fw_cmd_t ipc_cmd,
								aep_ipc_sizes_t io_sizes,
								ipl_t *ipl,
								opl_t *opl);

aep_ipc_ret_t HostIPC_ReadSynchronousResponse(host_ipc_handler  *hipc,
											 void *message_buf,
											 int offset,
											 int message_buf_size);

aep_ipc_ret_t hostIPC_SendResponse(host_ipc_handler  *hipc,
									aep_fw_cmd_t ipc_cmd,
									aep_ipc_sizes_t io_sizes,
									ipl_t *ipl);

