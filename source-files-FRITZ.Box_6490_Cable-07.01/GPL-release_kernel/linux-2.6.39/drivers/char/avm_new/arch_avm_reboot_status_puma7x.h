#ifndef __arch_avm_reboot_status_puma7x_h__
#define __arch_avm_reboot_status_puma7x_h__

#include <linux/slab.h>
#include <linux/efi.h>

#define _repeat3concat(a,b) a a a b

#define UPDATE_REBOOT_STATUS_TEXT \
	_repeat3concat("(c) AVM 2017, Reboot Status is: Firmware-Update", "")
#define NMI_REBOOT_STATUS_TEXT \
        _repeat3concat("(c) AVM 2017, Reboot Status is: NMI-Watchdog", "")
#define SOFTWATCHDOG_REBOOT_STATUS_TEXT \
	_repeat3concat("(c) AVM 2017, Reboot Status is: Software-Watchdog", "")
#define POWERON_REBOOT_STATUS_TEXT \
	_repeat3concat("(c) AVM 2017, Reboot Status is: Power-On-Reboot", "")
#define TEMP_REBOOT_STATUS_TEXT \
        _repeat3concat("(c) AVM 2017, Reboot Status is: Temperature-Reboot", "")
#define SOFT_REBOOT_STATUS_TEXT_PANIC \
	_repeat3concat("(c) AVM 2017, Reboot Status is: Software-Reboot", \
		       "\0(PANIC)")
#define SOFT_REBOOT_STATUS_TEXT_OOM \
	_repeat3concat("(c) AVM 2017, Reboot Status is: Software-Reboot", \
		       "\0(OOM)")
#define SOFT_REBOOT_STATUS_TEXT_OOPS \
	_repeat3concat("(c) AVM 2017, Reboot Status is: Software-Reboot", \
		       "\0(OOPS)")
#define SOFT_REBOOT_STATUS_TEXT_UPDATE \
	_repeat3concat("(c) AVM 2017, Reboot Status is: Software-Reboot", \
		       "\0(RESET-FOR-UPDATE)")

/*--- Achtung! Untermenge von obigen Einträgen: ---*/
#define SOFT_REBOOT_STATUS_TEXT \
        _repeat3concat("(c) AVM 2017, Reboot Status is: Software-Reboot", "")


#define EFI_TFFS_TOKENSPACE \
    EFI_GUID( 0x692cccd4, 0xd3b9, 0x5e94, 0xba, 0x7e, 0x50, 0xaa, 0xac, 0xd8, 0xa3, 0x8d )
/*#define EFFI_REBOOT_TABLE_GUID \
    EFI_GUID( 0x508f93b0, 0xa6ed, 0x5267, 0xaa, 0x17, 0xa7, 0x13, 0x90, 0x84, 0x75, 0x9b)*/
#define VARIABLE_NAME "reboot_status"



struct efivar_entry *efi_var=NULL;


static void ascii2utf(char *ascii, efi_char16_t *utf, size_t len)
{
	while(*ascii != '\0' && len > 1){
		*utf++ = *ascii++;
		--len;
	}
	
	*utf = '\0';
}

static void init_efi_struct(void)
{
	char *name = VARIABLE_NAME;

	efi_var=kzalloc(sizeof(*efi_var),GFP_KERNEL);
	ascii2utf(name, efi_var->var.VariableName, ARRAY_SIZE(efi_var->var.VariableName));
	
	efi_var->var.VendorGuid = EFI_TFFS_TOKENSPACE;
	efi_var->var.DataSize = sizeof(efi_var->var.Data);
}

static char *arch_get_mailbox(void)
{
	char* mailbox;
	int result;
	if(efi_var==NULL)
		init_efi_struct();
	
	result = efivar_entry_get(      efi_var,
					&(efi_var->var.Attributes),
					&(efi_var->var.DataSize),
					&(efi_var->var.Data[0]));

	if(result != 0 && result != -ENOENT){
		pr_err("[%s] Error reading EFI variable %s\n", __func__, VARIABLE_NAME);
		return NULL;
	}
	mailbox=(char*) &(efi_var->var.Data[0]);
		
	return mailbox;
}

static int arch_set_mailbox_msg(const char *val)
{
	int result;

	if(efi_var==NULL)
		init_efi_struct();

	result = efivar_entry_set_safe( efi_var->var.VariableName,
					EFI_TFFS_TOKENSPACE,
					(EFI_VARIABLE_NON_VOLATILE 
					| EFI_VARIABLE_RUNTIME_ACCESS
					| EFI_VARIABLE_BOOTSERVICE_ACCESS),
                            		true,
					strlen(val) + 1,
					(void*)val);
	if(result != 0){
		pr_err("Error setting value for %s\n", VARIABLE_NAME);
	}

	return result;
}
#endif

/* vim: set ts=8 sw=8 noet cino=>8\:0l1(0: */
