/*-------------------------------------------------##########################*\
 * Modul : preinit.h  created at  Fri  05-May-1995  13:25:38 by jochen
 * $Id: preinit.h 1.1 2001/07/04 15:34:08Z MPommerenke Exp $
 * $Log: preinit.h $
 * Revision 1.1  2001/07/04 15:34:08Z  MPommerenke
 * Initial revision
 * Revision 1.1  2001/07/04 11:05:09Z  MPommerenke
 * Initial revision
 * Revision 1.1  2001/06/11 12:47:50Z  MPommerenke
 * Initial revision
 * Revision 1.2  1999/11/23 11:48:14Z  MPommerenke
 * - MaPom: neue oslib
 * Revision 1.1  1999/07/23 08:39:09Z  MPommerenke
 * Initial revision
 * Revision 1.1  1998/11/10 13:43:45  DFriedel
 * Initial revision
 * Revision 1.1  1997/04/08 17:00:42  JSchieberlein.sw.avm
 * Initial revision
 * Revision 1.2  1995/12/13 11:07:41  JSchieberlein.sw.avm
 * CAPI20, neuer Release Mechanismus
 * Revision 1.1  1995/05/10 13:36:02  JOSH
 * Initial revision
 * Revision 1.1  95/05/05 15:20:03  jochen
 * Initial revision
\*---------------------------------------------------------------------------*/
#ifndef _preinit_h
#define _preinit_h


/*-------------------------------------------------------------------------------------*\
\*-------------------------------------------------------------------------------------*/
extern char *PREINIT_Params;


/*-------------------------------------------------------------------------------------*\
\*-------------------------------------------------------------------------------------*/
void PREINIT(unsigned long data);

#endif
