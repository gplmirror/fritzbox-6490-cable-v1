#include "avm_event_gen_types.h"
#include "avm_event_gen_enum_range.h"

unsigned int avm_event_check_enum_range___avm_event_cmd(enum __avm_event_cmd E) {
	switch(E) {
		case avm_event_cmd_register:
		case avm_event_cmd_release:
		case avm_event_cmd_source_register:
		case avm_event_cmd_source_release:
		case avm_event_cmd_source_trigger:
		case avm_event_cmd_trigger:
		case avm_event_cmd_undef:
			return 0;
		default:
			return 1;
	}
}

unsigned int avm_event_check_enum_range__avm_event_ethernet_speed(enum _avm_event_ethernet_speed E) {
	switch(E) {
		case avm_event_ethernet_speed_no_link:
		case avm_event_ethernet_speed_10M:
		case avm_event_ethernet_speed_100M:
		case avm_event_ethernet_speed_1G:
		case avm_event_ethernet_speed_error:
		case avm_event_ethernet_speed_items:
			return 0;
		default:
			return 1;
	}
}

unsigned int avm_event_check_enum_range__avm_event_id(enum _avm_event_id E) {
	switch(E) {
		case avm_event_id_wlan_client_status:
		case avm_event_id_autoprov:
		case avm_event_id_usb_status:
		case avm_event_id_dsl_get_arch_kernel:
		case avm_event_id_dsl_set_arch:
		case avm_event_id_dsl_get_arch:
		case avm_event_id_dsl_set:
		case avm_event_id_dsl_get:
		case avm_event_id_dsl_status:
		case avm_event_id_dsl_connect_status:
		case avm_event_id_push_button:
		case avm_event_id_telefon_wlan_command:
		case avm_event_id_capiotcp_startstop:
		case avm_event_id_telefon_up:
		case avm_event_id_reboot_req:
		case avm_event_id_appl_status:
		case avm_event_id_led_status:
		case avm_event_id_led_info:
		case avm_event_id_telefonprofile:
		case avm_event_id_temperature:
		case avm_event_id_cpu_idle:
		case avm_event_id_powermanagment_status:
		case avm_event_id_powerline_status:
		case avm_event_id_ethernet_connect_status:
		case avm_event_id_powermanagment_remote:
		case avm_event_id_log:
		case avm_event_id_remotewatchdog:
		case avm_event_id_rpc:
		case avm_event_id_remotepcmlink:
		case avm_event_id_pm_ressourceinfo_status:
		case avm_event_id_telephony_missed_call:
		case avm_event_id_telephony_tam_call:
		case avm_event_id_telephony_fax_received:
		case avm_event_id_internet_new_ip:
		case avm_event_id_firmware_update_available:
		case avm_event_id_smarthome_switch_status:
		case avm_event_id_telephony_incoming_call:
		case avm_event_id_mass_storage_mount:
		case avm_event_id_mass_storage_unmount:
		case avm_event_id_user_source_notify:
		case avm_event_last:
			return 0;
		default:
			return 1;
	}
}

unsigned int avm_event_check_enum_range__avm_event_push_button_key(enum _avm_event_push_button_key E) {
	switch(E) {
		case avm_event_push_button_wlan_on_off:
		case avm_event_push_button_wlan_wps:
		case avm_event_push_button_wlan_standby:
		case avm_event_push_button_dect_paging:
		case avm_event_push_button_dect_pairing:
		case avm_event_push_button_dect_on_off:
		case avm_event_push_button_dect_standby:
		case avm_event_push_button_power_set_factory:
		case avm_event_push_button_power_on_off:
		case avm_event_push_button_power_standby:
		case avm_event_push_button_power_socket_on_off:
		case avm_event_push_button_tools_profiling:
		case avm_event_push_button_plc_on_off:
		case avm_event_push_button_plc_pairing:
		case avm_event_push_button_led_standby:
			return 0;
		default:
			return 1;
	}
}

unsigned int avm_event_check_enum_range__avm_logtype(enum _avm_logtype E) {
	switch(E) {
		case local_panic:
		case local_crash:
		case remote_panic:
		case remote_crash:
			return 0;
		default:
			return 1;
	}
}

unsigned int avm_event_check_enum_range__avm_remote_wdt_cmd(enum _avm_remote_wdt_cmd E) {
	switch(E) {
		case wdt_register:
		case wdt_release:
		case wdt_trigger:
			return 0;
		default:
			return 1;
	}
}

unsigned int avm_event_check_enum_range__avm_remotepcmlinktype(enum _avm_remotepcmlinktype E) {
	switch(E) {
		case rpcmlink_register:
		case rpcmlink_release:
			return 0;
		default:
			return 1;
	}
}

unsigned int avm_event_check_enum_range__avm_rpctype(enum _avm_rpctype E) {
	switch(E) {
		case command_to_arm:
		case command_to_atom:
		case reply_to_arm:
		case reply_to_atom:
			return 0;
		default:
			return 1;
	}
}

unsigned int avm_event_check_enum_range__cputype(enum _cputype E) {
	switch(E) {
		case host_cpu:
		case remote_cpu:
			return 0;
		default:
			return 1;
	}
}

unsigned int avm_event_check_enum_range__powermanagment_device(enum _powermanagment_device E) {
	switch(E) {
		case powerdevice_none:
		case powerdevice_cpuclock:
		case powerdevice_dspclock:
		case powerdevice_systemclock:
		case powerdevice_wlan:
		case powerdevice_isdnnt:
		case powerdevice_isdnte:
		case powerdevice_analog:
		case powerdevice_dect:
		case powerdevice_ethernet:
		case powerdevice_dsl:
		case powerdevice_usb_host:
		case powerdevice_usb_client:
		case powerdevice_charge:
		case powerdevice_loadrate:
		case powerdevice_temperature:
		case powerdevice_dectsync:
		case powerdevice_usb_host2:
		case powerdevice_usb_host3:
		case powerdevice_dsp_loadrate:
		case powerdevice_vdsp_loadrate:
		case powerdevice_lte:
		case powerdevice_loadrate2:
		case powerdevice_dvbc:
		case powerdevice_maxdevices:
			return 0;
		default:
			return 1;
	}
}

unsigned int avm_event_check_enum_range__powermanagment_status_type(enum _powermanagment_status_type E) {
	switch(E) {
		case dsl_status:
			return 0;
		default:
			return 1;
	}
}

unsigned int avm_event_check_enum_range_avm_event_firmware_type(enum avm_event_firmware_type E) {
	switch(E) {
		case box_firmware:
		case fritz_fon_firmware:
		case fritz_dect_repeater:
		case fritz_plug_switch:
		case fritz_hkr:
			return 0;
		default:
			return 1;
	}
}

unsigned int avm_event_check_enum_range_avm_event_internet_new_ip_param_sel(enum avm_event_internet_new_ip_param_sel E) {
	switch(E) {
		case avm_event_internet_new_ip_v4:
		case avm_event_internet_new_ip_v6:
			return 0;
		default:
			return 1;
	}
}

unsigned int avm_event_check_enum_range_avm_event_led_id(enum avm_event_led_id E) {
	switch(E) {
		case avm_logical_led_inval:
		case avm_logical_led_ppp:
		case avm_logical_led_error:
		case avm_logical_led_pots:
		case avm_logical_led_info:
		case avm_logical_led_traffic:
		case avm_logical_led_freecall:
		case avm_logical_led_avmusbwlan:
		case avm_logical_led_sip:
		case avm_logical_led_mwi:
		case avm_logical_led_fest_mwi:
		case avm_logical_led_isdn_d:
		case avm_logical_led_isdn_b1:
		case avm_logical_led_isdn_b2:
		case avm_logical_led_lan:
		case avm_logical_led_lan1:
		case avm_logical_led_adsl:
		case avm_logical_led_power:
		case avm_logical_led_usb:
		case avm_logical_led_wifi:
		case avm_logical_led_last:
			return 0;
		default:
			return 1;
	}
}

unsigned int avm_event_check_enum_range_avm_event_msg_type(enum avm_event_msg_type E) {
	switch(E) {
		case avm_event_source_register_type:
		case avm_event_source_unregister_type:
		case avm_event_source_notifier_type:
		case avm_event_remote_source_trigger_request_type:
		case avm_event_ping_type:
		case avm_event_tffs_type:
			return 0;
		default:
			return 1;
	}
}

unsigned int avm_event_check_enum_range_avm_event_powermanagment_remote_action(enum avm_event_powermanagment_remote_action E) {
	switch(E) {
		case avm_event_powermanagment_ressourceinfo:
		case avm_event_powermanagment_activatepowermode:
			return 0;
		default:
			return 1;
	}
}

unsigned int avm_event_check_enum_range_avm_event_switch_type(enum avm_event_switch_type E) {
	switch(E) {
		case binary:
		case percent:
			return 0;
		default:
			return 1;
	}
}

unsigned int avm_event_check_enum_range_avm_event_telephony_param_sel(enum avm_event_telephony_param_sel E) {
	switch(E) {
		case avm_event_telephony_params_name:
		case avm_event_telephony_params_msn_name:
		case avm_event_telephony_params_calling:
		case avm_event_telephony_params_called:
		case avm_event_telephony_params_duration:
		case avm_event_telephony_params_port:
		case avm_event_telephony_params_portname:
		case avm_event_telephony_params_id:
		case avm_event_telephony_params_tam_path:
			return 0;
		default:
			return 1;
	}
}

unsigned int avm_event_check_enum_range_avm_event_tffs_call_type(enum avm_event_tffs_call_type E) {
	switch(E) {
		case avm_event_tffs_call_open:
		case avm_event_tffs_call_close:
		case avm_event_tffs_call_read:
		case avm_event_tffs_call_write:
		case avm_event_tffs_call_cleanup:
		case avm_event_tffs_call_reindex:
		case avm_event_tffs_call_info:
		case avm_event_tffs_call_init:
		case avm_event_tffs_call_deinit:
		case avm_event_tffs_call_notify:
		case avm_event_tffs_call_paniclog:
			return 0;
		default:
			return 1;
	}
}

unsigned int avm_event_check_enum_range_avm_event_tffs_notify_event(enum avm_event_tffs_notify_event E) {
	switch(E) {
		case avm_event_tffs_notify_clear:
		case avm_event_tffs_notify_update:
		case avm_event_tffs_notify_reinit:
			return 0;
		default:
			return 1;
	}
}

unsigned int avm_event_check_enum_range_avm_event_tffs_open_mode(enum avm_event_tffs_open_mode E) {
	switch(E) {
		case avm_event_tffs_mode_read:
		case avm_event_tffs_mode_write:
		case avm_event_tffs_mode_panic:
			return 0;
		default:
			return 1;
	}
}

unsigned int avm_event_check_enum_range_ePLCState(enum ePLCState E) {
	switch(E) {
		case PLCStateRunningNotConnected:
		case PLCStateRunningConnected:
		case PLCStateNotRunning:
			return 0;
		default:
			return 1;
	}
}
#include "avm_event_gen_enum_range_check_init.c"
