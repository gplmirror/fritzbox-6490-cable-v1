#! /usr/bin/perl -w
package input_union;

use strict;
use warnings;

use Exporter ();
our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);
$VERSION     = 1.00;

@ISA = qw(Exporter);
@EXPORT = qw(&input_union_init %union);
@EXPORT_OK = qw();

our %union;

$union{_avm_event_cmd_param} = {
    "priority" => 10000,
	"name" => "_avm_event_cmd_param",
	"selecttype" => "enum __avm_event_cmd",
	"union" => [
        { "type" => "struct _avm_event_cmd_param_register", "name" => "avm_event_cmd_param_register", "select" => [ "avm_event_cmd_register" ]  },
        { "type" => "struct _avm_event_cmd_param_release", "name" => "avm_event_cmd_param_release", "select" => [ "avm_event_cmd_release" ]  },
        { "type" => "struct _avm_event_cmd_param_trigger", "name" => "avm_event_cmd_param_trigger", "select" => [ "avm_event_cmd_trigger" ]  },
        { "type" => "struct _avm_event_cmd_param_register", "name" => "avm_event_cmd_param_source_register", "select" => [ "avm_event_cmd_source_register" ]  },
        { "type" => "struct _avm_event_cmd_param_source_trigger", "name" => "avm_event_cmd_param_source_trigger", "select" => [ "avm_event_cmd_source_trigger" ]  },
        ]
};

$union{__powermanagment_status_union} = {
    "priority" => 10000,
	"name" => "__powermanagment_status_union",
	"selecttype" => "enum _powermanagment_status_type_dsl",
	"union" => [
        { "type" => "unsigned int", "name" => "dsl_status", "select" => [ "dsl_status" ]  },
    ]
};

$union{avm_event_powermanagment_remote_union} = {
    "priority" => 10000,
	"name" => "avm_event_powermanagment_remote_union",
	"selecttype" => "enum avm_event_powermanagment_remote_action",
	"union" => [
        { "type" => "struct avm_event_powermanagment_remote_ressourceinfo", "name" => "ressourceinfo", "select" => [ "avm_event_powermanagment_ressourceinfo"]  },
        { "type" => "char", "name" => "powermode", "anzahl" => 32, "select" => [ "avm_event_powermanagment_activatepowermode" ]},
    ]
};

$union{avm_event_data_union} = {
    "priority" => 10000,
	"name" => "avm_event_data_union",
	"selecttype" => "enum avm_event_data_type",
	"union" => [
        	{ "type" => "struct avm_event_push_button", "name" => "push_button", "select" => [ "avm_event_id_push_button"]  },
            { "type" => "struct _cpmac_event_struct", "name" => "cpmac", "select" => [ "avm_event_id_ethernet_connect_status"]  },
            { "type" => "struct avm_event_led_status", "name" => "led_status", "select" => [ "avm_event_id_led_status"]  },
            { "type" => "struct avm_event_led_info", "name" => "led_info", "select" => [ "avm_event_id_led_info"]  },
            { "type" => "struct avm_event_telefonprofile", "name" => "telefonprofile", "select" => [ "avm_event_id_telefonprofile"]  },
            { "type" => "struct avm_event_temperature", "name" => "temperature", "select" => [ "avm_event_id_temperature"]  },
            { "type" => "struct avm_event_powermanagment_status", "name" => "powermanagment_status", "select" => [ "avm_event_id_powermanagment_status"]  },
            { "type" => "struct avm_event_cpu_idle", "name" => "cpu_idle", "select" => [ "avm_event_id_cpu_idle"]  },
            { "type" => "struct avm_event_powermanagment_remote", "name" => "powermanagment_remote", "select" => [ "avm_event_id_powermanagment_remote"]  },
            { "type" => "struct avm_event_log", "name" => "id_log", "select" => [ "avm_event_id_log"]  },
            { "type" => "struct avm_event_remotepcmlink", "name" => "id_remotepcmlink", "select" => [ "avm_event_id_remotepcmlink"]  },
            { "type" => "struct avm_event_remotewatchdog", "name" => "id_remotewatchdog", "select" => [ "avm_event_id_remotewatchdog"]  },
            { "type" => "struct avm_event_rpc", "name" => "id_rpc", "select" => [ "avm_event_id_rpc"]  },
            { "type" => "struct avm_event_pm_info_stat", "name" => "pm_info_stat", "select" => [ "avm_event_id_pm_ressourceinfo_status"]  },
            { "type" => "struct avm_event_wlan", "name" => "wlan", "select" => [ "avm_event_id_wlan_client_status"]  },
            { "type" => "struct avm_event_telephony_missed_call", "name" => "telephony_missed_call", "select" => [ "avm_event_id_telephony_missed_call"]  },
            { "type" => "struct avm_event_telephony_missed_call", "name" => "telephony_tam_call", "select" => [ "avm_event_id_telephony_tam_call"]  },
            { "type" => "struct avm_event_telephony_missed_call", "name" => "telephony_fax_received", "select" => [ "avm_event_id_telephony_fax_received"]  },
            { "type" => "struct avm_event_telephony_missed_call", "name" => "telephony_incoming_call", "select" => [ "avm_event_id_telephony_incoming_call"]  },
            { "type" => "struct avm_event_firmware_update_available", "name" => "firmware_update_available", "select" => [ "avm_event_id_firmware_update_available"]  },
            { "type" => "struct avm_event_internet_new_ip", "name" => "internet_new_ip", "select" => [ "avm_event_id_internet_new_ip"]  },
            { "type" => "struct avm_event_smarthome_switch_status", "name" => "smarthome_switch_status", select => [ "avm_event_id_smarthome_switch_status" ] },
            { "type" => "struct avm_event_mass_storage_mount", "name" => "mass_storage_mount", "select" => [ "avm_event_id_mass_storage_mount"]  },
            { "type" => "struct avm_event_mass_storage_unmount", "name" => "mass_storage_unmount", "select" => [ "avm_event_id_mass_storage_unmount"]  }
	]
};

$union{avm_event_tffs_call_union} = {
    "priority" => 10000,
	"name" => "avm_event_tffs_call_union",
	"selecttype" => "enum avm_event_tffs_call_type",
	"union" => [
        	{ "type" => "struct avm_event_tffs_open",     "name" => "open",     "select" => [ "avm_event_tffs_call_open"]  },
        	{ "type" => "struct avm_event_tffs_close",    "name" => "close",    "select" => [ "avm_event_tffs_call_close"]  },
        	{ "type" => "struct avm_event_tffs_read",     "name" => "read",     "select" => [ "avm_event_tffs_call_read"]  },
        	{ "type" => "struct avm_event_tffs_write",    "name" => "write",    "select" => [ "avm_event_tffs_call_write"]  },
        	{ "type" => "struct avm_event_tffs_cleanup",  "name" => "cleanup",  "select" => [ "avm_event_tffs_call_cleanup"]  },
        	{ "type" => "struct avm_event_tffs_reindex",  "name" => "reindex",  "select" => [ "avm_event_tffs_call_reindex"]  },
        	{ "type" => "struct avm_event_tffs_info",     "name" => "info",     "select" => [ "avm_event_tffs_call_info"]  },
        	{ "type" => "struct avm_event_tffs_init",     "name" => "init",     "select" => [ "avm_event_tffs_call_init"]  },
        	{ "type" => "struct avm_event_tffs_deinit",   "name" => "deinit",   "select" => [ "avm_event_tffs_call_deinit"]  },
        	{ "type" => "struct avm_event_tffs_notify",   "name" => "notify",   "select" => [ "avm_event_tffs_call_notify"]  },
        	{ "type" => "struct avm_event_tffs_paniclog", "name" => "paniclog", "select" => [ "avm_event_tffs_call_paniclog"]  },
	]
};

$union{avm_event_message_union} = {
    "priority" => 10000,
    "name" => "avm_event_message_union",
    "selecttype" => "avm_event_msg_type",
    "union" => [
            { "type" => "struct avm_event_source_register", "name" => "source_register", "select" => [ "avm_event_source_register_type"]  },
            { "type" => "struct avm_event_source_unregister", "name" => "source_unregister", "select" => [ "avm_event_source_unregister_type"]  },
            { "type" => "struct avm_event_source_notifier", "name" => "source_notifier", "select" => [ "avm_event_source_notifier_type"]  },
            { "type" => "struct avm_event_remote_source_trigger_request", "name" => "remote_source_trigger_request", "select" => [ "avm_event_remote_source_trigger_request_type"]  },
            { "type" => "struct avm_event_ping", "name" => "ping", "select" => [ "avm_event_ping_type"]  },
            { "type" => "struct avm_event_tffs", "name" => "tffs", "select" => [ "avm_event_tffs_type"]  },
    ]
};

$union{avm_event_internet_new_ip_param} = {
    "priority" => 10000,
	"name" => "avm_event_internet_new_ip_param",
	"selecttype" => "enum avm_event_internet_new_ip_param_sel",
	"union" => [
        	{ "type" => "unsigned char",  "anzahl" => "4",  "name" => "ipv4", "select" => [ "avm_event_internet_new_ip_v4" ] },
        	{ "type" => "unsigned char",  "anzahl" => "16",  "name" => "ipv6", "select" => [ "avm_event_internet_new_ip_v6" ] }
    ]
};

$union{avm_event_telephony_call_params} = {
    "priority" => 10000,
	"name" => "avm_event_telephony_call_params",
	"selecttype" => "enum avm_event_telephony_param_sel",
	"union" => [
        	{ "type" => "struct avm_event_telephony_string", "name" => "string",    "select" => [ "avm_event_telephony_params_name", "avm_event_telephony_params_msn_name", "avm_event_telephony_params_portname", "avm_event_telephony_params_tam_path" ]  },
        	{ "type" => "unsigned char",  "anzahl" => "32",  "name" => "number",    "select" => [ "avm_event_telephony_params_calling", "avm_event_telephony_params_called" ] },
            { "type" => "unsigned int", "name" => "duration", "select" => [ "avm_event_telephony_params_duration" ] },
            { "type" => "unsigned char", "name" => "port", "select" => [ "avm_event_telephony_params_port" ] },
            { "type" => "unsigned int", "name" => "id", "select" => [ "avm_event_telephony_params_id" ] }
    ]
};

##########################################################################################
#
##########################################################################################
sub input_union_init {
    my $count = 0; 
    foreach my $i ( keys %union ) {
        $count++;
    }
    print STDERR "[avm_event_input_union_init] " . $count . " unions defined\n";
}

1;
