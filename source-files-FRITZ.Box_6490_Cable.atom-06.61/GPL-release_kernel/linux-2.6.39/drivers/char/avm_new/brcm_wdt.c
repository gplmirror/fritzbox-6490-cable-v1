/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2015 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_AVM_WATCHDOG)
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/types.h>
#include <linux/smp.h>
#include <linux/init.h>
#include <linux/clk.h>
#include <linux/err.h>
#include <linux/io.h>
#include <linux/interrupt.h>
#include <linux/clk.h>

#include <asm/mach_avm.h>

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
void ar7wdt_hw_init(void) {
   printk("[ar7wdt_hw_init]: Watchdog functions stubbed for first ...\n");
}

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
void ar7wdt_hw_deinit(void) {
   printk("[ar7wdt_hw_deinit]: Watchdog functions stubbed for first ...\n");
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void ar7wdt_hw_reboot(void) {
   printk("[ar7wdt_hw_reboot]: Watchdog functions stubbed for first ...\n");
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void ar7wdt_hw_trigger(void) {
}

#endif/*--- #if defined(CONFIG_AVM_WATCHDOG) ---*/
