/*
* File Name: aep_host_ipc.h
*/

/*
  This file is provided under a dual BSD/GPLv2 license.  When using or
  redistributing this file, you may do so under either license.

  GPL LICENSE SUMMARY

  Copyright(c) 2007-2012 Intel Corporation. All rights reserved.

  This program is free software; you can redistribute it and/or modify
  it under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
  The full GNU General Public License is included in this distribution
  in the file called LICENSE.GPL.

  Contact Information:
  Intel Corporation
  2200 Mission College Blvd.
  Santa Clara, CA  97052

*/
#ifndef AEP_HOST_IPC_H
#define AEP_HOST_IPC_H
/**
 * DeInitialize IPC communication
 *
 * Function:	aep_host_ipc_deinitialize
 *  Description:
 *  This is used free all compoenents which were allocated
 *	during initialization
 * Parameters:
 *
 */

aep_result_t aep_host_ipc_deinitialize(void);
/**
 * Initialize IPC communication
 *
 * Function:	aep_host_ipc_initialize
 *  Description:
 *  This is used for initialize IPC comunication parameter with AEP
 * Parameters:
 *
 */

aep_result_t aep_host_ipc_initialize(struct tasklet_struct *schedule_tasklet,
										void *base_addr);

aep_result_t aep_host_send_ipc(aep_fw_cmd_t ipc_cmd, aep_ipc_sizes_t io_sizes,
								ipl_t *ipl, opl_t *opl);

aep_result_t aep_host_send_ipc_wq(aep_fw_cmd_t fw_cmd_ipc,
									ipl_t *ipl, opl_t *opl);

aep_result_t aep_host_send_response(aep_fw_cmd_t ipc_cmd,
											aep_ipc_sizes_t io_sizes,
											ipl_t *ipl);
aep_result_t aep_host_read_ipc_msg(void *opl, uint32_t offset,
							uint32_t opl_size);

aep_result_t aep_host_send_suspend_to_fw(void);

#endif
