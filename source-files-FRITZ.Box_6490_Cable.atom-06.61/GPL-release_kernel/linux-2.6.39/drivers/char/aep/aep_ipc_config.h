/*

  This file is provided under a dual BSD/GPLv2 license.  When using or
  redistributing this file, you may do so under either license.

  GPL LICENSE SUMMARY

  Copyright(c) 2007-2012 Intel Corporation. All rights reserved.

  This program is free software; you can redistribute it and/or modify
  it under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
  The full GNU General Public License is included in this distribution
  in the file called LICENSE.GPL.

  Contact Information:

  */

#ifndef __AEP_IPCLIB_CONFIG_H__
#define __AEP_IPCLIB_CONFIG_H__

#define AEP_PV_CONTROL_REG 0x00

#define CONFIG_IPC_SYNC_MESSAGE_BUF_SIZE   64


#define IPC_HOST_SLEEP_INTERVAL              100
/* AEP IPC status */
#define CONFIG_AEP_IPC_REGISTER_STATUS_OFFSET         0x140C
/* Host IPC status register */
#define CONFIG_HOST_IPC_REGISTER_STATUS_OFFSET        0x1404

#define CONFIG_IPC_REGISTER_STAT_AEP_READY     0x01
#define CONFIG_IPC_REGISTER_STAT_AEP_DONE      0x02
#define REG_AEP_DBL                 0x1408
#define REG_HOST_DBL                0x1400

/* Synchronous (one at a time) message block (no queueing) */
#define CONFIG_IPC_SYNC_MESSAGE_OFFSET          0x12C0
#define CONFIG_IPC_READ_SYNC_MESSAGE_OFFSET          0x1280

#define AEP_DBL_READY      0x3
#define AEP_DBL_DONE       0x2
#define AEP_ENABLED        0x1
#define AEP_IPC_CMD_ID_COMPLETE_FLAG 0x80000000


typedef enum {
	AEP_IPC_RET_SUCCESS         = 0,
	AEP_IPC_RET_ERROR         = -1,
	AEP_IPC_RET_WAIT_TIMEOUT    = -2
} aep_ipc_ret_t;

typedef struct {
	uint32_t commandId;
	uint32_t data_payload[16];
} ipc_synchronous_message;

#endif
