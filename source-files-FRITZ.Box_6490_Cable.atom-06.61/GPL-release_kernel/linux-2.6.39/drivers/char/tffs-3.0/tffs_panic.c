/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2007 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <linux/reboot.h>
#include <asm/fcntl.h>
#include <asm/ioctl.h>
#include <asm/io.h>
/*--- #include <linux/devfs_fs_kernel.h> ---*/
#include <linux/semaphore.h>
#include <asm/errno.h>
#include <linux/wait.h>
#include <linux/tffs.h>
#include <linux/zlib.h>
#include <linux/vmalloc.h>
#include <linux/rtc.h>
#if defined(CONFIG_PROC_FS)
#include <linux/file.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#endif /*--- #if defined(CONFIG_PROC_FS) ---*/
#include "tffs_local.h"
#if defined(CONFIG_AVM_WATCHDOG)
#include <linux/ar7wdt.h>
#endif/*--- #if defined(CONFIG_AVM_WATCHDOG) ---*/

#if defined(CONFIG_MACH_AR934x)
extern int ath_restore_log_from_ram(char *txtbuf, unsigned int txtlen);
extern void ath_inval_log_from_ram(void);
#endif/*--- #if defined(CONFIG_MACH_AR934x) ---*/

static struct tffs_fops_handle *panic_handle = NULL;

unsigned int tffs_spi_mode = 0;
unsigned int tffs_panic_log_suppress = 0;
EXPORT_SYMBOL(tffs_panic_log_suppress);

/*--------------------------------------------------------------------------------*\
 * Schreiben im Panic-Kontext
\*--------------------------------------------------------------------------------*/
void tffs_panic_log_open(void)
{
    if(panic_handle == NULL){
        panic_handle = tffs_open_panic();
    }
}
EXPORT_SYMBOL(tffs_panic_log_open);
/*--------------------------------------------------------------------------------*\
 * Schreiben im Panic-Kontext
\*--------------------------------------------------------------------------------*/
void tffs_panic_log_write(char *buffer, unsigned int len)
{
    static loff_t off __attribute__((unused));

    if(panic_handle != NULL){
        tffs_write_kern(panic_handle, buffer, len, &off);
    }
}
EXPORT_SYMBOL(tffs_panic_log_write);

/*--------------------------------------------------------------------------------*\
 * Schreiben im Panic-Kontext
\*--------------------------------------------------------------------------------*/
void tffs_panic_log_close(void)
{
    if(panic_handle != NULL){
        tffs_release_kern(panic_handle);
        panic_handle = NULL;
    }
}
EXPORT_SYMBOL(tffs_panic_log_close);

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static unsigned int get_act_time(void) {
    struct timeval now;
    do_gettimeofday(&now);
    return (unsigned int)(now.tv_sec);
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static char *time_to_ascii(unsigned long local_time, char *buf, int len, char *prefix) {
    static char *month[] = { "Jan",  "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    static char *day[]   = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    struct rtc_time tm;
    rtc_time_to_tm(local_time, &tm);
    snprintf(buf, len, "%s %s %02d %02d:%02d:%02d %02d UTC %s", day[tm.tm_wday],  month[tm.tm_mon], tm.tm_mday,
                                                  tm.tm_hour, tm.tm_min, tm.tm_sec, tm.tm_year + 1900,
                                                  prefix
                                                  );
    return buf;
}
/*--------------------------------------------------------------------------------*\
 * maxsize of buf: 14 + 10 + 2 + 2 + 2  = 30
\*--------------------------------------------------------------------------------*/
static char *human_readable_time(char *buf, unsigned long buf_size, unsigned long time) {
    unsigned long seconds, minutes, hours, days;

    seconds = time % 60; time /= 60;
    minutes = time % 60; time /= 60;
    hours   = time % 24; time /= 24;
    days    = time;
    snprintf(buf, buf_size, "%lu d %lu h %lu min %lu s", days, hours, minutes, seconds);
    return buf;
}

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
void tffs_panic_log_printkbuf(void) {
    if(tffs_panic_log_suppress == 0) {
        unsigned long printk_get_buffer(char **p_log_buf, unsigned long *p_log_end, unsigned long *p_anzahl);
        char *buf;
        unsigned long end;
        unsigned long anzahl;
        unsigned long len = printk_get_buffer(&buf, &end, &anzahl);
        struct timespec uptime;
        char htime[32], huptime[128];
        char time_stamp_buf[sizeof("UPTIME: \n") + sizeof(htime) + sizeof(huptime) + 10 + sizeof("\n( - panic on )\nPANIC LOG VERSION 2.0\n")];
        unsigned long time_stamp_buf_len;

        tffs_panic_log_suppress = 1;
        tffs_panic_log_open();
        do_posix_clock_monotonic_gettime(&uptime);
        monotonic_to_bootbased(&uptime);
        time_stamp_buf_len = snprintf(time_stamp_buf, sizeof(time_stamp_buf), "UPTIME: %lu\n(%s - panic on %s)\nPANIC LOG VERSION 2.0\n",
                                                                            (unsigned long)uptime.tv_sec,
                                                                            human_readable_time(huptime, sizeof(huptime), (unsigned long)uptime.tv_sec),
                                                                            time_to_ascii((unsigned long)get_act_time(), htime, sizeof(htime), "")
                                                                            );
        tffs_panic_log_write(time_stamp_buf, time_stamp_buf_len);
#if defined(CONFIG_AVM_WATCHDOG)
        AVM_WATCHDOG_emergency_retrigger();
#endif
        if(anzahl < len) {  /*--- alles im Buffer ---*/
            tffs_panic_log_write(buf, anzahl);
        } else {
            tffs_panic_log_write(buf + end, len - end);
#if defined(CONFIG_AVM_WATCHDOG)
            AVM_WATCHDOG_emergency_retrigger();
#endif
            tffs_panic_log_write(buf, end);
        }
        tffs_panic_log_close();
    }
}

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
void tffs_panic_log_register_spi(void)
{
    tffs_spi_mode = 1;
}

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static unsigned int calc_sum_and_correct(unsigned char *buf, int buf_len, int correct)
{
    unsigned int i, sum = 0;

    for(i = 0; i < buf_len; i++) {
        if(correct && (buf[i] == 0)){
            buf[i] = ' ';   /*--- correct buffer (support null-teminated string) ---*/
        }

        sum += buf[i];
    }

    return sum ^ (buf_len << 16);
}

#define MAX_LOG_IDS      4
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int map_log_id_to_index(unsigned int id)
{
    switch(id) {
        case FLASH_FS_ID_PANIC_LOG:  return 0x0;
        case FLASH_FS_ID_PANIC2_LOG: return 0x1;   
        case FLASH_FS_ID_CRASH_LOG:  return 0x2;
        case FLASH_FS_ID_CRASH2_LOG: return 0x3;
    }

    return MAX_LOG_IDS;
}

static unsigned int log_read_multiple[MAX_LOG_IDS];

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static ssize_t tffs_panic_log_read(unsigned char *buf, int buf_len, unsigned int id)
{
    struct tffs_fops_handle *handle;
    loff_t offp = 0;
    ssize_t read;

    handle = tffs_open_kern(id, 0);
    if(handle == NULL) {
        printk(KERN_ERR"%s tffs_open_kern failed\n", __func__);
        return -EINVAL;
    }

    read = tffs_read_kern(handle, buf, buf_len, &offp);
    /*--- printk(KERN_INFO"%s tffs_read ret=%d\n", __func__, ret); ---*/
    tffs_release_kern(handle);

    return read;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int paniclog_show(struct seq_file *seq, void *data __attribute__((unused)))
{
    /*--- printk(KERN_INFO"%s %p %p\n", __func__, seq->private, data); ---*/
	if(seq->private) {
        seq_printf(seq, "%s", (char *)seq->private);
    }
    return 0;
}

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
struct _time_checksum {
    unsigned int cs;
    unsigned int ts;        /* timestamp on read */
#define READ_BY_CR          0x1
#define READ_BY_SD          0x2
#define FIRST_READ_BY_SD    0x4     /* first read by SD */
    unsigned int readmask;
};
#define SKIP_UNTIL_CHAR(p, sign) while(*p && (*p != sign)) p++

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline unsigned COUNT_UNTIL_CHAR(const char *p, unsigned char sign, unsigned char count_sign)
{
    unsigned int count = 0;

    while(*p && (*p != sign)) {
        if(*p == count_sign) {
            count++;
        }
        p++;
    }

    return count;
}

/*--------------------------------------------------------------------------------*\
 * return: gefuelltes checksum
 * [id]cs,ts,readmask,[id2]cs,ts,readmask
\*--------------------------------------------------------------------------------*/
void parse_crash_env(const char *crashp, struct _time_checksum checksum[])
{
    memset(checksum, 0, MAX_LOG_IDS * sizeof(struct _time_checksum));

    while(*crashp) {
        unsigned int id, val, val1, val2;
        if(*crashp == '[') {
            sscanf(++crashp,"%x]%x,%x,%x", &id, &val, &val1, &val2);
            if(id < MAX_LOG_IDS) {
                unsigned int count = COUNT_UNTIL_CHAR(crashp, '[', ',');
                checksum[id].cs       = val;
                checksum[id].ts       = val1;
                if(count >= 1) {
                    checksum[id].ts       = val1;
                }
                if(count >= 2) {
                    checksum[id].readmask = val2;
                }
                /*--- printk(KERN_INFO"%s: %d(count=%d): %x %x %x\n", __func__, id, count, checksum[id].cs, checksum[id].ts,checksum[id].readmask); ---*/
            }
            SKIP_UNTIL_CHAR(crashp, '[');
        } else {
            return;
        }
    }
}

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static void set_crash_env(char *crashp, int len, struct _time_checksum checksum[])
{
    unsigned int written, i;

    for(i = 0; i < MAX_LOG_IDS; i++) {
        if(len > 0) {
            written = snprintf(crashp, len, "[%x]%x,%x,%x", i, checksum[i].cs, checksum[i].ts, checksum[i].readmask);
            len -= written, crashp += written;
        }
    }
}

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int __log_proc_open(struct inode *inode __attribute__((unused)), struct file *file, unsigned int crashreport, int id) {
    char crashenv[128];
    char timebuf[128];
    const char *timestart = "";
    int buf_len = (0x1 << 17) - 64; /*--- ReserveTimestamp ---*/
    int len = buf_len;
    struct _time_checksum saved_checksum[MAX_LOG_IDS];
    unsigned int new_read_mask = 0;
    unsigned int map_idx = map_log_id_to_index(id);
    char *buf = NULL;
    struct tffs_core_handle *handle;
    int result;

    /*--- printk(KERN_ERR"%s: id=%d map_idx=%x %s\n", __func__, id, map_idx, crashreport ? "cr" : "sd"); ---*/
    if(map_idx >= MAX_LOG_IDS) {
        /*--- printk("%s: error: id=%d too big\n", __func__, map_idx); ---*/
        return -ENODATA;
    }

    /*--- printk(KERN_ERR"%s: id=%d map_idx=%x %s f:_flags=%x\n", __func__, id, map_idx, crashreport ? "cr" : "sd", file->f_flags); ---*/
    if(file->f_flags & O_WRONLY) {
        unsigned int mapvalue;
        mapvalue  = crashreport ? READ_BY_CR : READ_BY_SD;
        mapvalue |= map_idx << 8;
        file->private_data = (void *)mapvalue;
        return 0;
    }

    memset(saved_checksum, 0, sizeof(saved_checksum));
    buf = kmalloc(len, GFP_KERNEL);
    if(buf == NULL) {
        result = -ENOMEM;
        goto err_out;
    }

    len = sizeof(crashenv);
    crashenv[0] = 0;

    handle = TFFS3_Open(FLASH_FS_CRASH, tffs3_mode_read);
    if(IS_ERR_OR_NULL(handle)){
        result = -ENODEV;
        goto err_out;
    }

    result = TFFS3_Read(handle, crashenv, &len);
    if(result == 0){
        parse_crash_env(crashenv, &saved_checksum[0]);
    }
    TFFS3_Close(handle);

    buf[0] = 0;
    if((len = tffs_panic_log_read(buf, buf_len, id)) > 0) {
        unsigned int new_sum = calc_sum_and_correct(buf, len, 1);
        /*--- printk(KERN_INFO"%s: sum=%x len=%d\n", __func__, new_sum, len); ---*/
        if(new_sum != saved_checksum[map_idx].cs) {
            /*--- new panic-log: set struct ---*/
            new_read_mask                    = crashreport ? READ_BY_CR : (READ_BY_SD | FIRST_READ_BY_SD);
            saved_checksum[map_idx].readmask = 0;
            saved_checksum[map_idx].ts       = get_act_time();
            saved_checksum[map_idx].cs       = new_sum;
        } else {
            new_read_mask                    = crashreport ? READ_BY_CR : READ_BY_SD;
        }
    } else {
        len = 0;
    }

#if defined(CONFIG_MACH_AR934x) || defined(CONFIG_MACH_QCA953x)
    if(id == FLASH_FS_ID_PANIC_LOG) {
        unsigned int sram_len = ath_restore_log_from_ram(buf, buf_len);
        if(sram_len) {
            unsigned int new_sram_sum;
            map_idx = 1;
            /*--- ... and prefer sram-log---*/
            len = sram_len;
            new_sram_sum = calc_sum_and_correct(buf, len, 1);
            /*--- printk(KERN_INFO"%s: prefer sram-trace len=%d", __func__, sram_len); ---*/
            if(new_sram_sum != saved_checksum[map_idx].cs) {
                struct timeval now;
                /*--- new sram-panic-log: clear counter ---*/
                new_read_mask                    = crashreport ? READ_BY_CR : (READ_BY_SD | FIRST_READ_BY_SD);
                saved_checksum[map_idx].readmask = 0;
                saved_checksum[map_idx].ts       = get_act_time();
                saved_checksum[map_idx].cs       = new_sram_sum;
            } else {
                new_read_mask                    = crashreport ? READ_BY_CR : READ_BY_SD;
            }
        }
    }
#endif/*--- #if defined(CONFIG_MACH_AR934x) ---*/

    if((len == 0)
       || (((saved_checksum[map_idx].readmask & new_read_mask) == new_read_mask)
            && !((log_read_multiple[map_idx] & new_read_mask) == new_read_mask)))
    {
        /*--- check if sent ---*/
        result = -ENODATA;
        goto err_out;
    }

    saved_checksum[map_idx].readmask |= new_read_mask;
    if(saved_checksum[map_idx].ts){
        timestart = time_to_ascii(saved_checksum[map_idx].ts, timebuf, sizeof(timebuf), (saved_checksum[map_idx].readmask & FIRST_READ_BY_SD) ? "by support data" : "by crash report");
    }

    sprintf(&buf[len], "-----\n(first) sent on: %s\n", timestart);
    set_crash_env(crashenv, sizeof(crashenv), saved_checksum);
/*---   printk(KERN_INFO"%s: '%s'\n", __func__, crashenv); ---*/

    handle = TFFS3_Open(FLASH_FS_CRASH, tffs3_mode_write);
    if(IS_ERR_OR_NULL(handle)){
        result = -ENODEV;
        goto err_out;
    }
    TFFS3_Write(handle, crashenv, strlen(crashenv) + 1, 1);
    TFFS3_Close(handle);

    return single_open(file, paniclog_show, buf);

err_out:
    if(buf != NULL){
       kfree(buf);
    }

    return result;

}

#define __generic_proc_log_open(name,  type, id)  static int name(struct inode *inode, struct file *file) {  return __log_proc_open(inode, file, type, id); }
  /*--- Crashreports ---*/
__generic_proc_log_open(paniclog_open_cr,  1, FLASH_FS_ID_PANIC_LOG); 
__generic_proc_log_open(crashlog_open_cr,  1, FLASH_FS_ID_CRASH_LOG); 
  /*---  Supportdata read every time possible ---*/
__generic_proc_log_open(paniclog_open_sd,  0, FLASH_FS_ID_PANIC_LOG); 
__generic_proc_log_open(crashlog_open_sd,  0, FLASH_FS_ID_CRASH_LOG); 

  /*--- Crashreports ---*/
__generic_proc_log_open(panic2log_open_cr, 1, FLASH_FS_ID_PANIC2_LOG); 
__generic_proc_log_open(crash2log_open_cr, 1, FLASH_FS_ID_CRASH2_LOG); 
  /*---  Supportdata read every time possible ---*/
__generic_proc_log_open(panic2log_open_sd, 0, FLASH_FS_ID_PANIC2_LOG); 
__generic_proc_log_open(crash2log_open_sd, 0, FLASH_FS_ID_CRASH2_LOG); 

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static ssize_t log_proc_write(struct file *file, const char *buffer, size_t count, loff_t *offset __maybe_unused) {
    unsigned char procfs_buffer[64];
    size_t procfs_buffer_size;
	unsigned int id, report;

	/*--- von wo kommen wir: ---*/
	id	   = ((unsigned int) file->private_data) >> 8;
	report = ((unsigned int)file->private_data)  & 0xFF;
	if(id >= MAX_LOG_IDS) {
		return count;
	}
    if (count >= sizeof(procfs_buffer)) {
       procfs_buffer_size = sizeof(procfs_buffer) - 1;
    } else {
       procfs_buffer_size = count;
    }
    /* write data to the buffer */
    if ( copy_from_user(procfs_buffer, buffer, procfs_buffer_size - 1) ) {
        return -EFAULT;
    }
    procfs_buffer[procfs_buffer_size] = 0;
	if(strstr(procfs_buffer, "readmultiple")) {
		log_read_multiple[id] |= report;
	} else if(strstr(procfs_buffer, "readonce")) {
		log_read_multiple[id] &= ~report;
	} else {
		printk(KERN_INFO"unknown option: use readmultiple or readonce\n");
	}
	/*--- 	printk(KERN_INFO"%s: '%s' %p log_read_multiple[%d]=%x (report=%x)\n", __func__, procfs_buffer, file->private_data, id, log_read_multiple[id], report); ---*/
    return count;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int log_proc_release(struct inode *inode, struct file *file) {
    if(!(file->f_flags & O_WRONLY)) {
		/*--- printk(KERN_INFO"%s: free after read\n", __func__); ---*/
		return seq_release_private(inode, file); /*---  gibt txtbuf frei ----*/
	}
	/*--- 	printk(KERN_INFO"%s no free\n", __func__); ---*/
	return 0;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static struct _logproc {
    const char *dir;
    const char *name;
    struct file_operations fops;
    struct proc_dir_entry *dir_entry;
} logproc[] = {
    { dir: "avm/log_cr", name: "panic",  fops: {open: paniclog_open_cr, write: log_proc_write, read: seq_read, llseek: seq_lseek, release: log_proc_release }}, 
    { dir: "avm/log_cr", name: "crash",  fops: {open: crashlog_open_cr, write: log_proc_write, read: seq_read, llseek: seq_lseek, release: log_proc_release }}, 
    { dir: "avm/log_cr", name: "panic2", fops: {open: panic2log_open_cr,write: log_proc_write, read: seq_read, llseek: seq_lseek, release: log_proc_release }},
    { dir: "avm/log_cr", name: "crash2", fops: {open: crash2log_open_cr,write: log_proc_write, read: seq_read, llseek: seq_lseek, release: log_proc_release }}, 
    { dir: "avm/log_sd", name: "panic",  fops: {open: paniclog_open_sd, write: log_proc_write, read: seq_read, llseek: seq_lseek, release: log_proc_release }}, 
    { dir: "avm/log_sd", name: "crash",  fops: {open: crashlog_open_sd, write: log_proc_write, read: seq_read, llseek: seq_lseek, release: log_proc_release }}, 
    { dir: "avm/log_sd", name: "panic2", fops: {open: panic2log_open_sd, write: log_proc_write, read: seq_read, llseek: seq_lseek, release: log_proc_release }}, 
    { dir: "avm/log_sd", name: "crash2", fops: {open: crash2log_open_sd, write: log_proc_write, read: seq_read, llseek: seq_lseek, release: log_proc_release }},
    /*--- compatible to the "old" solution: ---*/
    { dir: NULL, name: "avm_panic_cr",  fops: {open: paniclog_open_cr,  write: log_proc_write, read: seq_read, llseek: seq_lseek, release: seq_release_private /*--- gibt txtbuf frei ---*/}}, 
    { dir: NULL, name: "avm_panic_sd",  fops: {open: paniclog_open_sd,  write: log_proc_write, read: seq_read, llseek: seq_lseek, release: seq_release_private /*--- gibt txtbuf frei ---*/}}, 
};
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int __init prepare_log_proc(void)
{
    struct proc_dir_entry *dir_entry = NULL;
    const char *lastdir = "";
    unsigned int i;

    /*--- printk(KERN_ERR"%s\n", __FUNCTION__); ---*/
    for(i = 0; i < ARRAY_SIZE(logproc); i++) {
#if LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 32)
        logproc[i].fops.owner = THIS_MODULE;
#endif/*--- #if LINUX_VERSION_CODE < KERNEL_VERSION(2, 6, 32) ---*/
        if(logproc[i].dir && strcmp(lastdir, logproc[i].dir)) {
            dir_entry = proc_mkdir(logproc[i].dir, NULL);
            lastdir   = logproc[i].dir;
        }
#if !defined(CRASHPANIC_LOG_PER_REMOTE_EVENT_SOURCE)
        if(logproc[i].dir == NULL) {
            if(!proc_create(logproc[i].name, 0666, NULL, &logproc[i].fops)) {
                printk(KERN_ERR"%s can't proc_create(%s)\n", __func__, logproc[i].name);
            }
        } else if(dir_entry) {
            logproc[i].dir_entry = dir_entry;
            if(!proc_create(logproc[i].name, 0666, dir_entry, &logproc[i].fops)) {
                printk(KERN_ERR"%s can't proc_create(%s)\n", __func__, logproc[i].name);
            }
        }
#endif/*--- #if !defined(CRASHPANIC_LOG_PER_REMOTE_EVENT_SOURCE) ---*/
    }

    return 0;
}
late_initcall(prepare_log_proc);
