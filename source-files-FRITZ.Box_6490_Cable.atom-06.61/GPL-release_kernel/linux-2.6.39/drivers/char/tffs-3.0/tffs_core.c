/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2004-2014 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/
#ifdef CONFIG_SMP
#define __SMP__
#endif /*--- #ifdef CONFIG_SMP ---*/

#include <linux/module.h>
#include <linux/version.h>
#include <linux/slab.h>
#include <asm/fcntl.h>
/*--- #include <linux/devfs_fs_kernel.h> ---*/
#include <linux/mtd/mtd.h>
#include <linux/tffs.h>
#include <linux/sched.h>
#include <linux/errno.h>
#include <linux/vmalloc.h>
#include <linux/zlib.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/wait.h>
#include <linux/mutex.h>
#include <linux/io.h>
#include <linux/kernel.h>
#include <linux/avm_event.h>

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
/*--- #define TFFS_DEBUG ---*/
#include "tffs_local.h"
 
/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
struct tffs3_cfg_funcs cfg_funcs = {
#if defined(CONFIG_TFFS_DEV_MTDNOR)
        .mtdnor = TFFS3_NOR_Configure,
#endif
#if defined(CONFIG_TFFS_DEV_MTDNAND)
        .mtdnand = TFFS3_NAND_Configure,
#endif
#if defined(CONFIG_TFFS_DEV_BDEV)
        .bdev = TFFS3_BDEV_Configure,
#endif
#if defined(CONFIG_TFFS_DEV_CACHE)
        .cache = TFFS3_CACHE_Configure,
#endif
#if defined(CONFIG_TFFS_DEV_REMOTE)
        .remote = TFFS3_REMOTE_Configure,
        .server = TFFS3_SERVER_Configure,
#endif
#if defined(CONFIG_TFFS_DEV_LEGACY)
        .legacy = TFFS3_LGCY_Configure,
#endif
};


#if defined(CONFIG_TFFS_DEV_LEGACY)
extern int tffs_mtd[2];
#endif

static tffs_device TFFS_device;

static int lock_device(tffs_device *device, struct tffs_core_handle *handle)
{
    int result;

    result = 0;

    // if device is in panic mode, only the panic mode handle may access it.
    if(device->in_panic_mode && handle->mode != tffs3_mode_panic){
        result = -EBUSY;
        goto err_out;
    }

    if(handle->mode == tffs3_mode_panic){
        result = down_trylock(&(device->lock));
    } else {
        result = down_interruptible(&(device->lock));
    }

    if(result != 0){
        result = (handle->mode == tffs3_mode_panic) ? -EBUSY : result;
        goto err_out;
    }

err_out:
    return result;
}

static void notify_cb(void *priv __attribute__((unused)),
                        unsigned int id, enum tffs3_notify_event event)
{
    pr_debug("[%s] Called for ID 0x%x event %s\n", __func__, id,
                                                          event == tffs3_notify_clear  ? "clear" :
                                                          event == tffs3_notify_update ? "update" :
                                                          event == tffs3_notify_reinit ? "reinit" :
                                                                                         "unknown");
    if(TFFS_device.server_state == tffs3_module_running && TFFS_device.server.notify != NULL){
        TFFS_device.server.notify(TFFS_device.server.priv, id, event);
    }
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
int TFFS3_Early_Init(void)
{
    sema_init(&TFFS_device.lock, 1);
    TFFS_device.initialised = 1;

    pr_err("[%s] called\n", __func__);

    return 0;
}
arch_initcall(TFFS3_Early_Init);

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
int TFFS3_Register_NAND(struct mtd_info *mtd)
{
    int result;

    pr_err("[%s] Called\n", __func__);

    if(!TFFS_device.initialised){
        pr_err("[%s] TFFS_device not initialised\n", __func__);
        return -EAGAIN;
    }

    result = -ENODEV;
    down(&TFFS_device.lock);
    if(TFFS_device.backend_state == tffs3_module_init){
        if(cfg_funcs.mtdnand){
            result = cfg_funcs.mtdnand(&TFFS_device.backend, mtd);
            if(result == 0){
                TFFS_device.backend_state  = tffs3_module_configured;
            }
        } else {
            pr_err("[%s] No config function registered for NAND\n", __func__);
        }
    } else {
        pr_err("[%s] Backing device already registered.\n", __func__);
        result = -EINVAL;
    }
    up(&TFFS_device.lock);

    return result;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
int TFFS3_Register_LGCY(unsigned int mtd0, unsigned int mtd1)
{
    int result;

    pr_err("[%s] Called\n", __func__);

    if(!TFFS_device.initialised){
        pr_err("[%s] TFFS_device not initialised\n", __func__);
        return -EAGAIN;
    }

    result = -ENODEV;
    down(&TFFS_device.lock);
    if(TFFS_device.backend_state == tffs3_module_init){
        if(cfg_funcs.legacy){
            result = cfg_funcs.legacy(&TFFS_device.backend, mtd0, mtd1);
            if(result == 0){
                TFFS_device.backend_state  = tffs3_module_configured;
            }
        } else {
            pr_err("[%s] No config function registered for LGCY\n", __func__);
        }
    } else {
        pr_err("[%s] Backing device already registered.\n", __func__);
        result = -EINVAL;
    }
    up(&TFFS_device.lock);

    return result;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
int TFFS3_Register_REMOTE(unsigned int node_id)
{
    int result;

    pr_err("[%s] Called\n", __func__);

    if(!TFFS_device.initialised){
        pr_err("[%s] TFFS_device not initialised\n", __func__);
        return -EAGAIN;
    }

    result = -ENODEV;
    down(&TFFS_device.lock);
    if(TFFS_device.backend_state == tffs3_module_init){
        if(cfg_funcs.remote){
            result = cfg_funcs.remote(&TFFS_device.backend, node_id);
            if(result == 0){
                TFFS_device.backend_state  = tffs3_module_configured;
            }
        } else {
            pr_err("[%s] No config function registered for remote access\n", __func__);
        }
    } else {
        pr_err("[%s] Backing device already registered.\n", __func__);
        result = -EINVAL;
    }
    up(&TFFS_device.lock);

    return result;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
int TFFS3_Register_SERVER(unsigned int node_id)
{
    int result;

    pr_err("[%s] Called\n", __func__);
    result = 0;

    if(!TFFS_device.initialised){
        pr_err("[%s] TFFS_device not initialised\n", __func__);
        return -EAGAIN;
    }

    result = -ENODEV;
    down(&TFFS_device.lock);
    if(TFFS_device.server_state == tffs3_module_init){
        if(cfg_funcs.server){
            result = cfg_funcs.server(&TFFS_device.server, node_id);
            if(result == 0){
                TFFS_device.server_state  = tffs3_module_configured;
            }
        } else {
            pr_err("[%s] No config function registered for remote server\n", __func__);
        }
    } else {
        pr_err("[%s] Server already registered.\n", __func__);
        result = -EINVAL;
    }
    up(&TFFS_device.lock);

    return result;
}

int TFFS3_Register_Panic(enum tffs3_types type, union tffs3_panic_funcs *funcs)
{
    int result = 0;

    switch(type){
    case tffs3_type_mtdnand:
        TFFS_device.nand_fn = funcs->nand;
        break;
    case tffs3_type_mtdnor:
        TFFS_device.mtdnor_fn = funcs->mtdnor;
        break;
    default:
        pr_err("[%s] can not register panic funtions for unknown device type 0x%x\n", __func__, type);
        result = -EINVAL;
        break;
    }

    return result;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
int TFFS3_Init(void)
{
    int result;

    result = -ENODEV;

#if defined(CONFIG_TFFS_DEV_LEGACY)
    if(TFFS_device.backend_state == tffs3_module_init){
        pr_err("[%s] No storage module registered, trying legacy fallback\n", __func__);
        result = TFFS3_Register_LGCY(tffs_mtd[0], tffs_mtd[1]);
    }
#endif

    down(&TFFS_device.lock);

    if(TFFS_device.backend_state == tffs3_module_configured){

#if defined(CONFIG_TFFS_DEV_CACHE)
        result = TFFS3_CACHE_Configure(&TFFS_device.cache, &TFFS_device.backend);
        if(result != 0){
            pr_err("[%s] Configuration of caching module failed with status %d\n", __func__, result);
            goto err_out;
        }
        TFFS_device.cache_state = tffs3_module_configured;
#else
        // caching not enabled, use backend directly instead
        TFFS_device.cache = TFFS_device.backend;
#endif // defined(CONFIG_TFFS_DEV_CACHE)

        if(TFFS_device.cache.setup == NULL){
            pr_err("[%s] No cache/backend setup function registered.\n", __func__);
            result = -ENODEV;
            goto err_out;
        }

        result = TFFS_device.cache.setup(&TFFS_device.cache);
        if(result != 0){
            TFFS_device.backend_state = tffs3_module_error;
            TFFS_device.cache_state = tffs3_module_error;
            goto err_out;
        }

        TFFS_device.backend_state = tffs3_module_running;
        TFFS_device.cache_state = tffs3_module_running;

#if defined(CONFIG_TFFS_DEV_REMOTE)
        // backend device is up. Try starting server if configured
        if(TFFS_device.server_state == tffs3_module_configured){
            if(TFFS_device.server.setup){
                result = TFFS_device.server.setup(&TFFS_device.server);
                if(result == 0){
                    TFFS_device.server_state = tffs3_module_running;
                    if(TFFS_device.server.notify){
                        TFFS_device.cache.register_notify(&TFFS_device.cache, NULL, notify_cb);
                    }
                } else {
                    TFFS_device.server_state = tffs3_module_error;
                }
            } else {
                pr_err("[%s] No server setup function registered.\n", __func__);
            }

            // don't abort if server setup fails
            result = 0;
        }
#endif // defined(CONFIG_TFFS_DEV_REMOTE)
    } else {
        pr_err("[%s] Backing device either not configured or already set up.\n", __func__);
        result = -EINVAL;
    }

err_out:
    up(&TFFS_device.lock);

    return result;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
void TFFS3_Deinit(void)
{
    // pr_err("[%s] called\n", __func__);
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
struct tffs_core_handle *TFFS3_Open(enum _tffs_id id, enum tffs3_handle_mode mode)
{
    struct tffs_core_handle *handle;
    int result;

    // pr_err("[%s] called\n", __func__);

    handle = NULL;
    result = 0;


    if(mode == tffs3_mode_panic){
        result = down_trylock(&TFFS_device.lock);
        result = (result == 0) ? 0 : -EBUSY;
    } else {
        result = down_interruptible(&TFFS_device.lock);
        result = (result == 0) ? 0 : -EINTR;
    }

    // refuse opening new handles while a panic mode handle exists
    if(TFFS_device.in_panic_mode != 0){
        result = -EBUSY;
    }

    if(result != 0){
        goto err_out;
    }

    if(mode == tffs3_mode_panic){
        handle = &TFFS_device.panic_handle;
        TFFS_device.in_panic_mode = 1;
    } else {
        handle = kmalloc(sizeof(*handle), GFP_KERNEL);
    }

    if(handle == NULL) {
        pr_err("[%s] getting %shandle failed\n", __func__, TFFS_device.in_panic_mode ? "panic " : "");
        result = -ENOMEM;
        goto err_out;
    }

    memset(handle, 0x0, sizeof(*handle));
    handle->id = id;
    handle->mode = mode;

    handle->core_priv = TFFS_device.cache.open(&TFFS_device.cache, handle);
    if(IS_ERR_OR_NULL(handle->core_priv)){
        result = handle->core_priv == NULL ? -ENOMEM : PTR_ERR(handle->core_priv);
        handle->core_priv = NULL;
    }

err_out:
    if(result != 0 && handle != NULL){
        if(!IS_ERR_OR_NULL(handle->core_priv)){
            result = TFFS_device.cache.close(&TFFS_device.cache, handle->core_priv);
        }
        if(TFFS_device.in_panic_mode != 0){
            TFFS_device.in_panic_mode = 0;
        } else {
            kfree(handle);
        }
        handle = NULL;
    }

    up(&TFFS_device.lock);

    if(result != 0){
        handle = ERR_PTR(result);
    }

    return handle;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
int TFFS3_Close(struct tffs_core_handle *handle)
{
    int result;

    // pr_err("[%s] called\n", __func__);
    if(handle == NULL){
        result = -EBADF;
        goto err_out;
    }

    result = lock_device(&TFFS_device, handle);
    if(result != 0){
        goto err_out;
    }

    result = TFFS_device.cache.close(&TFFS_device.cache, handle->core_priv);
    if(result == 0){
        if(handle->mode == tffs3_mode_panic){
            TFFS_device.in_panic_mode = 0;
        } else {
            handle->core_priv = NULL;
            kfree(handle);
        }
    }

    up(&TFFS_device.lock);

err_out:
    return result;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
int TFFS3_Clear(struct tffs_core_handle *handle)
{
    // pr_err("[%s] called\n", __func__);

    return TFFS3_Write(handle, NULL, 0, 0);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int TFFS3_Werkseinstellungen(struct tffs_core_handle *handle)
{
    struct tffs_core_handle *tmp_handle;
    enum _tffs_id id;
    unsigned int count;
    int result;

    // pr_err("[%s] called\n", __func__);

    if(TFFS_device.in_panic_mode){
        return -EBUSY;
    }

    DBG((KERN_INFO "TFFS3_Werkseinstellungen(0x%x)\n", (int)handle));
    result = 0;
    count = 0;
    for(id = FLASH_FS_ID_TICFG ; id <= FLASH_FS_ID_FIRMWARE_CONFIG_LAST ; id++){
        tmp_handle = TFFS3_Open(id, tffs3_mode_write);
        if(tmp_handle == NULL){
            result = -ENOMEM;
            goto err_out;
        }

        result = TFFS3_Clear(tmp_handle);
        TFFS3_Close(tmp_handle);

        if(result) {
            DBG((KERN_INFO "TFFS3_Werkseinstellungen(0x%x): clear id 0x%x failed (%u cleared)\n", (int)handle, (int)id, count));
            goto err_out;
        }

        ++count;
    }

    DBG((KERN_INFO "TFFS3_Werkseinstellungen(0x%x): success (%u cleared)\n", (int)handle, count));

err_out:
    return result;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
int TFFS3_Write(struct tffs_core_handle *handle, uint8_t *write_buffer, size_t write_length, unsigned int final)
{
    size_t written;
    int result;

    // pr_err("[%s] called\n", __func__);

    result = lock_device(&TFFS_device, handle);
    if(result != 0){
        goto err_out;
    }

    result = TFFS_device.cache.write(&TFFS_device.cache, handle->core_priv, write_buffer, write_length, &written, final);

    up(&TFFS_device.lock);

err_out:
    return result;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
int TFFS3_Read(struct tffs_core_handle *handle, uint8_t *read_buffer, size_t *read_length)
{
    int result;

    // pr_err("[%s] called\n", __func__);

    result = lock_device(&TFFS_device, handle);
    if(result != 0){
        goto err_out;
    }

    result = TFFS_device.cache.read(&TFFS_device.cache, handle->core_priv, read_buffer, read_length);

    up(&TFFS_device.lock);

err_out:
    return result;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int TFFS3_Cleanup(struct tffs_core_handle *handle)
{
    int result;

    // pr_err("[%s] called\n", __func__);

    if(TFFS_device.in_panic_mode){
        return -EBUSY;
    }

    result = lock_device(&TFFS_device, handle);
    if(result != 0){
        goto err_out;
    }

    result = TFFS_device.cache.cleanup(&TFFS_device.cache, handle->core_priv);

    up(&TFFS_device.lock);

err_out:
    return result;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
int TFFS3_Create_Index(void)
{
    int result;

    // pr_err("[%s] called\n", __func__);

    if(TFFS_device.in_panic_mode){
        return -EBUSY;
    }

    result = down_interruptible(&(TFFS_device.lock));
    if(result != 0){
        goto err_out;
    }

    result = TFFS_device.cache.reindex(&TFFS_device.cache);

    up(&(TFFS_device.lock));

err_out:
    return result;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
int TFFS3_Info(struct tffs_core_handle *handle, unsigned int *Fill)
{
    int result;

    // pr_err("[%s] called\n", __func__);

    result = lock_device(&TFFS_device, handle);
    if(result != 0){
        goto err_out;
    }

    result = TFFS_device.cache.info(&TFFS_device.cache, Fill);

    up(&TFFS_device.lock);

err_out:
    return result;
}

EXPORT_SYMBOL(TFFS3_Register_NAND);
EXPORT_SYMBOL(TFFS3_Register_LGCY);
EXPORT_SYMBOL(TFFS3_Register_REMOTE);
EXPORT_SYMBOL(TFFS3_Register_SERVER);
EXPORT_SYMBOL(TFFS3_Open);
EXPORT_SYMBOL(TFFS3_Close);
EXPORT_SYMBOL(TFFS3_Read);
EXPORT_SYMBOL(TFFS3_Write);
