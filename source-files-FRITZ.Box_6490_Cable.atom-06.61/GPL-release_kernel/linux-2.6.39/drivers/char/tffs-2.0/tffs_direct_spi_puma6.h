/*------------------------------------------------------------------------------------------*\
 *
\*------------------------------------------------------------------------------------------*/
#if !defined(_TFFS_DIRECT_SPI_PUMA6_)
#define _TFFS_DIRECT_SPI_PUMA6_

/* Base Address */
#define SPI_F_CSR0_MBAR 0x0FFE0100
#define SPI_F_WIN_MBAR  0x08000000

/* Register offset */
#define MODE_CONTL_REG                          IO_ADDRESS((SPI_F_CSR0_MBAR) + 0x00)
#define ADDR_SPLIT_REG                          IO_ADDRESS((SPI_F_CSR0_MBAR) + 0x04)
#define CURRENT_ADDR_REG                        IO_ADDRESS((SPI_F_CSR0_MBAR) + 0x08)
#define COMMAND_DATA_REG                        IO_ADDRESS((SPI_F_CSR0_MBAR) + 0x0C)
#define INTERFACE_CONFIG_REG                    IO_ADDRESS((SPI_F_CSR0_MBAR) + 0x10)
#define HIGH_EFFICIENCY_COMMAND_DATA_REG        IO_ADDRESS((SPI_F_CSR0_MBAR) + 0x20)
#define HIGH_EFFICIENCY_TRANSACTION_PARAMS_REG  IO_ADDRESS((SPI_F_CSR0_MBAR) + 0x24)
#define HIGH_EFFICIENCY_OPCODE_REG              IO_ADDRESS((SPI_F_CSR0_MBAR) + 0x28)

/* Bit definitions for MODE_CNTL_REG */
#define MODE_CONTL_CLK_RATIOR_SHIFT             (0)
#define MODE_CONTL_BOOT_MODE_ENABLE             (0x1 << 4)
#define MODE_CONTL_SPI_UNIT_EN                  (0x1 << 5)
#define MODE_CONTL_SS1_EN                       (0x1 << 6)

#define MODE_CONTL_CMD_WIDTH_EQUAL_TO_DATA      (0x0 << 9)
#define MODE_CONTL_CMD_WIDTH_1_DATA_LINE        (0x1 << 9)

#define MODE_CONTL_SPI_WIDTH_1_BIT              (0x1 << 10)
#define MODE_CONTL_SPI_WIDTH_2_BIT              (0x2 << 10)
#define MODE_CONTL_SPI_WIDTH_4_BIT              (0x3 << 10)
#define MODE_CONTL_SPI_WIDTH_MASK               (0x3 << 10)

#define MODE_CONTL_N_ADDR_2_BYTES               (0x2 << 12)
#define MODE_CONTL_N_ADDR_3_BYTES               (0x3 << 12)
#define MODE_CONTL_N_ADDR_4_BYTES               (0x4 << 12)
#define MODE_CONTL_N_ADDR_MASK                  (0xF << 12)


#define MODE_CONTL_CS0_MODE_ENABLE              (0x1 << 16)
#define MODE_CONTL_CS0_MODE_ENABLE_MASK         (0x3 << 16)
#define MODE_CONTL_CS0_WP                       (0x1 << 18)

#define MODE_CONTL_CS1_MODE_ENABLE              (0x1 << 20)
#define MODE_CONTL_CS1_MODE_ENABLE_MASK         (0x3 << 20)
#define MODE_CONTL_CS1_WP                       (0x1 << 22)

#define MODE_CONTL_CS_TAR_SHIFT                 (24)
#define MODE_CONTL_CS_TAR_MASK                  (0XF << MODE_CONTL_CS_TAR_SHIFT)

/* Keep Chip Select Low (Command/Data register) */
#define KEEP_CS_LOW_MASK    0x04000000

#define N_ADDR_BYTE_MASK    0x0000F000
#define N_ADDR_BYTE_3       0x00003000
#define N_ADDR_BYTE_4       0x00004000


#define SR_WIP                          0x01    /* Write in progress */
#define SERIAL_FLASH_TIMEOUT            0xFFFF
#define SF_CMD_SIZE                     4
#define SF_MAX_CMD_SIZE                 5

#define SPI_USE_CS0     0
#define SPI_USE_CS1     1

/****************************************************************************
* NOTE:
*   Currently SPANSION, NUMONYIX, MACRONIX, Winbond and SST serial flash are
*   supported. All have common command Set
****************************************************************************/
/* Spansion family  Serial Flash with uniform sector size */
#define FLASH_S25FL004A             0x00F2
#define FLASH_S25FL008A             0x00F3
#define FLASH_S25FL016A             0x00F4
#define FLASH_S25FL032A             0x00F5
#define FLASH_S25FL064A             0x00F6
#define FLASH_S25FL128P             0x00F7
/* ST/Numonyix family Serial Flash with uniform sector size */
#define FLASH_M25P128               0x00F8
#define FLASH_M25P64                0x00FB
#define FLASH_N25Q128               0x00FE
/* Macronix */
#define FLASH_MX25l6405D            0x00F9
#define FLASH_MX25l12805D           0x00FA
/* Winbond */
#define FLASH_W25Q64BV              0x00FC
#define FLASH_W25Q128BV             0x00FD
/* SST */
#define FLASH_25VF64                0x00FF

typedef struct {
#define WRITE_EN(x)         (x)
    unsigned char write_en;
#define WRITE_DIS(x)        (x)
    unsigned char write_dis;
#define WRITE_STS(x)        (x)
    unsigned char write_sts;
#define READ_ID(x)          (x)
    unsigned char read_id;
#define READ_STS(x)         (x)
    unsigned char read_sts;
#define READ_DATA(x)        (x)
    unsigned char read_data;
#define READ_FAST(x)        (x)
    unsigned char read_fast;
#define ERASE_SECT(x)       (x)
    unsigned char erase_sect;
#define ERASE_BLOCK(x)      (x)
    unsigned char erase_block;
#define ERASE_CHIP(x)       (x)
    unsigned char erase_chip;
#define PROG_PAGE(x)        (x)
    unsigned char prog_page;
} sf_cmd_tbl_t;

/* Supported features */
#define FLASH_FEATURE_DUAL_FAST_READ    (1<<0)
#define FLASH_FEATURE_80MHZ_CLOCK       (2<<0)
#define FLASH_FEATURE_4KB_SECTORS       (3<<0)

#define FLASH_SUPPORTS( feat )  ( sf_info[i].feature & feat )

/*------------------------------------------------------------------------------------------*\
 * Urlader flash16.h
\*------------------------------------------------------------------------------------------*/
#define PROGRAM_ERROR_NO_ERROR                  0
#define PROGRAM_ERROR_WRITE_FAILED              1
#define PROGRAM_ERROR_CLEAR_FAILED              2
#define PROGRAM_ERROR_MANUFACTURER_FAILED       3
#define PROGRAM_ERROR_LAYOUT_FAILED             4
#define PROGRAM_ERROR_CRC_FAILED                5
#define PROGRAM_ERROR_UNKNOWN                   6
#define PROGRAM_ERROR_UNLOCK_FAILED             7

#define CFI_Query                               0x98
#define Autosel                                 0x90

/*-------------------------------------------------------------------------------------*\
 * MACRONIX Definitionen
\*-------------------------------------------------------------------------------------*/
#if defined(CONFIG_MIPS_AR9) || defined(CONFIG_MIPS_VR9)
#define FLASH_OFFSET(ADDR) (((ADDR) << 1) ^ (0x2))
#else
#define FLASH_OFFSET(ADDR)  ((ADDR) << 1)
#endif

#define Enable_CFI_Query                        FLASH_OFFSET(0x0055) /*--- address for cfi query enable ---*/
#define MX_Enable_Rom_Write1                    FLASH_OFFSET(0x0555) /*--- address1 ---*/
#define MX_Enable_Rom_Write2                    FLASH_OFFSET(0x02AA) /*--- address2 ---*/
#define MX_Enable_Rom_Write3                    FLASH_OFFSET(0x0555) /*--- address3 ---*/
#define MX_Enable_Rom_Write4                    FLASH_OFFSET(0x0555) /*--- address4 ---*/
#define MX_Enable_Rom_Write5                    FLASH_OFFSET(0x02AA) /*--- address5 ---*/
#define MX_Enable_Rom_Write6                    FLASH_OFFSET(0x0555) /*--- address6 ---*/

#define MX_Enable_Rom_Write1_Data               0xAA      /*--- data for address1 ---*/
#define MX_Enable_Rom_Write2_Data               0x55      /*--- data for address2 ---*/
#define MX_Program                              0xA0      /*--- program ---*/
#define MX_Autosel                              0x90      /*--- autosel ---*/
#define MX_Erase                                0x80      /*--- Enable erase ---*/
#define MX_Enable_Rom_Write4_Data               0xAA      /*--- data for address4 ---*/
#define MX_Enable_Rom_Write5_Data               0x55      /*--- data for address5 ---*/
#define MX_ChipErase                            0x10      /*--- chip erase ---*/
#define MX_SectorErase                          0x30      /*--- Sector erase ---*/
#define MX_Enable_Rom_Erase                     0x80      /*--- data for chiperase ---*/
#define MX_toggle6                              0x40
#define MX_toggle62                             0x44
#define MX_Flash_Reset                          0xF0      /*--- data for flashreset ---*/

#define MX_Write_Buffer                         0x25
#define MX_Program_Buffer                       0x29

#define MX_DPB_Entry                            0xE0        /*--- Enable DynamicProtectionBits ---*/
#define MX_DPB_Write                            0xA0        /*--- Change DynamicProtectionBits ---*/
#define MX_DPB_Exit1                            0x90        /*--- Exit DynamicProtectionBits ---*/
#define MX_DPB_Exit2                            0x00        /*--- Exit DynamicProtectionBits ---*/

#define MIRRORBIT_DEVICE_ID                     0x7E      /*--- Mirrorbit Flashs Macronix/Spansion ---*/

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define MX_MANUFACTURE_ID                       0xC2
#define MX_DEVICE_ID_64MB_TOP                   0xC9      /*--- MX29LV640AT ---*/
#define MX_DEVICE_ID_64MB_BOTTOM                0xCB      /*--- MX29LV640AT ---*/
#define MX_DEVICE_ID_32MB_TOP                   0xA7      /*--- MX29LV320AT ---*/
#define MX_DEVICE_ID_32MB_BOTTOM                0xA8      /*--- MX29LV320AB ---*/
#define MX_DEVICE_ID_16MB_TOP                   0xC4      /*--- MX29LV160AT ---*/
#define MX_DEVICE_ID_16MB_BOTTOM                0x49      /*--- MX29LV160AB ---*/

#define MX_MEMORY_TYPE_SPI                      0x20      /*--- MX25L6405 ---*/
#define MX_SIZE_64MB_SPI                        0x17
#define MX_SIZE_128MB_SPI                       0x18

/*------------------------------------------------------------------------------------------*\
 * Spansion ist identisch zu Macronix
\*------------------------------------------------------------------------------------------*/
#define SPANSION_MANUFACTURE_ID                 0x01
#define SPANSION_L016A_BOTTOM                   0x49
#define SPANSION_L016A_TOP                      0xC4
#define SPANSION_L032A_MIRROR_BT                0x1A      /*--- kann sowohl Bottom als auch Top sein ---*/
#define SPANSION_L032A_MIRROR_UNIFORM           0x1D
#define SPANSION_L064A_MIRROR_BT                0x10
#define SPANSION_L064A_MIRROR_UNIFORM           0x0C
#define SPANSION_L064A_MIRROR_UNIFORM_R67       0x13      /*--- wird vom Design nicht unterstützt ---*/

#define SPANSION_MEMORY_TYPE_SPI                0x02      /*--- S25FL064A ---*/
#define SPANSION_MEMORY_TYPE_I_SPI              0x19      /*--- S70FL256P ---*/
#define SPANSION_MEMORY_TYPE_II_SPI             0x20      /*--- S25FL129 ---*/
#define SPANSION_SIZE_64MB_SPI                  0x16

/*------------------------------------------------------------------------------------------*\
 * ST ist identisch zu Macronix
\*------------------------------------------------------------------------------------------*/
#define ST_MANUFACTURE_ID                       0x20
#define ST_N25Q256A                             0xBA
#define ST_M29W640GH_GL                         0x0C
#define ST_M29W640GT_GB                         0x10
#define ST_M29W128GH_GL                         0x7E

/*------------------------------------------------------------------------------------------*\
 * WINBOND
\*------------------------------------------------------------------------------------------*/
#define WINBOND_MANUFACTURE_ID                  0xEF
#define WINBOND_MEMORY_TYPE_SPI                 0x40

/*------------------------------------------------------------------------------------------*\
 * offsets cfi
\*------------------------------------------------------------------------------------------*/
#define CFI_QRY_STRING                          0x10
#define CFI_PRIM_VENDOR_CMD                     0x13
#define CFI_SYSTEM_DATA                         0x1B
#define CFI_GEO_DEVICE_SIZE                     0x27
#define CFI_GEO_WRITE_BUFFER_SIZE               0x2A
#define CFI_GEO_NUM_ERASE_REGIONS               0x2C
#define CFI_GEO_ERASE_INFO                      0x2D


/*--- macronix spezifisch, die Adresse der Primary Table wird separat ermittelt ---*/
#define MX_SECTOR_PROTECT_OFFSET                0x09
#define MX_BOOTSECTOR_FLAG_OFFSET               0x0F


/*------------------------------------------------------------------------------------------*\
 * INTEL Definitionen
\*------------------------------------------------------------------------------------------*/
#define INTEL_Read_Array                        0xFFFF
#define INTEL_Read_ID_Data                      0x9090
#define INTEL_Read_Status                       0x7070
#define INTEL_Clear_Status                      0x5050
#define INTEL_Enable_Rom_Write                  0x4040
#define INTEL_Erase                             0x2020
#define INTEL_Erase_Suspend                     0xB0B0
#define INTEL_Erase_Resume                      0xD0D0
#define INTEL_Lock_Cmd                          0x6060
#define INTEL_Lock_Block                        0x0101
#define INTEL_Unlock_Block                      0xD0D0
#define INTEL_Lock_Down_Block                   0x2F2F
#define INTEL_Protection_Block                  0xC0C0
#define INTEL_Sector_Erase_Data                 0xD0D0

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define INTEL_MANUFACTURE_ID                    0x89
#define INTEL_DEVICE_ID_8M_TOP                  0xC0      /*--- 8-MBit Top Boot Device ---*/
#define INTEL_DEVICE_ID_8M_BOTTOM               0xC1      /*--- 8-MBit Bottom Boot Device ---*/
#define INTEL_DEVICE_ID_16M_TOP                 0xC2      /*--- 16-MBit Top Boot Device ---*/
#define INTEL_DEVICE_ID_16M_BOTTOM              0xC3      /*--- 16-MBit Bottom Boot Device ---*/
#define INTEL_DEVICE_ID_32M_TOP                 0xC4      /*--- 32-MBit Top Boot Device ---*/
#define INTEL_DEVICE_ID_32M_BOTTOM              0xC5      /*--- 32-MBit Bottom Boot Device ---*/
#define INTEL_DEVICE_ID_64M_TOP                 0xCC      /*--- 64-MBit Top Boot Device ---*/
#define INTEL_DEVICE_ID_64M_BOTTOM              0xCD      /*--- 64-MBit Bottom Boot Device ---*/

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define TOP_FLASH           0
#define BOTTOM_FLASH        1
#define UNIFORM_FLASH       2
#define FLASH_BUFFER_SIZE   256

/*------------------------------------------------------------------------------------------*\
 * Definitionen SPI-Flash
\*------------------------------------------------------------------------------------------*/
#define SPI_CMD_SHIFT_24   0

#define SPI_WRITE_ENABLE                    (0x06)       /*--- write enable ---*/
#define SPI_WRITE_DISABLE                   (0x04)       /*--- write disable ---*/
#define SPI_READ_ID                         (0x9F << 24)       /*--- read identification ---*/
#define SPI_READ_STATUS                     (0x05)       /*--- read status register ---*/
#define SPI_WRITE_STATUS                    (0x01 << 24)       /*--- write status register ---*/
#define SPI_READ                            (0x03 << 24)       /*--- read data ---*/
#define SPI_FASTREAD                        (0x0B << 24)       /*--- fast read data ---*/
#define SPI_PARALLEL_MODE                   (0x55 << 24)       /*--- parallel mode ---*/
#define SPI_SECTOR_ERASE                    (0xD8 << 24)       /*--- sector erase ---*/
#define SPI_CHIP_ERASE                      (0xD7 << 24)       /*--- chip erase ---*/
#define SPI_PAGE_PROGRAM                    (0x02 << 24)       /*--- page program ---*/
#define SPI_DEEP_POWER_DOWN                 (0xB9 << 24)       /*--- deep power down ---*/
/*--- #define SPI_ENTER_4K                        (0xA5 << 24) ---*/       /*--- enter 4kb ---*/
/*--- #define SPI_EXIT_4K                         (0xB5 << 24) ---*/       /*--- exit 4kb ---*/
#define SPI_RELEASE_POWER_DOWN              (0xAB << 24)       /*--- release from deep power-down ---*/
#define SPI_READ_ELECTRONIK_ID  SPI_REALEASE_POWER_DOWN        /*--- read electronic id ---*/
#define SPI_READ_ELECTRONIC_ID_MANUFACTURE  (0x9F)       /*--- read electronic manufacturer id & device id ---*/

#define SPI_FLASH_COMMAND_EN4BYTEADDR       (0xb7)        /* Enter 4-byte address mode */
#define SPI_FLASH_COMMAND_EX4BYTEADDR       (0xe9)        /* Exit 4-byte address mode */
#define SPI_READ_4BYTE                      (0x13)
#define SPI_PAGE_PROGRAM_4BYTE              (0x12)       /*--- page program ---*/
#define SPI_SECTOR_ERASE_4BYTE              (0xDC)       /*--- sector erase ---*/
#define SPI_RESET_ENABLE                    (0x66)
#define SPI_RESET                           (0x99)
#define SPI_READ_CONFIG                     (0x15)

/*--- status bits ---*/
#define WIP                 (1 << 0)
#define WEL                 (1 << 1)
#define BP0                 (1 << 2)
#define BP1                 (1 << 3)
#define BP2                 (1 << 4)
#define BP3                 (1 << 5)
#define PROG_ERROR          (1 << 6)
#define REG_WRITE_PROTECT   (1 << 7)

/*--- config bits ---*/
#define ODS0                (1 << 0)
#define ODS1                (1 << 1)
#define ODS2                (1 << 2)
#define TB                  (1 << 3)
/*--- bit 4 reserved ---*/
#define MODE4B              (1 << 5)
#define DC0                 (1 << 6)
#define DC1                 (1 << 7)

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
enum flash_type {
    TYPE_UNKNOWN = -1,
    TYPE_NOR = 0,
    TYPE_NAND = 1,
    TYPE_SPI= 2
};

typedef struct _flash_addr_ {
    unsigned int    start;
    unsigned int    end;
} flash_Addr;

struct _CFI_System_Data_ {
    unsigned char  min_VCC;
    unsigned char  max_VCC;
    unsigned char  min_VPP;
    unsigned char  max_VPP;
    unsigned char  write_timeout;
    unsigned char  buffer_write_timeout;
    unsigned char  erase_sector_timeout;
    unsigned char  erase_chip_timeout;
    unsigned char  max_write_timeout;
    unsigned char  max_buffer_write_timeout;
    unsigned char  max_erase_sector_timeout;
    unsigned char  max_erase_chip_timeout;
};

struct _CFI_Erase_Regions_ {
    unsigned short  blk_num;
    unsigned int    blk_size;
    unsigned int    size;
};

struct _CFI_Device_Geometry_ {
    unsigned short  flash_interface_size;
    unsigned short  interface_code;
    unsigned short  writeBuffer_size;
    unsigned char   num_erase_regions;
    struct _CFI_Erase_Regions_ erase_regions[4];
};

struct _CFI_ {
    char            Query[3];
    unsigned int    PriVendorCmdSet_ID;
    unsigned int    Addr_ext_Query_Table;
    unsigned int    AltVendorIDCode;
    unsigned int    Addr_Second_ext_Query_Table;
};

struct _flash_device_ {
    unsigned char   Manufacturer;
    unsigned char   ID;
    unsigned char   Organisation;
    unsigned char   Blockmode;
    unsigned int    Size;
    unsigned int    SectorProtect;
    struct _CFI_                    Ident;
    struct _CFI_System_Data_        System;
    struct _CFI_Device_Geometry_    Geometry;

    int             (*Program_Cmd)(volatile unsigned int Addr, unsigned short Wert);
    int             (*Erase_Cmd)(volatile unsigned int Addr);
};

struct _Flash_Functions_ {
    struct _flash_device_   Device;
    int             (*Read)(volatile unsigned int Addr, unsigned char *pdata, unsigned int len);
    int             (*Write)(volatile unsigned int Addr, unsigned char *pdata, unsigned int len);
    int             (*Erase)(volatile unsigned int Addr, unsigned int size);
    void            (*Reset)(void);

    int             (*not_free)(unsigned int start_address, unsigned int len);
    unsigned int    (*GetBlockSize)(unsigned int address);
};

/*------------------------------------------------------------------------------------------*\
 * Urlader errors.h
\*------------------------------------------------------------------------------------------*/
#define SUCCESS                 0
#define FLASH_SUCCESS           SUCCESS
/*--- #define FLASH_IGNORE            0x8000 ---*/
/*--- #define FLASH_NO_BUFFER         0x8001 ---*/
#define FLASH_INVAL_ADDR        0x8002
#define FLASH_ID_FAILED         0x8003
#define FLASH_QUERY_FAILED      0x8004
#define FLASH_ERASE_REGIONS     0x8005
#define FLASH_MX_BOTTOM_FAILED  0x8006

/*------------------------------------------------------------------------------------------*\
 * sonstiges
\*------------------------------------------------------------------------------------------*/
typedef void (*spi_chipsel_type)(int cs);

#endif // _TFFS_DIRECT_SPI_PUMA6_
