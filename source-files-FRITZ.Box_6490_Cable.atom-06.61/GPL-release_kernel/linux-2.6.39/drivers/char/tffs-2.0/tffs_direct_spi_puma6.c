/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2007 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------*\
 * Direkte Ansteuerung des SPI-Flash im Panic Mode (Urlader-Routinen)
\*------------------------------------------------------------------------------------------*/

#include <linux/module.h>
#include <linux/version.h>
#include <linux/slab.h>
#include <asm/fcntl.h>
#include <linux/sched.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/io.h>
#include <linux/delay.h>
#include <linux/semaphore.h>
#include <arch-avalanche/generic/pal.h>
#include <linux/kernel.h>

#include "tffs_direct_spi_puma6.h"
#include <asm/irq.h>

#if defined (CONFIG_HW_MUTEXES)
#include <arch-avalanche/puma6/hw_mutex_ctrl.h>
#else
#define hw_mutex_lock(a)    (0)
#define hw_mutex_unlock(a)
#endif

// #define DEBUG_SPI
#ifdef DEBUG_SPI
#define DBG_SPI(...)        printk(__VA_ARGS__)
#else
#define DBG_SPI(...) 
#endif
#define DebugPrintf(...)    printk(__VA_ARGS__)

// TKL: TFFS ist immer im ersten Flash
// #define SPI_2_FLASH

#define AVM_CS_OVERRIDE
#if defined(AVM_CS_OVERRIDE)
#define CS0_GPIO    54
#define CS1_GPIO    52
#endif
#define FLASH_BUFFER_SIZE       256
#define SPI_MAX_FRAME_LEN 256
#define URLADER_PROTECT_SIZE    0xC0000

#define TRUE                    1
#define FALSE                   0

extern int tffs_mtd_offset[2];
extern unsigned int tffs_panic_mode;

static void puma6_spi_chipsel_0(int cs);
static void puma6_spi_chipsel_1(int cs);

/* Global table oc chip select callback table */
/* This table can be access from all project  */
spi_chipsel_type spi_chipsel[] = {puma6_spi_chipsel_0, puma6_spi_chipsel_1};

#define puma6_gpioOutBit(a, b)      PAL_sysGpioOutBitSync((a), (b))

static struct _Flash_Functions_      spi_Flash;
static struct _Flash_Functions_     *Flash = &spi_Flash;
static uint32_t cntl_mode_save;
static int spi_addr_width = 0;

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static inline uint32_t endian_swap(uint32_t x) {
    int swp = x;
    return ( ((swp & 0x000000FF)<<24) + ((swp & 0x0000FF00)<<8 ) +
             ((swp & 0x00FF0000)>>8 ) + ((swp & 0xFF000000)>>24) );
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int lock_flash_mutex(void)
{
//    hw_mutex_lock(HW_MUTEX_NOR_SPI);
    cntl_mode_save = endian_swap(*((volatile uint32_t *)(MODE_CONTL_REG)));

    return 1;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void unlock_flash_mutex(void)
{
    cntl_mode_save |= (MODE_CONTL_CS0_MODE_ENABLE | MODE_CONTL_CS1_MODE_ENABLE);
    *((volatile uint32_t *)(MODE_CONTL_REG)) = endian_swap(cntl_mode_save);

//    hw_mutex_unlock(HW_MUTEX_NOR_SPI);
}

/*------------------------------------------------------------------------------------------*\
 * beim PUMA-Design gibt es 2 SPI-Flashs an 2 CS
 * anhand der Adresse wird das CS ausgerechnet
\*------------------------------------------------------------------------------------------*/
static unsigned int calc_cs(unsigned int address) {
    
    DBG_SPI("[%s] address 0x%x ", __FUNCTION__, address);

    if (address < (32<<20)) {
        DBG_SPI(" - 0\n");
        return SPI_USE_CS0;     /*--- < 32MB ---*/
    } else {
        DBG_SPI(" - 1\n");
        return SPI_USE_CS1;     /*--- > 32MB ---*/
    }
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static inline unsigned int puma6_spi_GetBlockSize(unsigned int address) {

    return Flash->Device.Geometry.erase_regions[0].blk_size;
}

#if 0
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int spi_not_free(unsigned int start_address, unsigned int len) {

    DBG_SPI("[flash_not_free] addr 0x%x\n", start_address);
    
    return TRUE;
}
#endif

/*------------------------------------------------------------------------------------------*\
 * SPI transfer
 *
 * This writes "bitlen" bits out the SPI MOSI port and simultaneously clocks
 * "bitlen" bits in the SPI MISO port.  That's just the way SPI works.
 *
 * The source of the outgoing bits is the "dout" parameter and the
 * destination of the input bits is the "din" parameter.  Note that "dout"
 * and "din" can point to the same memory location, in which case the
 * input data overwrites the output data (since both are buffered by
 * temporary variables, this is OK).
 *
 * If the chipsel() function is not NULL, it is called with a parameter
 * of '1' (chip select active) at the start of the transfer and again with
 * a parameter of '0' at the end of the transfer.
 *
 * If the chipsel() function _is_ NULL, it the responsibility of the
 * caller to make the appropriate chip select active before calling
 * puma6_spi_xfer() and making it inactive after puma6_spi_xfer() returns.
 *
 * puma6_spi_xfer() interface:
 *   chipsel: Routine to call to set/clear the chip select:
 *              if chipsel is NULL, it is not used.
 *              if(cs),  make the chip select active (typically '0').
 *              if(!cs), make the chip select inactive (typically '1').
 *   dout:    Pointer to a string of bits to send out.  The bits are
 *              held in a byte array and are sent MSB first.
 *   din:     Pointer to a string of bits that will be filled in.
 *   bitlen:  How many bits to write and read.
 *
 *   Returns: 0 on success, not 0 on failure
 *
 *   Achtung: einschalten der Debugausgaben in dieser Funktion führen beim Schriben zu einem
 *   Timeout im SPI-Flash - es werden dann nicht alle Daten geschrieben. (ab ca. 200Bytes)
\*------------------------------------------------------------------------------------------*/
static int  puma6_spi_xfer(spi_chipsel_type chipsel, int bitlen, uint8_t *dout, uint8_t *din) {

    unsigned int tmpdin  = 0;
	unsigned int tmpdout = 0;
	unsigned int bytelen = 0;
    unsigned int bytes = 0;
    unsigned int bytesToSend = 0;
    unsigned int bytesToRead = 0;
    int i=0;

    /*--- DebugPrintf("[%s]: chipsel 0x%x %s %s bitlen %d ", __func__, (int)chipsel, dout ? "dout":"", din ? "din":"", bitlen); ---*/

    bytelen = bitlen/8;
    if ((bitlen%8) != 0) {
        bytelen++;
    }

    /* select the target chip */
	if(chipsel != NULL) {
		(chipsel)(1);
	}

    /* write bytelen bytes to Flash */
    if (dout != NULL) {
        bytes=0; 
        while (bytes < bytelen) {
            tmpdout = 0;

            /* Set first byte to send */
            tmpdout = (dout[bytes] << 16);
            bytes++;
            bytesToSend = 1;

            /* Set Second byte to send */
            if (bytes < bytelen) {
                tmpdout |= dout[bytes] << 8;
                bytes++;
                bytesToSend = 2;
            }
                
            /* Set third byte to send */
            if (bytes < bytelen) {
                tmpdout |= dout[bytes];
                bytes++;
                bytesToSend = 3;       
            }
           
            /* Set number of bytes to send, and set CS to stay low. */
            tmpdout |= (KEEP_CS_LOW_MASK | (bytesToSend & 0x03)<<24 );

            /* Write data to register */
            *((volatile uint32_t *)(COMMAND_DATA_REG)) = endian_swap(tmpdout);
#ifdef SPI_DEBUG
            (spi_debug == 1) ? DebugPrintf("Write : 0x%08X ",tmpdout):0;
#endif
            
        }
    }

    /* read bytelen bytes from Flash */
    if (din != NULL) {
        bytes=0; 
        while (bytes < bytelen) {
            if (bytelen - bytes >= 3) {
                bytesToRead = 3;
            } else if (bytelen - bytes >= 2) {
                bytesToRead = 2;
            } else {
                bytesToRead = 1;
            }

            /* Write number of bytes to read */
            tmpdin = (KEEP_CS_LOW_MASK | (bytesToRead & 0x03)<<24);
            *((volatile uint32_t *)(COMMAND_DATA_REG)) = endian_swap(tmpdin);
#ifdef SPI_DEBUG
            (spi_debug == 1) ? DebugPrintf("Read(W): 0x%08X ",tmpdin):0;
#endif
            

           /* Read byte */
            tmpdin = endian_swap(*((volatile uint32_t *)(COMMAND_DATA_REG)));
#ifdef SPI_DEBUG
            (spi_debug == 1) ? DebugPrintf("Read(R): 0x%08X ",tmpdin):0;
#endif
            

            for (i=(bytesToRead-1)*8;i>=0;i-=8) {
                din[bytes] = (uint8_t)(tmpdin>>i & 0x000000FF);
                bytes++;
            } 
        }
    }

#ifdef SPI_DEBUG
    DebugPrintf("\n");
#endif
    /* deselect the target chip */
	if(chipsel != NULL) {
		(chipsel)(0);	
	}

	return(0);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void puma6_spi_send_address(uint32_t address)
{
    unsigned char *tmp;
    unsigned int bitlen;

    tmp = (unsigned char *) &address;
    if(spi_addr_width == 3){
        bitlen = 24;
        ++tmp;
    } else {
        bitlen = 32;
    }

    puma6_spi_xfer(NULL, bitlen, tmp, NULL);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static unsigned int puma6_spi_read_byte(unsigned int address, unsigned char *pdata) {

    unsigned char buffer;
    unsigned int  cs = calc_cs(address);
    unsigned int  cs_offset = cs ? (32<<20) : 0;

    spi_chipsel[cs](1);

    buffer = SPI_READ >> 24;
    puma6_spi_xfer(NULL, 8, &buffer, NULL);

    puma6_spi_send_address(address - cs_offset);

    puma6_spi_xfer(NULL, 8, NULL, pdata);

    spi_chipsel[cs](0);

    return 1;
}

/*------------------------------------------------------------------------------------------*\
 * 4 Byte aligned, wir kopieren immer 4 Bytes 
\*------------------------------------------------------------------------------------------*/
static unsigned int puma6_spi_read_block(unsigned int address, unsigned char *pdata, unsigned int datalen) {

    unsigned char buffer;
    unsigned int  cs = calc_cs(address);
    unsigned int  cs_offset = cs ? (32<<20) : 0;

    spi_chipsel[cs](1);

    buffer = SPI_READ >> 24;
    puma6_spi_xfer(NULL, 8, &buffer, NULL);

    puma6_spi_send_address(address - cs_offset);

    puma6_spi_xfer(NULL, datalen << 3, NULL, pdata);      /*--- hier werden Bits angegeben ---*/

    spi_chipsel[cs](0);

    return datalen;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int puma6_spi_read(unsigned int address, unsigned char *pdata, unsigned int len) {

    unsigned int Bytes = 0;
    unsigned int read, read_len;
    
    DBG_SPI("[puma6_spi_read] 0x%x len %d 0x%p\n", address, len, pdata);

    if (lock_flash_mutex() == 0) {     /* Lock the HW Mutex */
        DebugPrintf("<%s failed to lock HW Mutex>\n", __func__);
        return 0;
    }

    while (Bytes < len) {
        if ((address & 3) || ((unsigned int)pdata & 3) || ((len - Bytes) < 4)) {     /*--- byteweise lesen ---*/
            DBG_SPI("[puma6_spi_read] Bytes 0x%x len %d\n", address, len);
            read = puma6_spi_read_byte(address, pdata);
        } else {
            DBG_SPI("[puma6_spi_read] Int 0x%x len %d\n", address, len & ~3);
            read_len = (len - Bytes) > ((SPI_MAX_FRAME_LEN-1)<<2) ? ((SPI_MAX_FRAME_LEN-1)<<2) : (len - Bytes);
            read = puma6_spi_read_block(address, pdata, read_len);
        }
        address += read;
        pdata += read;
        Bytes += read;
        if (read == 0) {
            DebugPrintf("<ERROR: Flash read aborted %x>\n", (int)address);
            break;
        }
    }

    unlock_flash_mutex();  /* Release HW Mutes */
    return Bytes;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int puma6_sf_write_then_read(int cs, unsigned char *tx_data, int tx_len, unsigned char *rx_data, int rx_len)
{
    spi_chipsel[cs](1);
	if((tx_len != 0)  && (tx_data != NULL)) {
        if(puma6_spi_xfer(NULL, (tx_len * 8) /* byte to bit */, tx_data, NULL) != 0 ) {
			DebugPrintf("[%s] puma6_spi_xfer failed in TX \n", __func__);
			return -1;
		}
    }

	if((rx_len != 0)  && (rx_data != NULL)) {
		if (puma6_spi_xfer(NULL, (rx_len * 8), NULL, rx_data)!= 0 ) {
			DebugPrintf("[%s] puma6_spi_xfer failed in RX \n", __func__);
			return -1;
		}
	}
    spi_chipsel[cs](0);

	return (int)(rx_len+tx_len);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
unsigned char puma6_spi_read_status(unsigned int cs) {

    unsigned char status;
    unsigned char cmd = SPI_READ_STATUS;
   
    puma6_sf_write_then_read(cs, &cmd, 1, &status, 1);

    return status;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void puma6_spi_cmd_simple(unsigned char cmd, unsigned int cs) {

    DBG_SPI("[%s] cs 0x%x cmd 0x%x\n", __FUNCTION__, cs, cmd);

    puma6_spi_xfer(spi_chipsel[cs], 8, &cmd, NULL);

}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
unsigned int puma6_spi_write_byte(unsigned int address, unsigned char *pdata) {

    unsigned int cs = calc_cs(address);
    unsigned char status;
    unsigned char buffer;
    unsigned int SectorSize = puma6_spi_GetBlockSize(address);
    unsigned int cs_offset = cs ? (32<<20) : 0;

    if (!SectorSize) {
        DebugPrintf("Error: <sector_size == 0 Addr 0x%x>\n", address);
        return 0;
    }

#if 0
    if (address == save_urlader_config.flash_addr) {
        if (!Delete_Default_Config) {
            *pdata = save_urlader_config.data[save_urlader_config.flash_addr - save_urlader_config.start_addr];
        }
    }
#endif

    puma6_spi_cmd_simple(SPI_WRITE_ENABLE, cs);

    spi_chipsel[cs](1);
    buffer = SPI_PAGE_PROGRAM >> 24;
    puma6_spi_xfer(NULL, 8, &buffer, NULL);

    puma6_spi_send_address(address - cs_offset);

    puma6_spi_xfer(NULL, 8, pdata, NULL);
    spi_chipsel[cs](0);

    while (1) {
        status = puma6_spi_read_status(cs);
        if (!(status & WIP))
            break;
    }

    puma6_spi_read_byte(address, &buffer);
    if (buffer != *pdata) {
        DebugPrintf("\n<Flash>: B Error: Addr 0x%x should=0x%x read=0x%x\n", address, *pdata, buffer);
        return 0;
    }

    return 1;
    
}

/*------------------------------------------------------------------------------------------*\
 * wir schreiben hier die Daten immer 32 Bit weise
\*------------------------------------------------------------------------------------------*/
unsigned int puma6_spi_write_block(unsigned int address, unsigned char *data, unsigned int datalen) {

    unsigned char buffer[FLASH_BUFFER_SIZE];
    unsigned int *pData, *pAddr = (unsigned int *)address;
    unsigned int  SectorSize = puma6_spi_GetBlockSize(address);
    unsigned int  BufferSize;
    unsigned int  cs = calc_cs(address);
    unsigned int  tmp;
    unsigned char tmp_char;
    unsigned int  cs_offset = cs ? (32<<20) : 0;
    int i, status = 0;

    if (Flash->Device.Blockmode)
        BufferSize = Flash->Device.Geometry.writeBuffer_size;
    else
        BufferSize = sizeof(unsigned short);

    if (!SectorSize) {
        DebugPrintf("Error: <sector_size == 0 Addr 0x%x>\n", address);
        return 0;
    }

    if (datalen > BufferSize) {
        DebugPrintf("Error: <len > BufferSize %d>\n", BufferSize);
        return 0;
    }
        
    if ((unsigned int)data & 3) {
        memset(buffer, 0xff, FLASH_BUFFER_SIZE);
        memcpy(buffer, data, datalen);         /*--- die Daten aligned in den Buffer kopieren ---*/
        pData = (unsigned int *)&buffer;
    } else {
        pData = (unsigned int *)data;
    }

    /*--- wir dürfen nicht über eine Sectorgrenze schreiben ---*/
    if (((unsigned int)pAddr % SectorSize) && (datalen > ((unsigned int)pAddr % SectorSize))) {
        datalen -= (unsigned int)pAddr % SectorSize;
    }

    /*--- nicht über die Grenze des Writebuffers schreiben ---*/
    if (((unsigned int)pAddr % BufferSize) && (datalen > (BufferSize - ((unsigned int)pAddr % BufferSize)))) {
        datalen = (BufferSize - ((unsigned int)pAddr % BufferSize));
    }

    puma6_spi_cmd_simple(SPI_WRITE_ENABLE, cs);
    /*--- DBG_SPI("[%s] status 0x%x\n", __func__, puma6_spi_read_status(cs)); ---*/

    spi_chipsel[cs](1);

    tmp_char = SPI_PAGE_PROGRAM >> 24;
    puma6_spi_xfer(NULL, 8, &tmp_char, NULL);

    puma6_spi_send_address(address - cs_offset);

    for (i=0;i<(datalen>>2);i++) {
        /*--- DBG_SPI("[puma6_spi_write_block] 0x%x\n", *(unsigned int *)pdata); ---*/
        tmp = *(unsigned int *)pData;
        puma6_spi_xfer(NULL, 32, (unsigned char *)&tmp, NULL);
        pData++;
    }

    spi_chipsel[cs](0);

    while (1) {
        status = puma6_spi_read_status(cs);
        if (!(status & WIP) && !(status & WEL))
            break;
    }

    /*--- DBG_SPI("[%s] %d Bytes 0x%x\n", __func__, datalen, address); ---*/

    /*--- zum Schluss noch alles Vergleichen ---*/
    puma6_spi_read_block(address, buffer, datalen);

    if (memcmp((char *)buffer, (char *)data, datalen)) {
#if 0
        for (i=0;i<datalen;i++)
            DebugPrintf("0x%x ", buffer[i]);
        DebugPrintf("\n\n");
        for (i=0;i<datalen;i++)
            DebugPrintf("0x%x ", data[i]);
#endif
        /*--- DebugPrintf("\n<FlashBlock>: %d Error: Addr 0x%x should=0x%B read=0x%B\n", datalen, address, data, buffer); ---*/
        DebugPrintf("{%s} ERROR datalen %d  Address 0x%x\n", __func__, datalen, address);
        return 0;
    }

    DBG_SPI("[%s] wrote %d\n", __func__, datalen);
    return datalen;
}

/*------------------------------------------------------------------------------------------*\
 *   |< config_area < 0         >|< config_area >= 0                                     >|
 *   +------------------------------------------------------------------------------------+
 *   |                           | config    |                                            |
 *   +------------------------------------------------------------------------------------+
 *                               ^save_urlader_config.start_addr
\*------------------------------------------------------------------------------------------*/
static int puma6_spi_write(unsigned int address, unsigned char *pdata, unsigned int len) {

    unsigned int  Bytes = 0;
    unsigned int  written, write;
    /*--- int           config_area, config_len; ---*/
    /*--- unsigned char buffer[FLASH_BUFFER_SIZE]; ---*/
    unsigned int  buffer_size = FLASH_BUFFER_SIZE;
 
    DBG_SPI("[puma6_spi_write] address 0x%x len %d pdata 0x%p\n", address, len, pdata);

    if (lock_flash_mutex() == 0) {     /* Lock the HW Mutex */
        DebugPrintf("<%s failed to lock HW Mutex>\n", __func__);
        return 0;
    }

    while (Bytes < len) {
        if ((address & 3) || ((unsigned int)pdata & 3) || ((len - Bytes) < 4)) {     /*--- byteweise lesen ---*/
            DBG_SPI("[puma6_spi_write] Bytes addr 0x%x\n", address);
            written = puma6_spi_write_byte(address, pdata);
        } else {
            write = (len - Bytes) > buffer_size ? buffer_size : (len - Bytes);
            write &= ~3;
            DBG_SPI("[puma6_spi_write] Integer 0x%x len %d\n", address, write);
            
#if 0
            config_area = (address -  save_urlader_config.start_addr); 

            if (!Delete_Default_Config && (config_area >= 0) && (config_area < save_urlader_config.size)) {
                config_len = save_urlader_config.size - config_area;
                config_len = (config_len > buffer_size) ? buffer_size : config_len;
                /*--- wir dürfen maximal "write" Bytes schreiben ---*/
                config_len = (config_len > write) ? write : config_len; 
                memcpy(buffer, 
                       &save_urlader_config.data[address - save_urlader_config.start_addr],
                       config_len);
                written = spi_write_block((unsigned int)address, buffer, config_len);
            } else {
                if ((config_area < 0) && ((address + write) > save_urlader_config.start_addr)) {
                    /*--- wir würden in den Configbereich hineinschreiben - also write verkleinern ---*/
                    write = save_urlader_config.start_addr - address;
                }
                written = puma6_spi_write_block(address, pdata, write);
            }
#else
            written = puma6_spi_write_block(address, pdata, write);
#endif
        }
        address += written;
        pdata += written;
        Bytes += written;

        if (written == 0) {
            DebugPrintf("<ERROR: Flash write aborted 0x%x>\n", (int)address);
            break;
        }
    }

    unlock_flash_mutex();  /* Release HW Mutes */
    return Bytes;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int puma6_spi_erase_cmd(volatile unsigned int address) {
#if 0
    unsigned char status;
    unsigned int ret = PROGRAM_ERROR_NO_ERROR;
    unsigned int blocksize = spi_GetBlockSize(address);
    unsigned int cs = calc_cs(address);
    unsigned int tmp;
    unsigned char tmp_char;
    unsigned int  cs_offset = cs ? (32<<20) : 0;

    if (lock_flash_mutex() == 0) {     /* Lock the HW Mutex */
        DebugPrintf("<%s failed to lock HW Mutex>\n", __func__);
        return 0;
    }

    puma6_spi_cmd_simple(SPI_WRITE_ENABLE, cs);   /*--- WRITE_ENABLE muss nicht gelöscht werden ---*/

    spi_chipsel[cs](1);

    tmp_char = SPI_SECTOR_ERASE >> 24;
    puma6_spi_xfer(NULL, 8, &tmp_char, NULL);

    tmp = address - cs_offset;
    puma6_spi_xfer(NULL, 32, (unsigned char *)&tmp, NULL);

    spi_chipsel[cs](0);

    while (1) {
        status = puma6_spi_read_status(cs);
        if (!(status & WIP) && !(status & WEL))
            break;
    }
    
    unlock_flash_mutex();  /* Release HW Mutes */

    if (spi_not_free(address, blocksize)) {
        return PROGRAM_ERROR_WRITE_FAILED;
    }

    return ret;
#else
    return PROGRAM_ERROR_NO_ERROR;
#endif
}

/*------------------------------------------------------------------------------------------*\
 *
\*------------------------------------------------------------------------------------------*/
static void puma6_spi_reset(void)
{
    spi_chipsel[0](0);
    udelay(500);
    puma6_spi_cmd_simple(SPI_RESET_ENABLE, 0);

    udelay(500);
    puma6_spi_cmd_simple(SPI_RESET, 0);

    // reset can take up to 100ms to complete
    mdelay(100);
}

/*------------------------------------------------------------------------------------------*\
 * den Flash-Controller in bootfaehige Konfiguration bringen
\*------------------------------------------------------------------------------------------*/
static void puma6_spi_init_hw(void)
{
    unsigned int modeCtl;

    modeCtl = endian_swap(*((volatile uint32_t *)(MODE_CONTL_REG)));

    /* Boot Mode */
    modeCtl |= MODE_CONTL_BOOT_MODE_ENABLE;

    /* SS1_UNIT_EN */
    modeCtl |= MODE_CONTL_SPI_UNIT_EN;

    /* SS1_EN */
    modeCtl |= MODE_CONTL_SS1_EN;

    /* CMD WIDTH */
    modeCtl &= ~((uint32_t) MODE_CONTL_CMD_WIDTH_1_DATA_LINE);
    modeCtl |= MODE_CONTL_CMD_WIDTH_EQUAL_TO_DATA;

    /* Set SPI_WIDTH */
    modeCtl &= ~((uint32_t) MODE_CONTL_SPI_WIDTH_MASK);
    modeCtl |= MODE_CONTL_SPI_WIDTH_1_BIT;

#if 0
    /* NR_ADDR_BYTES  */
    modeCtl &= ~((uint32_t) MODE_CONTL_N_ADDR_MASK);
    modeCtl |= MODE_CONTL_N_ADDR_4_BYTES;
    spi_addr_width = 4;
#endif

    if(modeCtl & MODE_CONTL_N_ADDR_4_BYTES){
        spi_addr_width = 4;
    } else {
        spi_addr_width = 3;
    }

    /* Set chip select 0 */
    modeCtl &= ~((uint32_t) MODE_CONTL_CS0_MODE_ENABLE_MASK);
    modeCtl |= MODE_CONTL_CS0_MODE_ENABLE;

    /* Set chip select 1 */
    modeCtl &= ~((uint32_t) MODE_CONTL_CS1_MODE_ENABLE_MASK);
    modeCtl |= MODE_CONTL_CS1_MODE_ENABLE;

    /* CS_TAR */
    modeCtl &= ~((uint32_t) MODE_CONTL_CS_TAR_MASK);
    modeCtl |= 0x4 << MODE_CONTL_CS_TAR_SHIFT;

    cntl_mode_save = modeCtl;
    *((volatile uint32_t *)(MODE_CONTL_REG)) = endian_swap(cntl_mode_save);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void puma6_spi_chipsel_0(int cs) {
    uint32_t modeCtrlReg;

    /* Turn Off Chip Select */
#if defined(AVM_CS_OVERRIDE)
    puma6_gpioOutBit(CS0_GPIO, 1);
#if defined(SPI_2_FLASH)
    puma6_gpioOutBit(CS1_GPIO, 1);
#endif
#endif
    
    /* Clear bits CS0 and CS1 mode bits in Mode Control Register */
    modeCtrlReg = endian_swap(*((volatile uint32_t *)(MODE_CONTL_REG)));
    modeCtrlReg &= ~((uint32_t) (MODE_CONTL_CS0_MODE_ENABLE_MASK | MODE_CONTL_CS1_MODE_ENABLE_MASK));;
    *((volatile uint32_t *)(MODE_CONTL_REG)) = endian_swap(modeCtrlReg);   

    if (cs == 1) {
        /* Select Chip#0 */
        
        modeCtrlReg |= MODE_CONTL_CS0_MODE_ENABLE;
    } else {
        /* AVM/TKL: make sure that both CS lines are enabled, otherwise we may not be able
         * to boot after pseudo cold reset on Puma6 */
        modeCtrlReg |= (MODE_CONTL_CS0_MODE_ENABLE | MODE_CONTL_CS1_MODE_ENABLE);
    }

    *((volatile uint32_t *)(MODE_CONTL_REG)) = endian_swap(modeCtrlReg);
    *((volatile uint32_t *)(COMMAND_DATA_REG)) = 0;

#if defined(AVM_CS_OVERRIDE)
    udelay(500);
    if (cs == 1) {
        puma6_gpioOutBit(CS0_GPIO, 0);
    }
#endif

    return;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static void puma6_spi_chipsel_1(int cs) {
#if defined(SPI_2_FLASH)
    uint32_t modeCtrlReg;

    /* Turn Off Chip Select */
#if defined(AVM_CS_OVERRIDE)
    puma6_gpioOutBit(CS0_GPIO, 1);
    puma6_gpioOutBit(CS1_GPIO, 1);
#endif
        
    /* Clear bits CS0 and CS1 mode bits in Mode Control Register */
    modeCtrlReg = endian_swap(*((volatile uint32_t *)(MODE_CONTL_REG)));
    modeCtrlReg &= ~((uint32_t) (MODE_CONTL_CS0_MODE_ENABLE_MASK | MODE_CONTL_CS1_MODE_ENABLE_MASK));;
    *((volatile uint32_t *)(MODE_CONTL_REG)) = endian_swap(modeCtrlReg);

    if (cs == 1) {
        /* Select Chip#1 */

        modeCtrlReg |= MODE_CONTL_CS1_MODE_ENABLE;
    } else {
        /* AVM/TKL: make sure that both CS lines are enabled, otherwise we may not be able
         * to boot after pseudo cold reset on Puma6 */
        modeCtrlReg |= (MODE_CONTL_CS0_MODE_ENABLE | MODE_CONTL_CS1_MODE_ENABLE);
    }

    *((volatile uint32_t *)(MODE_CONTL_REG)) = endian_swap(modeCtrlReg);
    *((volatile uint32_t *)(COMMAND_DATA_REG)) = 0;
        
#if defined(AVM_CS_OVERRIDE)
    udelay(500);
    if (cs == 1) {
        puma6_gpioOutBit(CS1_GPIO, 0);
    }
#endif

#endif // defined(SPI_2_FLASH)
    return;
}

/*------------------------------------------------------------------------------------------*\
 * auf dem Demoboard nutzen wir SERIAL_CS1
\*------------------------------------------------------------------------------------------*/
static unsigned int puma6_init_flash_device(void) {

    unsigned int  Data_Register;
    unsigned char buffer[4];
    unsigned char status;
    
    if (lock_flash_mutex() == 0) {     /* Lock the HW Mutex */
        DebugPrintf("<%s failed to lock HW Mutex>\n", __func__);
        return FLASH_ID_FAILED;
    }
    puma6_spi_init_hw();

    puma6_spi_chipsel_0(0);

    puma6_spi_reset();

    buffer[0] = SPI_READ_ELECTRONIC_ID_MANUFACTURE;

    spi_chipsel[0](1);
    puma6_spi_xfer(NULL, 8, buffer, NULL);
    puma6_spi_xfer(NULL, 24, NULL, buffer);
    spi_chipsel[0](0);
    
    unlock_flash_mutex();

    Data_Register = *(unsigned int *)buffer;
    Data_Register >>= 8;
    DBG_SPI("Data_Register 0x%x\n", Data_Register);
    switch (Data_Register & 0xFFFF00) {
        case ((MX_MANUFACTURE_ID << 16) + (MX_MEMORY_TYPE_SPI << 8)):
            Flash->Device.Manufacturer = MX_MANUFACTURE_ID;
            Flash->Device.ID = MX_MEMORY_TYPE_SPI;
            Flash->Device.Size = 1<<(Data_Register & 0xFF);
            Flash->Device.Geometry.writeBuffer_size = FLASH_BUFFER_SIZE;
            Flash->Device.Geometry.num_erase_regions = 1;
            Flash->Device.Geometry.erase_regions[0].blk_size = 0x10000;
            Flash->Device.Geometry.erase_regions[0].size = Flash->Device.Size;
            Flash->Device.Geometry.erase_regions[0].blk_num = Flash->Device.Size / 0x10000;
                    /*--- flash->Device.Geometry.erase_regions[i].blk_size * flash->Device.Geometry.erase_regions[i].blk_num; ---*/

            Flash->Device.Organisation = UNIFORM_FLASH;
            Flash->Device.Blockmode = TRUE;

            break;
        case ((SPANSION_MANUFACTURE_ID << 16) + ( SPANSION_MEMORY_TYPE_SPI << 8)):
        case ((SPANSION_MANUFACTURE_ID << 16) + ( SPANSION_MEMORY_TYPE_II_SPI << 8)):
            Flash->Device.Manufacturer = SPANSION_MANUFACTURE_ID;
            Flash->Device.ID = SPANSION_MEMORY_TYPE_SPI;
            Flash->Device.Size = 1<<(Data_Register & 0xFF);
            Flash->Device.Geometry.writeBuffer_size = FLASH_BUFFER_SIZE;
            Flash->Device.Geometry.num_erase_regions = 1;
            Flash->Device.Geometry.erase_regions[0].blk_size = 0x10000;
            Flash->Device.Geometry.erase_regions[0].size = Flash->Device.Size;
            Flash->Device.Geometry.erase_regions[0].blk_num = Flash->Device.Size / 0x10000;
                    /*--- flash->Device.Geometry.erase_regions[i].blk_size * flash->Device.Geometry.erase_regions[i].blk_num; ---*/

            Flash->Device.Organisation = UNIFORM_FLASH;
            Flash->Device.Blockmode = TRUE;

            break;
        case ((ST_MANUFACTURE_ID << 16) + (ST_N25Q256A << 8)):
            Flash->Device.Manufacturer = ST_MANUFACTURE_ID;
            Flash->Device.ID = ST_MANUFACTURE_ID;
            Flash->Device.Size = 1<<(Data_Register & 0xFF);
            Flash->Device.Geometry.writeBuffer_size = FLASH_BUFFER_SIZE;
            Flash->Device.Geometry.num_erase_regions = 1;
            Flash->Device.Geometry.erase_regions[0].blk_size = 0x10000;
            Flash->Device.Geometry.erase_regions[0].size = Flash->Device.Size;
            Flash->Device.Geometry.erase_regions[0].blk_num = Flash->Device.Size / 0x10000;

            Flash->Device.Organisation = UNIFORM_FLASH;
            Flash->Device.Blockmode = TRUE;
            break;
        default:
            DebugPrintf("[init_spi] <no flash detected>\n");
            return FLASH_ID_FAILED;
    }

    if(spi_addr_width == 4){
        if (lock_flash_mutex() == 0) {     /* Lock the HW Mutex */
            DebugPrintf("<%s failed to lock HW Mutex>\n", __func__);
            return FLASH_ID_FAILED;
        }

        puma6_spi_cmd_simple(SPI_FLASH_COMMAND_EN4BYTEADDR, 0);
        udelay(50);

        buffer[0] = SPI_READ_CONFIG;
        status = 0;
        puma6_sf_write_then_read(0, &(buffer[0]), 1, &status, 1);

        unlock_flash_mutex();

        if(!(status & MODE4B)){
            DebugPrintf("[%s] switch to 4 byte address mode failed\n", __func__);
            return FLASH_ID_FAILED;
        }
    }

#if defined(SPI_2_FLASH)
    {
        unsigned int flash_type, flash_size;

        if (lock_flash_mutex() == 0) {     /* Lock the HW Mutex */
            DebugPrintf("<%s failed to lock HW Mutex>\n", __func__);
            return FLASH_ID_FAILED;
        }

        buffer[0] = SPI_READ_ELECTRONIC_ID_MANUFACTURE;

        spi_chipsel[1](1);
        puma6_spi_xfer(NULL, 8, buffer, NULL);
        puma6_spi_xfer(NULL, 24, NULL, buffer);
        spi_chipsel[1](0);

        unlock_flash_mutex();  /* Release HW Mutes */

        Data_Register = *(unsigned int *)buffer;
        DBG_SPI("Data_Register 2 0x%x\n", Data_Register);

        flash_type = Data_Register >> 24;
        flash_size = 1<<((Data_Register >> 8) & 0xFF);
        if ((flash_type != Flash->Device.Manufacturer) || 
            (flash_size != Flash->Device.Size)) {
            DebugPrintf("[init_spi] <no 2. flash detected>\n");
            DBG_SPI("flash_type 0x%x 0x%x flash_size 0x%x 0x%x\n", 
                    flash_type, Flash->Device.Manufacturer, 
                    flash_size, Flash->Device.Size);
            return FLASH_ID_FAILED;
        }
        Flash->Device.Size <<= 1;   /*--- doppelter Flash ---*/
    }
#endif

    Flash->Device.Program_Cmd = 0;       /*--- gibt es so nicht ---*/
    Flash->Device.Erase_Cmd = puma6_spi_erase_cmd;
    Flash->Write = puma6_spi_write;
    Flash->Read = puma6_spi_read;
    Flash->Erase = NULL; //Erase;
    Flash->Reset = puma6_spi_reset;
    Flash->not_free = NULL; //spi_not_free;
    Flash->GetBlockSize = puma6_spi_GetBlockSize;

    return FLASH_SUCCESS;
}

/*------------------------------------------------------------------------------------------*\
 * aus Performacegründen kopieren wir zuerst 32bit weise, den Rest 8bit weise
\*------------------------------------------------------------------------------------------*/
int tffs_spi_read(unsigned int address, unsigned int mtd_id, unsigned char *pdata, unsigned int len)
{
    unsigned int Bytes = 0;
    unsigned int chip_addr;

    DBG_SPI("[%s] 0x%x mtd %d len %d 0x%p\n", __func__, address, mtd_id, len, pdata);

    if(tffs_mtd_offset[mtd_id] == 0) {
        /*--- falls tffs_mtd_offset noch nicht initialisiert: Urlader nicht ueberschreiben! ---*/
        return 0;
    }

    chip_addr = tffs_mtd_offset[mtd_id] + address;
    if((chip_addr < URLADER_PROTECT_SIZE) || (chip_addr + len > Flash->Device.Size)){
        return 0;
    }
    
    Bytes = puma6_spi_read(chip_addr, pdata, len);

    return Bytes;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int tffs_spi_write(unsigned int address, unsigned int mtd_id, unsigned char *pdata, unsigned int len)
{
    unsigned int Bytes = 0;
    unsigned int chip_addr;

    DBG_SPI("[%s] address 0x%x mtd %d len %d pdata 0x%p\n", __func__, address, mtd_id, len, pdata);

    if(tffs_mtd_offset[mtd_id] == 0) {
        /*--- falls tffs_mtd_offset noch nicht initialisiert: Urlader nicht ueberschreiben! ---*/
        return 0;
    }

    chip_addr = tffs_mtd_offset[mtd_id] + address;
    if((chip_addr < URLADER_PROTECT_SIZE) || (chip_addr + len > Flash->Device.Size)){
        return 0;
    }

    Bytes = puma6_spi_write(chip_addr, pdata, len);

    return Bytes;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int tffs_spi_init(void)
{
    int result = -EINVAL;

    if(!tffs_panic_mode){
        printk(KERN_ERR "[%s] Called, but not in panic mode.\n", __func__);
        result = -EINVAL;
        goto err_out;
    }

    local_irq_disable();
    result = hw_mutex_panic_lock(HW_MUTEX_NOR_SPI);
    if(result != 0){
        printk(KERN_ERR "[%s] Unable to acquire HW mutex\n", __func__);
        goto err_out;
    }

    result = puma6_init_flash_device();

err_out:
    return result;
}


