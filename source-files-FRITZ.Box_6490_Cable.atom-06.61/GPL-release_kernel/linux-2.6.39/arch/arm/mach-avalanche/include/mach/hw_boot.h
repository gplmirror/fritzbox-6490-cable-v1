/*--------------------------------------------------------------------------------*\
 * Bootregister
\*--------------------------------------------------------------------------------*/
#ifndef __hw_boot_h__
#define __hw_boot_h__

#include <mach/hardware.h>

#define PUMA_BOOTCONF_BASE             IO_ADDRESS(0x08611A00)

#define PUMA_KICK0_VALUE               0x83E70B13
#define PUMA_KICK1_VALUE               0x95A4F1E0

union _pinmux0 {
    volatile unsigned int reg;
    struct __pinmux0 {
        unsigned int reserved1:13;                  /*--- Bit 19:31 ---*/
        volatile unsigned int epga_clk_dir:1;       /*--- Bit 18 ---*/
        volatile unsigned int clk_out_1_dir:1;      /*--- Bit 17 ---*/
        volatile unsigned int clk_out_0_dir:1;      /*--- Bit 16 ---*/
        unsigned int reserved2:6;                   /*--- Bit 10:15 ---*/
        volatile unsigned int usb_otg:1;            /*--- Bit 9 ---*/
        unsigned int reserved3:1;                   /*--- Bit 8 ---*/
        volatile unsigned int hpi_enable:1;         /*--- Bit 7 ---*/
        unsigned int reserved4:3;                   /*--- Bit 4:6 ---*/
        volatile unsigned int uartb_en:1;           /*--- Bit 3 ---*/
        volatile unsigned int mpeg_outen:1;         /*--- Bit 2 ---*/
        volatile unsigned int mpeg_inen:1;          /*--- Bit 1 ---*/
        volatile unsigned int oob_en:1;             /*--- Bit 0 ---*/
    } bits;
};

union _usb_phy_control {
    volatile unsigned int reg;
    struct __usb_phy_control {
        unsigned int reserved1:14;                  /*--- Bit 18:31 ---*/
        volatile unsigned int drvvbus_sel:1;        /*--- Bit 17 ---*/
        volatile unsigned int USB_clk_Sel:1;        /*--- Bit 16 ---*/
        unsigned int reserved2:9;                   /*--- Bit 7:15 ---*/
        volatile unsigned int phyclkgd:1;           /*--- Bit 6 ---*/
        volatile unsigned int sesnden:1;            /*--- Bit 5 ---*/
        unsigned int vbdtcten:1;                    /*--- Bit 4 ---*/
        volatile unsigned int vbusens:1;            /*--- Bit 3 ---*/
        volatile unsigned int pllon:1;              /*--- Bit 2 ---*/
        volatile unsigned int otgpdwn:1;            /*--- Bit 1 ---*/
        volatile unsigned int pdwn:1;               /*--- Bit 0 ---*/
    } bits;
};

union _io_pd_cr {
    volatile unsigned int reg;
    struct __io_pd_cr {
        unsigned int reserved:16;
        volatile unsigned int Gmii_GPIO_IO_pd:1;            /*--- Bit 15 Power down for GMII PADs that are MUXed with UAX GPIO: TXD7:4 and RXD7:4 ---*/
        volatile unsigned int  VLYNQ_pd:1;                  /*--- Bit 14 ---*/
        volatile unsigned int  Clock_out1_pd:1;             /*--- Bit 13 ---*/
        volatile unsigned int  Clock_out0_pd:1;             /*--- Bit 12 ---*/
        volatile unsigned int  External_Interrupt_pd:1;     /*--- Bit 11 ---*/
        volatile unsigned int  TAGC_IO_pd:1;                /*--- Bit 10 ---*/
        volatile unsigned int  AGC_IO_pd:1;                 /*--- Bit 9 ---*/
        volatile unsigned int  Asyn_DDR_Controller_IO_pd:1; /*--- Bit 8 ---*/
        volatile unsigned int  BBU_IO_pd:1;                 /*--- Bit 7 ---*/
        volatile unsigned int  Epga_IO_pd:1;                /*--- Bit 6 ---*/
        volatile unsigned int  OOB_IO_pd:1;                 /*--- Bit 5 ---*/
        volatile unsigned int  GMII_IO_pd:1;                /*--- Bit 4 Exclude PADs that are MUXed with AUX GPIO ---*/
        volatile unsigned int  IIC_IO_pd:1;                 /*--- Bit 3 ---*/
        volatile unsigned int  GPIO_IO_pd:1;                /*--- Bit 2 ---*/
        volatile unsigned int  TDM_and_Codec_IO_pd :1;      /*--- Bit 1 ---*/
        volatile unsigned int  UART_IO_pd:1;                /*--- Bit 0 ---*/
    } bits;
};
struct _bootconf {
    unsigned int reserved1[6];             /*--- 0x08611A00 Reserved - 0x08611A14 ---*/
    volatile unsigned int Device_ID_0;     /*--- 0x8611A18  ---*/
    volatile unsigned int Device_ID_1;     /*--- 0x8611A1C  ---*/
    unsigned int reserved2[6];
    volatile unsigned int Kick_0;          /*--- 0x8611A38 Kick 0 ---*/
    volatile unsigned int Kick_1;          /*--- 0x8611A3C Kick 1 ---*/
    unsigned int reserved3[52];            /*--- 0x8611A40 Reserved ----*/
    union _pinmux0 PinMux0;                /*--- 0x08611B10   ---*/
    unsigned int reserved4[1];              /*--- 0x8611A40 Reserved ----*/
    volatile unsigned int Test_Observe;    /*--- 0x8611B18 ---*/
    unsigned int reserved5[1];
    union _usb_phy_control USBPHY_Control; /*--- 0x8611B20 ---*/
    unsigned int reserved6[3];
    union _io_pd_cr IO_PD_CR_IO_pd;        /*--- 0x08611B30  ---*/
    unsigned int reserved7[10];
    volatile unsigned int CODEC_RESET;      /*--- 0x8611B5C  ---*/
    volatile unsigned int Pull_UpDown_REG0; /*--- 0x8611B60  ---*/
    volatile unsigned int Pull_UpDown_REG1; /*--- 0x8611B64  ---*/
    unsigned int reserved8[2];
    volatile unsigned int bbu_cr;           /*--- 0x8611B70  ---*/
    volatile unsigned int tdm_clk_src_sel;  /*--- 0x8611B74  ---*/
};
#endif/*--- #ifndef __hw_boot_h__ ---*/
