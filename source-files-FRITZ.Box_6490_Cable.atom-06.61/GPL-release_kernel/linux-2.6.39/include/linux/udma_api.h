#ifndef UDMA_API_H
#define UDMA_API_H

#include <linux/netdevice.h>

/*There're two UDMA ports belong to APPCPU */
#define UDMA_PORT_NUM_TOTAL 2 

typedef enum {
	UDMA_BURST_SZ_4_BYTES = 0,
	UDMA_BURST_SZ_8_BYTES,
	UDMA_BURST_SZ_16_BYTES,
	UDMA_BURST_SZ_32_BYTES,
	UDMA_BURST_SZ_64_BYTES,
	UDMA_BURST_SZ_128_BYTES
} udma_burstsz_t;
static const udma_burstsz_t udma_to_l2_burstsz_dft = UDMA_BURST_SZ_64_BYTES;
static const udma_burstsz_t l2_to_udma_burstsz_dft = UDMA_BURST_SZ_64_BYTES;

typedef enum {
	UDMA_GAP_VAL_0_CLKS = 0,
	UDMA_GAP_VAL_16_CLKS,
	UDMA_GAP_VAL_64_CLKS,
	UDMA_GAP_VAL_256_CLKS,
	UDMA_GAP_VAL_1024_CLKS,
	UDMA_GAP_VAL_2048_CLKS,
	UDMA_GAP_VAL_4096_CLKS,
	UDMA_GAP_VAL_8192_CLKS
} udma_gapval_t;
static const udma_gapval_t udma_to_l2_gap_dft = UDMA_GAP_VAL_64_CLKS;
static const udma_gapval_t l2_to_udma_gap_dft = UDMA_GAP_VAL_0_CLKS;

typedef enum {
        UDMA_OK            = 0x0, /**< 0x0 */
		UDMA_BUSY				, /* UDMA is busy to response */
		UDMA_ERR				, /* UDMA error */
		UDMA_FULL 				, /* The input queue is full to receive new buffers */
		UDMA_EMPTY				, /* The input queue is full to receive new buffers */
		UDMA_INVALID_PARAM		, /* Invalid param */
		UDMA_UNINITIALIZED		, /* UDMA uninitialized */
		UDMA_NO_PERM			, /* UDMA is or going to be stopped that no permision for UDMA access */
		UDMA_AGAIN				 /* Try again */		
} udma_result_t;

typedef void (*rx_callback_t)(struct sk_buff *skb, struct net_device *netdev);

/**  udma_xmit_skb - Transmit a packet via UDMA
 * @param[in] port - udma port number, could be 0 or 1
 * @param[in] skb  - the packet pointer to be send,
 * the buffer will be freed by UDMA driver
 * When UDMA is free, the packet is accepted by UDMA driver.
 * When UDMA is BUSY, the UDMA driver will try for 16 times and finally free
 * the skb buffer if UDMA is still BUSY.
 * Note that the context is that of a softIRQ.
 * Note that the customer driver does not need to free the skb pointer since
 * it's covered by UDMA driver.
 *
 * return 0 on success, UDMA accept the packet.
 * return others for failure.
*/
int udma_xmit_skb(unsigned char port, struct sk_buff *skb);


/**  udma_send_packet - A buffer is used to send
 * @port - udma port number, could be 0 or 1
 * @buffer_desc - parameter to describe a coming buffer
 * 
 * return 0 on success, UDMA driver took care of the buffer
 * return others for failure
*/
int udma_send_packet(unsigned char port, struct sk_buff *skb);

/**  udma_set_burstsize - seperately set the UDMA port tx/rx burst size
 * @param[in] port - udma port number, could be 0 or 1
 * @param[in] udma_to_l2_burstsize - this will translate into the number of
 * bytes per udma to l2switch DMA bus transfer, the burst size should be choosen
 * from udma_burstsz_t, other burtsize values would return failure.
 * @param[in] l2_to_udma_burstsize - this will translate into the number of
 * bytes per l2switch to udma DMA bus transfer, the burst size should be
 * choosen from udma_burstsz_t, other burtsize values would return failure.
 * Note that this function should be called before udma_register_handler to
 * setup initial udma burst size, and can * be only called once in the
 * initialization stage. And user can use only one of udma_set_burstsize or
 * udma_set_burstsize_tx_rx to config the DMA burst size.
 * If this function is not called, then the UDMA ports works in
 * UDMA_BURST_SZ_64_BYTES mode by default.
 *
 * return 0, success.
 * return others for failure.
*/
int udma_set_burstsize(uint8_t port, udma_burstsz_t udma_to_l2_burstsize,
	udma_burstsz_t l2_to_udma_burstsize);

/**  udma_set_gapval - seperately set the UDMA port tx and rx gap value.
 * @param[in] port - udma port number, could be 0 or 1
 * @param[in] udma_to_l2_gapval - this will translate into the udma to l2switch
 * DMA stop clock cycles between burst, the gapval should be choosen from
 * udma_gapval_t, other gap values would return failure.
 * @param[in] l2_to_udma_gapval - this will translate into the l2switch to
 * udma DMA stop clock cycles between burst, the gapval should be choosen from
 * udma_gapval_t, other gap values would return failure.
 * Note that this function should be called before udma_register_handler to
 * setup initial udma gap value, and can  * be only called once in the
 * initialization stage.
 * If this function is not called, then the UDMA ports works in
 * UDMA_GAP_VAL_0_CLKS mode when in l2switch to UDMA path and
 * UDMA_GAP_VAL_64_CLKS when in UDMA to l2switch path.
 *
 * return 0, success.
 * return others for failure.
*/
int udma_set_gapval(uint8_t port, udma_gapval_t udma_to_l2_gapval,
	udma_gapval_t l2_to_udma_gapval);

/**  udma_give_free_buffer - Give a free buffer to UDMA driver, the buffer will
 * be used for packet receive.
 * @port - udma port number, could be 0 or 1
 * @buffer_desc - parameter to describe a coming buffer
 * 
 * Note the upper layer should calls this functioin periodly to give enough
 * free buffers to UDMA driver.
 *
 * return 0 on success, UDMA driver took care of the buffer.
 * return others for failure.
 * It's requried that the given Rx buffer size should be no smaller than 2KB
 * due to silicon limitation.
 */

int udma_register_handler(unsigned char port, struct net_device *dev, rx_callback_t rx_handle);

/**  udma_flush - Flush the pending requests in a UDMA port and recycling all
 * of the buffers.
 * 
 * UDMA driver will flush all of the pending Tx/Rx packets, updating the buffer
 * status and return them to upper layer.
 * The buffers may be already handled or still in a initial status
 * The upper layer is expected to recycle the return buffers.
 * 
 * @port - udma port number, could be 0 or 1
 * 
 * This function is expected to be called by upper layer when exit
 *
 * return 0 on success, flush function succeed
 * return others for failure
*/
void udma_flush(unsigned char  port);

#endif /* UDMA_API_H */
