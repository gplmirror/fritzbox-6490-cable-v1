#ifndef __RTP_TIMESTAMP_H
#define __RTP_TIMESTAMP_H

#ifdef __KERNEL__
#include <linux/skbuff.h>
#endif

enum _rtp_timestamp_state {
   ts_stopped = 0,    
   ts_running = 1,    
   ts_running_with_stamp_remove = 2    
};
 
struct _rtp_timestamp_stat {
    enum _rtp_timestamp_state state; 
    unsigned short session; /* RTP Port */
    signed long    incoming_cnt; 
    signed long    incoming_sum;
    signed long    incoming_quadsum;    
    signed long    incoming_min;
    signed long    incoming_max;
    signed long    outgoing_cnt; 
    signed long    outgoing_sum;
    signed long    outgoing_quadsum;    
    signed long    outgoing_min;
    signed long    outgoing_max;
};
#ifdef __KERNEL__
void rtp_timestamp_start_stat(unsigned short rtp_port, int remove_stamp_on_egress);
void rtp_timestamp_stop_stat(unsigned short rtp_port);
void rtp_timestamp_clear_stat(unsigned short rtp_port);

void rtp_timestamp_insert_in_skb(struct sk_buff *skb);
int rtp_timestamp_insert_in_rtp(unsigned short rtp_port, unsigned char *data, unsigned int datalen);

void rtp_timestamp_trace_session_from_skb_data(struct sk_buff *skb, unsigned short is_outgoing);
void rtp_timestamp_trace_session_from_skb_network(struct sk_buff *skb, unsigned short is_outgoing); /* for kdsldmod */
void rtp_timestamp_trace_session_from_rtp(unsigned short rtp_port, unsigned char *data, unsigned int *datalen, unsigned short is_outgoing);

void rtp_timestamp_read_stat(unsigned short rtp_port, struct _rtp_timestamp_stat *rtp_ts_stat);

int get_padding_len(void);
#endif
#endif /* __RTP_TIMESTAMP_H */
