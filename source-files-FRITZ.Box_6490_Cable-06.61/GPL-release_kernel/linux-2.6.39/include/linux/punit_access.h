/*
 *  include/linux/punit_access.h
 *
 *  GPL LICENSE SUMMARY
 *
 *  Copyright(c) 2013 Intel Corporation. All rights reserved.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of version 2 of the GNU General Public License as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
 *  The full GNU General Public License is included in this distribution
 *  in the file called LICENSE.GPL.
 *
 *  Contact Information:
 *    Intel Corporation
 *    2200 Mission College Blvd.
 *    Santa Clara, CA  97052
 *
 */

#ifndef _PUNIT_ACCESS_H
#define _PUNIT_ACCESS_H

#define P_UNIT_CMD_TYPE_IPC         (0x0 << 8)
#define P_UNIT_CMD_TYPE_BBU         (0x1 << 8)
#define P_UNIT_CMD_TYPE_WATCHDOG    (0x2 << 8)
#define P_UNIT_CMD_TYPE_RESET       (0x3 << 8)
#define P_UNIT_CMD_DATA_ATTACHED    (0x1 << 14)
#define P_UNIT_CMD_DATA_EXPECTED    (0x1 << 15)

#define P_UNIT_CMD_RESET_GET_LAST_RESET_CAUSE                   (P_UNIT_CMD_TYPE_RESET | 0x04 | P_UNIT_CMD_DATA_EXPECTED) // Returns the last reset cause indicated by firmware. If a hardware reset(not under firmware control) occurs the state indicated here is a "cold boot"
#define P_UNIT_CMD_RESET_CLR_LAST_RESET_CAUSE                   (P_UNIT_CMD_TYPE_RESET | 0x05)

#define P_UNIT_CMD_RESET_SET_WARM_RESET_ON_BUTTON               (P_UNIT_CMD_TYPE_RESET | 0x08) // When set, on a reset button press the firmware will start the warm reset sequence (warm reset by default)
#define P_UNIT_CMD_RESET_SET_COLD_RESET_ON_BUTTON               (P_UNIT_CMD_TYPE_RESET | 0x09) // When set, on a reset button press the firmware will start the cold reset sequence (warm reset by default)

#define P_UNIT_CMD_RESET_EN_ATOM_RESET_INDICATION               (P_UNIT_CMD_TYPE_RESET | 0x0C | P_UNIT_CMD_DATA_ATTACHED) // When enabled, if a reset request is sent to Punit firmware, an IPC will be sent to ATOM to inform it a reset will occur. Data Sent: 0 = Disable (default), 1 = Enable
#define P_UNIT_CMD_RESET_ATOM_RESET_INDICATION_ACK              (P_UNIT_CMD_TYPE_RESET | 0x11) // After receiving the IPC of a reset request, the ATOM should respond with this ACK IPC when it gives the firmware the OK to continue with the reset.
#define P_UNIT_CMD_RESET_ATOM_RESET_INDICATION_ACK_TIMEOUT      (P_UNIT_CMD_TYPE_RESET | 0x14 | P_UNIT_CMD_DATA_ATTACHED) // Set the timeout time in milliseconds for the ATOM to ack, if this time expires the reset sequence will proceed without an ack. Unit is in milliseconds (valid range: 0-60,000 milliseconds) (default 2,000  milliseconds)
#define P_UNIT_CMD_RESET_ATOM_RESET_INDICATION                  (P_UNIT_CMD_TYPE_RESET | 0x10 | P_UNIT_CMD_DATA_ATTACHED) // If a reset request is sent to firmware and if enabled this IPC will be sent to ATOM to inform it a reset will occur.

#define P_UNIT_CMD_WATCHDOG_DISABLE_PUNIT_WDT             (P_UNIT_CMD_TYPE_WATCHDOG | 0x0)
#define P_UNIT_CMD_WATCHDOG_ENABLE_PUNIT_WDT              (P_UNIT_CMD_TYPE_WATCHDOG | 0x1)
#define P_UNIT_CMD_WATCHDOG_REQUEST_PING_PUNIT_WDT        (P_UNIT_CMD_TYPE_WATCHDOG | 0x2)
#define P_UNIT_CMD_WATCHDOG_REQUEST_PUNIT_WDT_TIMEOUT     (P_UNIT_CMD_TYPE_WATCHDOG | 0x3)
#define P_UNIT_CMD_WATCHDOG_DO_COLD_RESET                 (P_UNIT_CMD_TYPE_WATCHDOG | 0x4)
#define P_UNIT_CMD_WATCHDOG_DO_WARM_RESET                 (P_UNIT_CMD_TYPE_WATCHDOG | 0x5)
#define P_UNIT_CMD_WATCHDOG_DO_CPU_RESET                  (P_UNIT_CMD_TYPE_WATCHDOG | 0x6)
#define P_UNIT_CMD_WATCHDOG_DO_NOTHING                    (P_UNIT_CMD_TYPE_WATCHDOG | 0x7)

#define P_UNIT_CMD_BBU_ENTER_BBU_MODE  (P_UNIT_CMD_TYPE_BBU | 0x00)
#define P_UNIT_CMD_BBU_EXIT_BBU_MODE  (P_UNIT_CMD_TYPE_BBU | 0x02)//initiat from 8051 to ATOM
#define P_UNIT_CMD_BBU_DISABLE_RTC_ALARM_BBU_EXIT  (P_UNIT_CMD_TYPE_BBU | 0x10)
#define P_UNIT_CMD_BBU_ENABLE_RTC_ALARM_BBU_EXIT  (P_UNIT_CMD_TYPE_BBU | 0x11)

/* ioctl interfaces */
struct punit_cmd
{
        unsigned int command;
        unsigned int data;
};
#define SOC_P_UNIT_MODULE_ID                    (0xDA)
#define P_UNIT_CMD           _IOR  (SOC_P_UNIT_MODULE_ID, 1, struct punit_cmd) // if no data needed, assign field data 0
#define P_UNIT_CMD_WR_DATA   _IOR (SOC_P_UNIT_MODULE_ID, 2, struct punit_cmd)
#define P_UNIT_CMD_RD_DATA   _IOWR (SOC_P_UNIT_MODULE_ID, 3, struct punit_cmd)

#ifdef __KERNEL__
/* kernel module export interfaces */
extern uint32_t  punit_reset_event;
extern int p_unit_access_avaiable(void);
extern void p_unit_acquire_lock(void);
extern void p_unit_release_lock(void);
extern int p_unit_get_FwVersion(unsigned int *fw_version);
extern int p_unit_get_flag(uint32_t *flag);
extern int p_unit_cmd(unsigned int command);
extern int p_unit_cmd_wr_data(unsigned int  command, unsigned int  data);
extern int p_unit_cmd_rd_data(unsigned int command, unsigned int *data);
#endif

#endif  /* _PUNIT_ACCESS_H */
