#include <linux/version.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/ioport.h>
#include <linux/avm_power.h>
#include "avm_power.h"

static void *usbpower_handle;

#if defined(CONFIG_MIPS_UR8)
#include <asm/mach-ur8/hw_gpio.h>
#include <asm/mach-ur8/hw_irq.h>
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 32)
#include <asm/mach-ur8/ur8_clk.h>
#endif
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static struct resource usb_gpioressource = {
    .name  = "usb_power",
    .flags = IORESOURCE_IO,		 
    .start = GPIO_BIT_DRVVBUS,
    .end   = GPIO_BIT_DRVVBUS,
};
#endif/*--- #if defined(CONFIG_MIPS_UR8) ---*/

/*--------------------------------------------------------------------------------*\
 * state: 0x1:  5 V an  (nur UR8)
 *        0x0   5 V aus (nur UR8)
 *        0x2   liefere aktuellen USB-Stromverbrauch  (alle Hosts)  
\*--------------------------------------------------------------------------------*/
static int avm_power_usb_Callback(int state){
    /*--- printk("avm_power_usb_Callback: %d\n", state); ---*/
    if(state < 2) {
#if defined(CONFIG_MIPS_UR8)
        avm_gpio_out_bit(GPIO_BIT_DRVVBUS, state ? 1 : 0);
#endif/*--- #if defined(CONFIG_MIPS_UR8) ---*/
    } else {
        /*--- liefere SUM(mA) zurueck ---*/
        return pm_ressourceinfo.deviceinfo[powerdevice_usb_host].power_rate +
               pm_ressourceinfo.deviceinfo[powerdevice_usb_host2].power_rate +
               pm_ressourceinfo.deviceinfo[powerdevice_usb_host3].power_rate;
    }
    return 0;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
int avm_power_usb_init(void) {
#if defined(CONFIG_MIPS_UR8)
    if(request_resource(&gpio_resource, &usb_gpioressource)) {
        panic("[avmpower]bye bye - can't initialize avmpower-interface!\n"); 
    }
    avm_gpio_ctrl(GPIO_BIT_DRVVBUS, GPIO_PIN, GPIO_OUTPUT_PIN);
#endif/*--- #if defined(CONFIG_MIPS_UR8) ---*/
    usbpower_handle = PowerManagmentRegister("usbpower", avm_power_usb_Callback);
    return usbpower_handle ? 0 : -1;
}
#ifdef CONFIG_AVM_POWER_MODULE
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
void avm_power_usb_exit(void) {
    if(usbpower_handle){
        PowerManagmentRelease(usbpower_handle);
#if defined(CONFIG_MIPS_UR8)
        release_resource(&usb_gpioressource);
#endif/*--- #if defined(CONFIG_MIPS_UR8) ---*/
        usbpower_handle = NULL;
    }
}
#endif/*--- #ifdef CONFIG_AVM_POWER_MODULE ---*/
