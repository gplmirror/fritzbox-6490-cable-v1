#include <linux/version.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/avm_power.h>
#include "avm_power.h"


#if defined(CONFIG_MIPS_UR8)
#include <asm/mach-ur8/hw_gpio.h>
#include <asm/mach-ur8/hw_irq.h>
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2, 6, 32)
#include <asm/mach-ur8/ur8_clk.h>
#endif
#endif/*--- #if defined(CONFIG_MIPS_UR8) ---*/


static void *wlanboost_handle __attribute__((unused));
#if defined(CONFIG_MIPS_UR8)
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int avm_power_wlan_Callback(int state){

    unsigned int value;

#define DEV_SARAS_NM_PWDREG_ADDR       (0xA1018000 | 0x38)
#define DEV_SARAS_PM_NM_REG1_ADDR      (0xA1018000 | 0x0c)

#define DEV_SARAS_NM_PWD               (0xAB)

    /*--- printk("avm_power_wlan_Callback: %d\n", state); ---*/
    *(volatile unsigned int *)DEV_SARAS_NM_PWDREG_ADDR = DEV_SARAS_NM_PWD;

    switch (state) {
        case 1:     /*--- on ---*/
            value = *(volatile unsigned int *)DEV_SARAS_PM_NM_REG1_ADDR;
            /*--- printk("[wlan_booston] 0x%x 0x%x\n", DEV_SARAS_PM_NM_REG1_ADDR, value); ---*/
            *(volatile unsigned int *)DEV_SARAS_PM_NM_REG1_ADDR = value | (1<<6);  /*--- set 1.2V ---*/ 
            break;
        case 0:     /*--- off ---*/
            value = *(volatile unsigned int *)DEV_SARAS_PM_NM_REG1_ADDR;
            /*--- printk("[wlan_boostoff] 0x%x 0x%x\n", DEV_SARAS_PM_NM_REG1_ADDR, value); ---*/
            *(volatile unsigned int *)DEV_SARAS_PM_NM_REG1_ADDR = value & ~(1<<6);  /*--- set 1.1V ---*/ 
            break;
        default:
            break;
    }

    *(volatile unsigned int *)DEV_SARAS_NM_PWDREG_ADDR = 0;

    return 0;
}
#endif/*--- #if defined(CONFIG_MIPS_UR8) ---*/

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
int avm_power_wlan_init(void) {
#if defined(CONFIG_MIPS_UR8)
    wlanboost_handle = PowerManagmentRegister("wlanboost", avm_power_wlan_Callback);        /*--- für die 7212 ---*/
    return wlanboost_handle ? 0 : -1;
#else
    return 0;
#endif/*--- #if defined(CONFIG_MIPS_UR8) ---*/
}
#ifdef CONFIG_AVM_POWER_MODULE
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
void avm_power_wlan_exit(void) {
    if(wlanboost_handle) {
        PowerManagmentRelease(wlanboost_handle);
        wlanboost_handle = NULL;
    }
}
#endif/*--- #ifdef CONFIG_AVM_POWER_MODULE ---*/
