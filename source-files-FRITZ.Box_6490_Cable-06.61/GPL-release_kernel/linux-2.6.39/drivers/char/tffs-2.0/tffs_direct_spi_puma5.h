#if !defined(_HW_SPI_H_)
#define _HW_SPI_H_

/*------------------------------------------------------------------------------------------*\
 * Definitionen SPI-Flash
 * nach einigen Komandos muss CE wieder high sein -> deshalb gleich (x << 24)
 * gesetzt werden diese mit spi_cmd_simple
 * beim PUMA-SPI-Interface hat sich das Interface ge�ndert, Kommandos werden nicht geshiftet
\*------------------------------------------------------------------------------------------*/
#if defined(CONFIG_ARCH_PUMA5) || defined(CONFIG_MIPS_AR9) || defined(CONFIG_MIPS_VR9) || defined(CONFIG_MIPS_AR7242)
    #define SPI_CMD_SHIFT_24   0
#else
    #define SPI_CMD_SHIFT_24   24
#endif
#define SPI_WRITE_ENABLE                    (0x06 << SPI_CMD_SHIFT_24)       /*--- write enable ---*/
#define SPI_WRITE_DISABLE                   (0x04 << SPI_CMD_SHIFT_24)       /*--- write disable ---*/
#define SPI_READ_ID                         (0x9F << 24)       /*--- read identification ---*/
#define SPI_READ_STATUS                     (0x05 << SPI_CMD_SHIFT_24)       /*--- read status register ---*/
#define SPI_WRITE_STATUS                    (0x01 << 24)       /*--- write status register ---*/
#define SPI_READ                            (0x03 << 24)       /*--- read data ---*/
#define SPI_FASTREAD                        (0x0B << 24)       /*--- fast read data ---*/
#define SPI_PARALLEL_MODE                   (0x55 << 24)       /*--- parallel mode ---*/
#define SPI_SECTOR_ERASE                    (0xD8 << 24)       /*--- sector erase ---*/ 
#define SPI_CHIP_ERASE                      (0xD7 << 24)       /*--- chip erase ---*/
#define SPI_PAGE_PROGRAM                    (0x02 << 24)       /*--- page program ---*/
#define SPI_DEEP_POWER_DOWN                 (0xB9 << 24)       /*--- deep power down ---*/
#define SPI_ENTER_4K                        (0xA5 << 24)       /*--- enter 4kb ---*/
#define SPI_EXIT_4K                         (0xB5 << 24)       /*--- exit 4kb ---*/
#define SPI_RELEASE_POWER_DOWN              (0xAB << 24)       /*--- release from deep power-down ---*/
#define SPI_READ_ELECTRONIK_ID  SPI_REALEASE_POWER_DOWN        /*--- read electronic id ---*/
#define SPI_READ_ELECTRONIC_ID_MANUFACTURE  (0x9F << SPI_CMD_SHIFT_24)       /*--- read electronic manufacturer id & device id ---*/

/*--- status bits ---*/ 
#define WIP                 (1 << 0)
#define WEL                 (1 << 1)
#define BP0                 (1 << 2)
#define BP1                 (1 << 3)
#define BP2                 (1 << 4)
#define BP3                 (1 << 5)
#define PROG_ERROR          (1 << 6)
#define REG_WRITE_PROTECT   (1 << 7)


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define SPI_CLK_OFF         0x00
#define SPI_CFG_OFF         0x04
#define SPI_CMD_OFF         0x08
#define SPI_STAT_OFF        0x0C
#define SPI_DATA_OFF        0x10


#define SPI_CLK_EN          (1<<31)

#define SPI_CMD_READ        (1 << 16)
#define SPI_CMD_WRITE       (2 << 16)
#define SPI_CMD_MODE3       (1 << 18)
#define SPI_WORD_LEN_8      ((8-1) << 19)  
#define SPI_WORD_LEN_16     ((16-1) << 19)  
#define SPI_WORD_LEN_24     ((24-1) << 19)  
#define SPI_WORD_LEN_32     ((32-1) << 19)  
#define SPI_USE_CS0         (0 << 28)
#define SPI_USE_CS1         (1 << 28)
#define SPI_USE_CS2         (2 << 28)
#define SPI_USE_CS3         (3 << 28)

#define SPI_MAX_FRAME_LEN   4096        /*--- 11 Bit Framel�nge, es k�nnen 32 Bit pro Frame �bertragen werden ---*/

#define AVALANCHE_SPI_SIZE                 0xFF

#if !defined(_ASSEMBLER_)
union _spi_clk_ctrl {
    volatile unsigned int Register;
    struct __spi_clk_ctrl {
        unsigned int enable         : 1;
        unsigned int reserved       : 15;
        unsigned int dclk_div       : 16;
    } Bits;
};

union _spi_device_cfg {
    volatile unsigned int Register;
    struct __spi_device_cfg {
        unsigned int reserved3  : 3;
        unsigned int dd3        : 1;
        unsigned int ckph3      : 1;
        unsigned int csp3       : 1;
        unsigned int ckp3       : 1;
        unsigned int reserved2  : 3;
        unsigned int dd2        : 1;
        unsigned int ckph2      : 1;
        unsigned int csp2       : 1;
        unsigned int ckp2       : 1;
        unsigned int reserved1  : 3;
        unsigned int dd1        : 1;
        unsigned int ckph1      : 1;
        unsigned int csp1       : 1;
        unsigned int ckp1       : 1;
        unsigned int reserved0  : 3;
        unsigned int dd0        : 1;
        unsigned int ckph0      : 1;
        unsigned int csp0       : 1;
        unsigned int ckp0       : 1;
    } Bits;
};

union _spi_cmd {
    volatile unsigned int Register;
    struct __spi_cmd {
        unsigned int reserved0  : 2;
        unsigned int cs_num     : 2;
        unsigned int reserved2  : 4;
        unsigned int wlen       : 5;
        unsigned int cmd_mode   : 1;
        unsigned int cmd        : 2;
        unsigned int firq       : 1;
        unsigned int wirq       : 1;
        unsigned int reserved3  : 2;
        unsigned int flen       : 12;
    } Bits;
};

union _spi_sr {
    volatile unsigned int Register;
    struct __spi_sr {
        unsigned int reserved1           : 3;
        volatile unsigned int wdcnt      : 13;
        unsigned int reserved0           : 13;
        volatile unsigned int fc         : 1;
        volatile unsigned int wc         : 1;
        volatile unsigned int busy       : 1;
    } Bits;
};

union _data_sr {
    volatile unsigned int Register;
    unsigned char Byte[4];
} data;

union _SFI_setup {
    volatile unsigned int Register;
    struct __sfi_setup {
        volatile unsigned int reserved0     : 8;
        volatile unsigned int wcmd          : 8;
        volatile unsigned int reserved1     : 3;
        volatile unsigned int dual_read     : 1;
        volatile unsigned int num_d_bytes   : 2;
        volatile unsigned int num_a_bytes   : 2;
        volatile unsigned int rcmd          : 8;
    } Bits;
};

union _SFI_switch {
    volatile unsigned int Register;
    struct __sfi_switch {
        volatile unsigned int reserved      : 30;
        volatile unsigned int mm_int_en     : 1;
        volatile unsigned int mmpt_s        : 1;
    } Bits;
};

struct _spi_register {
    volatile unsigned int       ProductID;
    union _spi_clk_ctrl         clk_ctrl;
    union _spi_device_cfg       device_cfg;
    union _spi_cmd              cmd;
    union _spi_sr               status;
    union _data_sr              data;
    union _SFI_setup            sfi[2];
    volatile unsigned int       reserved[2];
    union _SFI_switch           sfi_switch;
};

#endif /*--- #if !defined(_ASSEMBLER_) ---*/
#endif
