#ifndef __avm_profile_h__
#define __avm_profile_h__

/*--------------------------------------------------------------------------------*\
 * each entry means one realcore - can divided on mips in virtual cores (vpe_nr)
\*--------------------------------------------------------------------------------*/
struct _cpucore_profile {
    unsigned int cpu_nr_offset;
    unsigned int vpe_nr;        /*--- on mips ---*/
};
extern int profilestat_category(char *txtbuf, int txtbuf_len, unsigned int cpu_offset, unsigned int cpus, unsigned int full);
extern int profilestat_totalcall(char *txtbuf, int txtbuf_len, unsigned int cpu_offset, unsigned int cpus, unsigned int weight);
extern int get_user_info(char *buf, unsigned int maxbuflen, pid_t pid, unsigned long addr);
extern int __get_userinfo(char *buf, unsigned int maxbuflen, struct mm_struct *mmm, unsigned long addr);
#endif/*--- #ifndef __avm_profile_h__ ---*/
