/*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2004 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
 \*------------------------------------------------------------------------------------------*/
#ifdef CONFIG_SMP
#define __SMP__
#endif /*--- #ifdef CONFIG_SMP ---*/

#include <linux/module.h>
#include <linux/version.h>
#include <linux/slab.h>
#include <asm/fcntl.h>
/*--- #include <linux/devfs_fs_kernel.h> ---*/
#include <linux/mtd/mtd.h>
#include <linux/tffs.h>
#include <linux/sched.h>
#include <linux/errno.h>
#include <linux/vmalloc.h>
#include <linux/zlib.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/wait.h>
#include <linux/mutex.h>
#include <linux/io.h>
#include <linux/kernel.h>

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
/*--- #define TFFS_DEBUG ---*/
#include <linux/tffs.h>
#include "tffs_local.h"
#include "tffs_legacy.h"

#ifdef SPI_PANIC_LOG
int tffs_spi_read(unsigned int address, unsigned int mtd_id, unsigned char *pdata, unsigned int len);
int tffs_spi_write(unsigned int address, unsigned int mtd_id, unsigned char *pdata, unsigned int len);

#if defined(CONFIG_VR9) || defined(CONFIG_AR10) || defined(CONFIG_MACH_PUMA5) || defined(CONFIG_MACH_PUMA6)
#define SPI_PANIC_MODE      (tffs_panic_mode && tffs_spi_mode)
#endif /*--- #if defined(CONFIG_VR9) ---*/

#endif /*--- #ifdef(SPI_PANIC_LOG) ---*/

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/

// TODO: get rid of sparsely populated array, use something like a hashmap
static loff_t TFFS_Global_Index[FLASH_FS_ID_LAST];

int tffs_mtd[2] = { CONFIG_TFFS_MTD_DEVICE_0, CONFIG_TFFS_MTD_DEVICE_1 };
int tffs_mtd_offset[2];
unsigned char *TFFS3_Cleanup_Buffer = NULL;

static int TFFS3_LGCY_Reindex(struct tffs_module *this);
static int TFFS3_LGCY_Cleanup(struct tffs_module *this, void *handle);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static inline int MTD_READ(struct tffs_mtd *local_TFFS, loff_t from, size_t len, size_t *retlen, u_char *buf)
{
//    pr_err("[%s] Called\n", __func__);

    if(local_TFFS == NULL){
        pr_err("[%s] mtd_info/mtd_info->read is NULL\n", __func__);
        return -EFAULT;
    }
#ifdef SPI_PANIC_LOG
    if(SPI_PANIC_MODE){
        *retlen = tffs_spi_read((unsigned int)from, ((current_mtd == avail_mtd[0]) ? 0 : 1), buf, len);
        return 0;
    }
#endif
    return mtd_read(local_TFFS->mtd, from, len, retlen, buf);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static inline int MTD_WRITE(struct tffs_mtd *local_TFFS, loff_t from, size_t len, size_t *retlen, u_char *buf)
{
//    pr_err("[%s] Called\n", __func__);

#ifdef SPI_PANIC_LOG
    if(SPI_PANIC_MODE){
        *retlen = tffs_spi_write((unsigned int)from, ((current_mtd == avail_mtd[0]) ? 0 : 1), buf, len);
        return 0;
    }
#endif /*--- #ifdef SPI_PANIC_LOG ---*/

    if(local_TFFS == NULL){
        pr_err("[%s] mtd_info/mtd_info->write is NULL\n", __func__);
        return -EFAULT;
    }
    return mtd_write(local_TFFS->mtd, from, len, retlen, buf);
}

static inline int MTD_READ_HDR(struct tffs_mtd *local_TFFS, loff_t from, struct _TFFS_Entry *E)
{
    int result;
    struct _TFFS_Entry Entry;
    size_t retlen;

    result = MTD_READ(local_TFFS, from, sizeof(Entry), &retlen, (u_char *) &Entry);

    if(result == 0 && retlen == sizeof(Entry)){
        E->ID = be16_to_cpu(Entry.ID);
        E->Length = be16_to_cpu(Entry.Length);
    } else {
        result = -EIO;
    }

    return result;
}

static inline int MTD_WRITE_HDR(struct tffs_mtd *local_TFFS, loff_t from, struct _TFFS_Entry *E)
{
    int result;
    struct _TFFS_Entry Entry;
    size_t retlen;

    Entry.ID = cpu_to_be16(E->ID);
    Entry.Length = cpu_to_be16(E->Length);

    result = MTD_WRITE(local_TFFS, from, sizeof(Entry), &retlen, (u_char *) &Entry);
    if(result != 0 || retlen != sizeof(Entry)){
        result = -EIO;
    }

    return result;
}

static inline unsigned int align_len(unsigned int len)
{
    return (len + 3) & ~0x3;
}
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static inline int get_mtd_device_wrapped(struct tffs_mtd *tffs_mtd, unsigned int mtd_idx)
{
    pr_err("[%s] Called\n", __func__);

    tffs_mtd->mtd = get_mtd_device(NULL, mtd_idx);
    if(IS_ERR(tffs_mtd->mtd)){
        return -1;
    }

    tffs_mtd->mtd_idx = mtd_idx;
    tffs_mtd->size = tffs_mtd->mtd->size;

    return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static inline void put_mtd_device_wrapped(struct tffs_mtd *tffs_mtd)
{
    pr_err("[%s] Called\n", __func__);

    put_mtd_device(tffs_mtd->mtd);
}



#if 0
/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
void TFFS_Deinit(void)
{
    DBG((KERN_INFO "TFFS_Deinit()\n"));
    if(!TFFS_mtd.use_bdev)
        put_mtd_device_wrapped(&TFFS_mtd);
    TFFS_mtd_number = (unsigned int) -1;
    TFFS_mtd.tffs.mtd = NULL;
}
#endif

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
static void *TFFS3_LGCY_Open(struct tffs_module *this, struct tffs_core_handle *core_handle)
{
    struct tffs_lgcy_ctx *ctx;
    struct TFFS_LGCY_State *state;

//    pr_err("[%s] Called\n", __func__);

    ctx = (struct tffs_lgcy_ctx *) this->priv;
    core_handle->max_segment_size = (32 * 1024) + 16; // FIXME

    // when opened in panic mode, use static state struct and if there is a special panic
    // mtd_info set up, use that one
    state = NULL;
    if(core_handle->mode == tffs3_mode_panic){
        if(ctx->in_panic_mode == 0){
            state = &(ctx->panic_state);
            ctx->in_panic_mode = 1;
        }
    } else {
        state = kmalloc(sizeof(*state), GFP_KERNEL);
    }

    if(state == NULL) {
        pr_err("[%s] malloc(%u) failed\n", __func__, sizeof(*state));
        goto err_out;
    }

    state->offset = 0;
    state->id = core_handle->id;

    if(ctx->idx_created == 0){
        TFFS3_LGCY_Reindex(this);
        ctx->idx_created = 1;
    }

err_out:
    return state;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
static int TFFS3_LGCY_Close(struct tffs_module *this, void *handle)
{
    struct tffs_lgcy_ctx *ctx;
    struct TFFS_LGCY_State *state;

//    pr_err("[%s] Called\n", __func__);

    ctx = (struct tffs_lgcy_ctx *) this->priv;

    if(ctx && ctx->active_mtd){
        mtd_sync(ctx->active_mtd->mtd);
    }

    state = (struct TFFS_LGCY_State *) handle;

    if(ctx->in_panic_mode == 0){
        kfree(state);
    }

    return 0;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
static void TFFS3_LGCY_Format_Callback(struct erase_info *instr)
{
    DBG(("TFFS: mtd_erase_callback\n"));
    switch(instr->state){
    case MTD_ERASE_PENDING:
        break;
    case MTD_ERASING:
        break;
    case MTD_ERASE_SUSPEND:
        break;
    case MTD_ERASE_FAILED:
    case MTD_ERASE_DONE:
        wake_up((wait_queue_head_t *) instr->priv);
        break;
    }
    return;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
static int TFFS3_LGCY_Format(struct tffs_mtd *tffs_mtd)
{
    struct erase_info *erase;
    int ret = 0;

    DECLARE_WAITQUEUE(wait, current);
    wait_queue_head_t wait_q;

    pr_err("[%s] Called\n", __func__);

    init_waitqueue_head(&wait_q);

    erase = (struct erase_info *) kzalloc(sizeof(struct erase_info), GFP_KERNEL);
    if(erase == NULL ){
        DBG((KERN_ERR "TFFS_LGCY_Format: malloc(%u) failed\n", sizeof(struct erase_info)));
        return -ENOMEM;
    }

    DBG((KERN_INFO "TFFS_LGCY_Format: malloc(%u) success\n", sizeof(struct erase_info)));
    erase->mtd = tffs_mtd->mtd;
    erase->addr = 0;
    erase->len = tffs_mtd->mtd->size;
    erase->callback = TFFS3_LGCY_Format_Callback;
    erase->priv = (u_long) &wait_q;
    erase->next = NULL;

    DBG((KERN_INFO "TFFS_LGCY_Format: erase: addr %llx len %llx\n", erase->addr, erase->len));

    ret = mtd_erase(tffs_mtd->mtd, erase);

    DBG((KERN_INFO "TFFS_LGCY_Format: erase: ret 0x%x\n", ret));
    if(!ret){
        set_current_state(TASK_UNINTERRUPTIBLE);
        add_wait_queue(&wait_q, &wait);
        if(erase->state != MTD_ERASE_DONE && erase->state != MTD_ERASE_FAILED){
            schedule();
        }

        remove_wait_queue(&wait_q, &wait);
        set_current_state(TASK_RUNNING);

        ret = (erase->state == MTD_ERASE_FAILED) ? -EIO : 0;
        if(ret){
            DBG((KERN_ERR "Failed (callback) to erase mtd, region [0x%llx, 0x%llx]\n", erase->addr,erase->len));
        }
    }else{
        DBG((KERN_ERR "Failed to erase mtd, region [0x%llx, 0x%llx]\n", erase->addr,erase->len));
        ret = -EIO;
    }

    kfree(erase);
    return ret;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
enum _tffs_memcmp
{
    tffs_memcmp_equal, tffs_memcmp_writeable, tffs_memcmp_clear_required
};

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
static enum _tffs_memcmp TFFS3_Memcmp(unsigned char *FlashMemory, unsigned char *NewMemory,
        unsigned int Length)
{
    unsigned int ret;
    enum _tffs_memcmp result;

    pr_err("[%s] Called\n", __func__);

    ret = memcmp(FlashMemory, NewMemory, Length);
    if(ret == 0){
        result = tffs_memcmp_equal;
    } else {
        result = tffs_memcmp_writeable;
        while(Length--){
            if((*FlashMemory & *NewMemory) != *NewMemory){
                result = tffs_memcmp_clear_required;
                break;
            }
            ++FlashMemory;
            ++NewMemory;
        }
    }

    return result;
}

#if 0
static int find_entry(struct tffs_lgcy_ctx *ctx, enum _tffs_id Id, loff_t from, loff_t *at, unsigned short *found)
{
    struct _TFFS_Entry Entry;
    int result;
    size_t retlen;

    result = -ENOENT;
    do{
        result = MTD_READ(ctx->active_mtd, from, sizeof(struct _TFFS_Entry),
                          &retlen, (unsigned char *) &Entry);
        if(result != 0){
            result = -EIO;
            break;
        }

        tffs_write_statistic(FLASH_FS_ID_SKIP, retlen, 0, 0); /*--- internal read ---*/

        *at = from;
        *found = Entry.ID;
        result = 0;

        from += sizeof(struct _TFFS_Entry) + ((Entry.Length + 3) & ~0x03);
    }while(*found != Id && *found != FLASH_FS_ID_FREE && (from + sizeof(struct _TFFS_Entry)) < ctx->active_mtd->size);

    return result;
}
#endif

static int do_write(struct tffs_lgcy_ctx *ctx, enum _tffs_id Id, uint8_t *write_buffer, size_t write_length, size_t *rlen)
{
    int result, retlen;
    struct _TFFS_Entry Entry;
    loff_t offset, clear_offset, write_offset;
    size_t clear_size;
    unsigned int Len, fill;
    uint8_t *rd_buf;

    *rlen = write_length;
    rd_buf = NULL;
    result = 0;
    Len = 0;
    offset = 0;
    write_offset = -1;
    clear_offset = -1;
    clear_size = 0;
    fill = 0;

    // if entry is to be cleared, invalidate it in global index
    if(write_buffer == NULL){
        TFFS_Global_Index[Id] = -1;
    }

    do{
        result = MTD_READ_HDR(ctx->active_mtd, offset, &Entry);
        if(result != 0){
            goto err_out;
        }

        tffs_write_statistic(FLASH_FS_ID_SKIP, sizeof(Entry), 0, 0); /*--- internal read ---*/

        /*---------------------------------------------------------------------------------------*\
         * ID found
         \*---------------------------------------------------------------------------------------*/
        if(Entry.ID == (unsigned short) Id && Id < FLASH_FS_DROPABLE_DATA){ /*---  old entry found ----*/
            DBG((KERN_INFO "found old entry with ID=%x ", Id));

            /*-----------------------------------------------------------------------------------*\
             * if we haven't found a place to write the data to yet, check if we can re-use
             * this old entry. Skip test when in panic mode because of the kmalloc needed
            \*-----------------------------------------------------------------------------------*/
            if(write_offset < 0 && ctx->in_panic_mode == 0 && Entry.Length == write_length){
                rd_buf = kmalloc(Entry.Length, GFP_KERNEL);
                if(rd_buf == NULL){
                    DBG((KERN_ERR "TFFS_LGCY_Write: malloc(%u) failed\n", Entry.Length));
                    result = -ENOMEM;
                    goto err_out;
                }

                DBG((KERN_INFO "TFFS_LGCY_Write: malloc(%u) success\n", Entry.Length));
                result = MTD_READ(ctx->active_mtd, offset + sizeof(Entry),
                                   Entry.Length, &retlen, rd_buf);
                if(result){
                    DBG((KERN_ERR "TFFS_LGCY_Write: MTD_READ failed\n"));
                    goto err_out;
                }

                tffs_write_statistic(Entry.ID, retlen, 0, 0);

                switch(TFFS3_Memcmp(rd_buf, write_buffer, Entry.Length)){
                case tffs_memcmp_equal:
                    DBG((KERN_INFO "old and new entry identical\n"));
                    /* identical entry found, we can abort search here. There is a corner case
                     * where we found an entry that has been written earlier but somehow the
                     * previous entry for that one has not been cleared.
                     * Update global index and make sure an earlier entry gets cleared now.
                     */
                    TFFS_Global_Index[Entry.ID] = offset;
                    result = 0;
                    goto clear_old;
                    break;
                case tffs_memcmp_writeable:
                    DBG((KERN_INFO "old entry can be overwritten by new one\n"));
                    write_offset = offset;
                    break;
                default:
                case tffs_memcmp_clear_required:
                    break;
                }
                kfree(rd_buf);
                rd_buf = NULL;
            }

            /*
             * remember entry for clearing later, unless we are going to overwrite it
             */
            if(write_offset != offset){
                if(clear_offset < 0){
                    /* first entry found for this id. This will have to be cleared when a
                     * new valid entry has been written
                     */
                    clear_offset = offset;
                    clear_size = Entry.Length;

                } else {
                    /* only the first entry for a given ID is valid. If we find a second
                     * (third, fourth...) entry for this ID, it must be a remnant of a failed
                     * write and can therefore safely be marked as erased
                     */
                    Entry.ID = FLASH_FS_ID_SKIP; // safe to change, can't be FLASH_FS_ID_FREE checked for later
                    result = MTD_WRITE_HDR(ctx->active_mtd, offset, &Entry);

                    if(result != 0){
                        DBG((KERN_ERR "TFFS: write TFFS_LGCY_Entry (id=SKIP) failed\n"));
                        goto err_out;
                    }
                }
            }
        }

        /*---------------------------------------------------------------------------------------*\
         * found start of free space. Append new entry here unless we already found an earlier
         * entry we can recycle
        \*---------------------------------------------------------------------------------------*/
        if(Entry.ID == FLASH_FS_ID_FREE && write_offset < 0){
            DBG((KERN_INFO "found free entry\n"));
            write_offset = offset;
        }

        Len = align_len(Entry.Length);
        offset += (sizeof(struct _TFFS_Entry) + Len);
    }while(Entry.ID != FLASH_FS_ID_FREE && (offset + sizeof(Entry) + write_length) <= ctx->active_mtd->size);

    /*
     * if there is data to be written, write it to the place we have found above
     */
    if(write_buffer != NULL){
        if(write_offset >= 0){
            Entry.ID = (unsigned short) Id;
            Entry.Length = (unsigned short) write_length;

            result = MTD_WRITE_HDR(ctx->active_mtd, write_offset, &Entry);
            if(result != 0){
                DBG((KERN_ERR "TFFS: write TFFS_LGCY_Entry (id=0x%x, ptr 0x%x %u bytes) failed, reason %d\n", Id, (unsigned int)E, sizeof(struct _TFFS_Entry), result));
                goto err_out;
            }

            tffs_write_statistic(FLASH_FS_ID_SKIP, sizeof(struct _TFFS_Entry), 1, 0); /*--- internal write ---*/

            DBG((KERN_INFO "header with id %x written\n", Id));

            result = MTD_WRITE(ctx->active_mtd, write_offset + sizeof(struct _TFFS_Entry),
                               Entry.Length, &retlen, write_buffer);

            if(result != 0 || (retlen != Entry.Length)){
                DBG((KERN_ERR "TFFS: write data (id=0x%x, ptr 0x%x %u bytes) failed, reason %d\n", Id, (unsigned int)E + sizeof(struct _TFFS_Entry), Entry.Length, result));
                result = result ? result : -EIO;
                goto err_out;
            }

            tffs_write_statistic(Entry.ID, retlen, 1, 0);

            // write succeeded, update global index
            TFFS_Global_Index[Entry.ID] = write_offset;

            DBG((KERN_INFO "%d bytes data written\n", write_length));
        } else {
            /* if we found nowhere to put the data, we must have run out of space.
             * Return file table overflow error, so wrapper func may trigger a cleanup and retry.
             */
            result = -ENFILE;
            goto err_out;
        }
    }

clear_old:
    /*
     * if an entry already existed, clear it out after the new one has been written
     */
    if(clear_offset >= 0){
        Entry.ID = FLASH_FS_ID_SKIP;
        Entry.Length = clear_size;

        result = MTD_WRITE_HDR(ctx->active_mtd, clear_offset, &Entry);
        if(result != 0){
            DBG((KERN_ERR "TFFS: write TFFS_LGCY_Entry (id=SKIP) failed\n"));
            goto err_out;
        }

        tffs_write_statistic(FLASH_FS_ID_SKIP, sizeof(struct _TFFS_Entry), 1, 0); /*--- internal write ---*/

        DBG((KERN_INFO "cleared ID=0x%x Len=%u\n", Id, clear_size));
    }

    /*
     * something changed. Send notification for ID
     */
    if((write_offset >= 0 || clear_offset >= 0) && ctx->notify_cb != NULL){
        ctx->notify_cb(ctx->notify_priv, Id, tffs3_notify_update);
    }

    /*
     *   check if filesystem is more than 75% full
     */
    if(ctx->in_panic_mode == 0){
        fill = offset + Entry.Length;
        fill *= 100;
        fill /= (unsigned int) ctx->active_mtd->size;

        DBG((KERN_INFO "fill level %u%%\n", Fuell));
        if(fill > 75){
            pr_err("TFFS: fill level > 75%% ... trigger Cleanup\n");
            tffs_send_event(TFFS_EVENT_CLEANUP);
        }
    }

err_out:
    if(rd_buf != NULL){
        kfree(rd_buf);
    }

    return result;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
static int TFFS3_LGCY_Write(struct tffs_module *this, void *handle, uint8_t *write_buffer,
                              size_t write_length, size_t *rlen, unsigned int final)
{
    struct tffs_lgcy_ctx *ctx;
    struct TFFS_LGCY_State *state;
    int result;

//    pr_err("[%s] Called\n", __func__);

    state = (struct TFFS_LGCY_State *) handle;
    if(state->id >= FLASH_FS_ID_LAST){
        pr_err("[%s] invalid tffs_id: 0x%x\n", __func__, state->id);
        return -ENOENT;
    }

    if(!final){
        return -ENOSPC;
    }

    ctx = (struct tffs_lgcy_ctx *)this->priv;
    if(ctx == NULL){
        return -EBADF;
    }

    result = do_write(ctx, state->id, write_buffer, write_length, rlen);
    // no space in current segment. Retry after switching segments unless we are in panic mode
    if(result == -ENFILE && ctx->in_panic_mode == 0){
        result = TFFS3_LGCY_Cleanup(this, handle);
        if(result != 0){
            result = -ENOSPC;
            goto err_out;
        }

        result = do_write(ctx, state->id, write_buffer, write_length, rlen);
    }

    if(result == 0 && ctx->notify_cb != NULL && ctx->in_panic_mode == 0){
        ctx->notify_cb(ctx->notify_priv, state->id, tffs3_notify_update);
    }

err_out:
    return result;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
static int TFFS3_LGCY_Read(struct tffs_module *this, void *handle, uint8_t *read_buffer, size_t *read_length)
{
    struct tffs_lgcy_ctx *ctx;
    struct TFFS_LGCY_State *state;
    unsigned int retlen, ret;
    struct _TFFS_Entry Entry;
    loff_t offset;
    size_t Len;

//    pr_err("[%s] Called\n", __func__);

    ctx = (struct tffs_lgcy_ctx *)this->priv;
    if(ctx == NULL){
        return -EBADF;
    }

    state = (struct TFFS_LGCY_State *) handle;

    if(state->id >= FLASH_FS_ID_LAST){
        pr_err("[%s] invalid tffs_id: 0x%x\n", __func__, state->id);
        return -ENOENT;
    }

    /*--- DBG((KERN_INFO "TFFS_LGCY_Read(handle=%x): id = 0x%x, max_length = %u\n", (unsigned int)handle, (unsigned int)Id, *read_length)); ---*/

    offset = TFFS_Global_Index[state->id];
    if(offset >= 0){
        DBG((KERN_INFO "Eintrag gefunden\n"));
        ret = MTD_READ_HDR(ctx->active_mtd, offset, &Entry);
        if(ret != 0){
            return ret;
        }

        tffs_write_statistic(FLASH_FS_ID_SKIP, retlen, 0, 0); /*--- internal read ---*/

        if(Entry.Length > state->offset){
            offset += (sizeof(struct _TFFS_Entry) + state->offset);

            Len = min((unsigned int) (Entry.Length - state->offset), *read_length);
            ret = MTD_READ(ctx->active_mtd, offset, Len, &retlen, read_buffer);
            if(ret){
                return ret;
            }
            tffs_write_statistic(state->id, retlen, 0, 0);
            *read_length = retlen;
            state->offset += Len;
        } else {
            *read_length = 0;
        }

        DBG((KERN_INFO "daten kopiert\n"));
        return 0;
    }
    DBG((KERN_INFO "Eintrag %i nicht gefunden\n", Id));

    return -ENOENT;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
static int TFFS3_LGCY_Reindex(struct tffs_module *this)
{
    unsigned int ret;
    struct _TFFS_Entry Entry;
    loff_t offset;
    unsigned int Len;
    unsigned int i;
    struct tffs_lgcy_ctx *ctx;

    pr_err("[%s] Called\n", __func__);

    ctx = (struct tffs_lgcy_ctx *)this->priv;
    if(ctx == NULL){
        return -EBADF;
    }

    for(i = 0; i < FLASH_FS_ID_LAST; i++){
        TFFS_Global_Index[i] = -1;
    }
    offset = 0;

    DBG((KERN_INFO "TFFS_LGCY_Create_Index(): "));

    while(offset + sizeof(struct _TFFS_Entry) < ctx->active_mtd->size){
        ret = MTD_READ_HDR(ctx->active_mtd, offset, &Entry);
        if(ret){
            return ret;
        }

        tffs_write_statistic(FLASH_FS_ID_SKIP, sizeof(struct _TFFS_Entry), 0, 0); /*--- internal read ---*/

        if(Entry.ID >= (unsigned short) FLASH_FS_ID_LAST){ /*---  Ende gefunden ----*/
            DBG((KERN_INFO " [end found]\n"));
            return 0;
        }
        DBG((KERN_INFO " [<0x%x> %u bytes]\n", Entry.ID, Entry.Length));

        /*--- doppelter Eintrag gefunden, diesen Eintrag loeschen ---*/
        if(Entry.ID != FLASH_FS_ID_SKIP && TFFS_Global_Index[Entry.ID] >= 0){
            printk(" [<0x%x> %u bytes, cleared]\n", Entry.ID, Entry.Length);
            printk("offset = %#llx\n", offset);
            Entry.ID = FLASH_FS_ID_SKIP;
            ret = MTD_WRITE_HDR(ctx->active_mtd, offset, &Entry);
            if(ret){
                return ret;
            }

            tffs_write_statistic(FLASH_FS_ID_SKIP, sizeof(struct _TFFS_Entry), 1, 0); /*--- internal write ---*/
        }else{
            TFFS_Global_Index[Entry.ID] = offset;
        }

        Len = align_len(Entry.Length);
        offset += (sizeof(struct _TFFS_Entry) + Len);
    }
    DBG((KERN_INFO " [filesystem full]\n"));

    return 0;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
static int
Build_Cleanup_Buffer(struct tffs_lgcy_ctx *ctx, unsigned char *Cleanup_Buffer,
                    unsigned int *Cleanup_Buffer_Len, unsigned int *SkipCount)
{
    struct _TFFS_Entry Entry, *pEntry;
    loff_t offset;
    unsigned char *P;
    unsigned int Len, retlen, ret;

    pr_err("[%s] Called\n", __func__);

    offset = 0;
    P = Cleanup_Buffer;
    while(offset + sizeof(struct _TFFS_Entry) < ctx->active_mtd->size){
        ret = MTD_READ_HDR(ctx->active_mtd, offset, &Entry);
        if(ret){
            return ret;
        }

        tffs_write_statistic(FLASH_FS_ID_SKIP, sizeof(struct _TFFS_Entry), 0, 0); /*--- internal read ---*/

        if(Entry.ID >= (unsigned short) FLASH_FS_ID_LAST){ /*---  Ende gefunden ----*/
            pr_debug("[%s] [end found]\n", __func__);
            break;
        }

        Len = align_len(Entry.Length);

        if(Entry.ID != FLASH_FS_ID_SKIP){
            pEntry = (struct _TFFS_Entry *) P;
            pEntry->ID = cpu_to_be16(Entry.ID);
            pEntry->Length = cpu_to_be16(Entry.Length);

            P += sizeof(struct _TFFS_Entry);
            ret = MTD_READ(ctx->active_mtd,
                           offset + sizeof(struct _TFFS_Entry),
                           Entry.Length,
                           &retlen,
                           (unsigned char *) P);
            if(ret){
                return ret;
            }
            pr_debug("[%s] [<0x%x> %u bytes]\n", __func__, pEntry->ID, pEntry->Length);

            tffs_write_statistic(Entry.ID, retlen, 0, 0);

            P += Len;
        }else{
            (*SkipCount)++;
            pr_debug("[%s] [SKIP %u bytes]\n", __func__, pEntry->Length);
        }

        offset += (sizeof(struct _TFFS_Entry) + Len);
    }

    *Cleanup_Buffer_Len = P - Cleanup_Buffer;
    pr_debug("[%s] buffer_len %d\n", __func__, *Cleanup_Buffer_Len);

    return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int TFFS3_LGCY_Cleanup(struct tffs_module *this, void *handle)
{
    unsigned int retlen, ret;
    unsigned int SkipCount = 0, Cleanup_Buffer_Len = 0;
    struct tffs_mtd *other;
    struct tffs_lgcy_ctx *ctx;
    struct TFFS_LGCY_State *state;
    union _tffs_segment_entry *pu;
    union _tffs_segment_entry u;
    uint32_t current_number;

    pr_err("[%s] Called\n", __func__);

    ctx = (struct tffs_lgcy_ctx *)this->priv;
    if(ctx == NULL){
        return -EBADF;
    }

    state = (struct TFFS_LGCY_State *) handle;
    if(state->id != 0){
        return -EINVAL;
    }

    if(TFFS3_Cleanup_Buffer == NULL ){
        TFFS3_Cleanup_Buffer = vmalloc(ctx->active_mtd->size);
        if(TFFS3_Cleanup_Buffer == NULL ){
            pr_err("[%s] malloc(%u) failed\n", __func__, ctx->active_mtd->size);
            return -ENOMEM;
        }
    }

    /*------------------------------------------------------------------------------------------*\
     * gültige Daten zusammesuchen, tffs-index bestimmen und tffs löschen, wenn es notwendig ist
     \*------------------------------------------------------------------------------------------*/
    ret = Build_Cleanup_Buffer(ctx, TFFS3_Cleanup_Buffer, &Cleanup_Buffer_Len, &SkipCount);
    if(ret == 0 && SkipCount > 0){
        pr_debug("[%s] [%u data records skiped]\n", __func__, SkipCount);

        other = (ctx->active_mtd == &(ctx->avail_mtd[0])) ? &(ctx->avail_mtd[1]) : &(ctx->avail_mtd[0]);
        pr_debug("[%s] close old mtd, open %s\n", __func__, other->mtd->name);

        if(get_mtd_device_wrapped(other, other->mtd_idx)){
            panic("[%s] can't get mtd%u\n", __func__, other->mtd_idx);
            return -ENXIO;
        }


        pr_debug("[%s] formatting segment on %s\n", __func__, other->mtd->name);
        ret = TFFS3_LGCY_Format(other);
        if(ret){
            pr_err("[%s] format failed\n", __func__);
            return ret;
        }
    }else{
        pr_debug("[%s] no IDs skiped, leaving it as it is\n", __func__);
        return 0;
    }

    /*------------------------------------------------------------------------------------------*\
    \*------------------------------------------------------------------------------------------*/

    pu = (union _tffs_segment_entry *) TFFS3_Cleanup_Buffer;
    if(pu->Entry.ID != FLASH_FS_ID_SEGMENT){
        panic("TFFS_LGCY_Cleanup: flash segment %u file invalid\n", ctx->active_mtd->mtd_idx);
    }
    current_number = TFFS_GET_SEGMENT_VALUE(pu);
    DBG((KERN_INFO " [double buffer: read current number %u]\n", current_number));
    TFFS_SET_SEGMENT_VALUE(pu, 0);

    /*------------------------------------------------------------------------------------------*\
     * tffs neu schreiben
     \*------------------------------------------------------------------------------------------*/
    DBG((KERN_INFO " [write filesystem]\n"));
    ret = MTD_WRITE(other,
                    0,
                    Cleanup_Buffer_Len,
                    &retlen,
                    (unsigned char *) TFFS3_Cleanup_Buffer);
    if(ret){
        return ret;
    }

    tffs_write_statistic(FLASH_FS_ID_SKIP, retlen, 1, 0); /*--- internal write ---*/

    /*------------------------------------------------------------------------------------------*\
     * id setzen
     \*------------------------------------------------------------------------------------------*/
    u.Entry.ID = FLASH_FS_ID_SEGMENT;
    u.Entry.Length = sizeof(unsigned int);
    current_number++;
    DBG((KERN_INFO " [double buffer: write current number %u]\n", current_number));
    TFFS_SET_SEGMENT_VALUE(&u, current_number);
    ret = MTD_WRITE_HDR(other, 0, &u.Entry);
    if(ret){
        return ret;
    }

    tffs_write_statistic(FLASH_FS_ID_SKIP, sizeof(struct _TFFS_Entry), 1, 0); /*--- internal write ---*/

    /*------------------------------------------------------------------------------------------*\
     * alles fertig, tffs umsetzen
    \*------------------------------------------------------------------------------------------*/
    DBG((KERN_INFO "TFFS_LGCY_Cleanup: set mtd to %d\n", other));
    put_mtd_device_wrapped(ctx->active_mtd);
    ctx->active_mtd = other;

    DBG((KERN_INFO " [recreate index]\n"));
    TFFS3_LGCY_Reindex(this);

    DBG((KERN_INFO "TFFS_LGCY_Cleanup: success\n"));
    return 0;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
static int TFFS3_LGCY_Info(struct tffs_module *this, unsigned int *Fill)
{
    struct tffs_lgcy_ctx *ctx;
    unsigned int ret;
    struct _TFFS_Entry Entry;
    loff_t offset, max_offset;
    unsigned int Count;

    DBG((KERN_INFO "[%s] Called ", __func__));

    ctx = (struct tffs_lgcy_ctx *) this->priv;

    offset = 0;
    max_offset = 0;
    for(Count = 0; Count < FLASH_FS_ID_LAST; ++Count){
        offset = TFFS_Global_Index[Count];
        if(offset >= 0){
            DBG((KERN_INFO "offset = 0x%llx", offset));
            max_offset = max(offset, max_offset);
        }
    }
    if(max_offset == 0){
        *Fill = 0;
        return 0;
    }

    DBG((KERN_INFO "Header des hoechsten Eintrags lesen (0x%x)\n", Max));
    ret = MTD_READ_HDR(ctx->active_mtd, max_offset, &Entry);
    if(ret){
        return ret;
    }

    tffs_write_statistic(FLASH_FS_ID_SKIP, sizeof(struct _TFFS_Entry), 0, 0); /*--- internal read ---*/

    DBG((KERN_INFO " [<0x%x> %u bytes]\n", Entry.ID, Entry.Length));
    max_offset += Entry.Length;

    *Fill = ((int) max_offset * 100) / (unsigned int) (ctx->active_mtd->size);
    DBG((KERN_INFO "TFFS_LGCY_Info: Fill=%u%% success\n", *Fill));

    return 0;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
static int TFFS3_LGCY_Register_Notify(struct tffs_module *this, void *notify_priv, tffs3_notify_fn notify_cb)
{
    struct tffs_lgcy_ctx *ctx;
    int result;

    ctx = (struct tffs_lgcy_ctx *) this->priv;

    result = 0;
    if(ctx->notify_cb == NULL){
        ctx->notify_priv = notify_priv;
        ctx->notify_cb = notify_cb;
    } else {
        result = -EEXIST;
    }


    return result;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
static int TFFS3_LGCY_Remove_Notify(struct tffs_module *this, void *notify_priv, tffs3_notify_fn notify_cb)
{
    struct tffs_lgcy_ctx *ctx;
    int result;

    ctx = (struct tffs_lgcy_ctx *) this->priv;

    result = -EINVAL;
    if(ctx->notify_priv == notify_priv && ctx->notify_cb == notify_cb){
        ctx->notify_cb = NULL;
        ctx->notify_priv = NULL;
        result = 0;
    }

    return result;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
static int TFFS3_LGCY_Setup(struct tffs_module *this)
{
    struct tffs_lgcy_ctx *ctx;
    int ret;
    union _tffs_segment_entry u;
    int result;

    ctx = (struct tffs_lgcy_ctx *) this->priv;

    memset(&u, 0x0, sizeof(u));
    pr_err("[%s] Called\n", __func__);

    pr_err("[%s]mtd%u, mtd%u)\n", __func__, ctx->mtd_num[0], ctx->mtd_num[1]);

    result = get_mtd_device_wrapped(&(ctx->avail_mtd[0]), ctx->mtd_num[0]);
    if(result != 0){
        pr_err("[%s] can't get mtd%u\n", __func__, ctx->mtd_num[0]);
        return -ENODEV;
    }

    result = get_mtd_device_wrapped(&(ctx->avail_mtd[1]), ctx->mtd_num[1]);
    if(result != 0){
        pr_err("[%s] can't get mtd%u\n", __func__, ctx->mtd_num[1]);
        return -ENODEV;
    }

    /*------------------------------------------------------------------------------------------*\
     * prüfen welcher buffer der "richtige" ist
     \*------------------------------------------------------------------------------------------*/

    // TODO: potentieller Lesefehler wird nicht bearbeitet
    ret = MTD_READ_HDR(&(ctx->avail_mtd[0]), 0, &u.Entry);
    if(ret){
        pr_err("[%s] MTD read failed, could not read a complete _tffs_segment_entry from %s\n", __func__, ctx->avail_mtd[0].mtd->name);
    }

    tffs_write_statistic(u.Entry.ID, sizeof(struct _TFFS_Entry), 0, 0);

    if(u.Entry.ID == FLASH_FS_ID_SEGMENT){
        ctx->avail_mtd[0].segment_id = TFFS_GET_SEGMENT_VALUE(&u);
        pr_info("[%s] double_buffer(0): segment value %u\n", __func__, ctx->avail_mtd[0].segment_id);
    }else{
        pr_info("[%s] double_buffer(0): no SEGMENT VALUE (0x%x)\n", __func__, u.Entry.ID);
    }

    ret = MTD_READ_HDR(&(ctx->avail_mtd[1]), 0, &u.Entry);
    if(ret){
        pr_err("[%s] MTD read failed, could not read a complete _tffs_segment_entry from %s\n", __func__, ctx->avail_mtd[1].mtd->name);
    }

    tffs_write_statistic(u.Entry.ID, sizeof(struct _TFFS_Entry), 0, 0);

    if(u.Entry.ID == FLASH_FS_ID_SEGMENT){
        ctx->avail_mtd[1].segment_id = TFFS_GET_SEGMENT_VALUE(&u);
        pr_info("[%s] double_buffer(1): segment value %u\n", __func__, ctx->avail_mtd[1].segment_id);
    }else{
        pr_info("[%s] double_buffer(1): no SEGMENT VALUE (0x%x)\n", __func__, u.Entry.ID);
    }

    if(ctx->avail_mtd[0].segment_id == 0 && ctx->avail_mtd[1].segment_id == 0){
        panic("TFFS: no valid filesystem");
    }

    if(ctx->avail_mtd[0].segment_id > ctx->avail_mtd[1].segment_id){
        put_mtd_device_wrapped(&(ctx->avail_mtd[1]));
        ctx->active_mtd = &(ctx->avail_mtd[0]);
    } else {
        put_mtd_device_wrapped(&(ctx->avail_mtd[0]));
        ctx->active_mtd = &(ctx->avail_mtd[1]);
    }

    pr_info("[%s] double_buffer: use segment %u (avail: %u + %u)\n", __func__,
            ctx->active_mtd->segment_id, ctx->avail_mtd[0].segment_id, ctx->avail_mtd[1].segment_id);

    pr_info("[%s] mtd%u size=0x%llx\n", __func__, ctx->active_mtd->mtd_idx, ctx->active_mtd->mtd->size);

    return 0;
}

/*-----------------------------------------------------------------------------------------------*\
\*-----------------------------------------------------------------------------------------------*/
int TFFS3_LGCY_Configure(struct tffs_module *this, int mtd_num0, int mtd_num1)
{
    struct tffs_lgcy_ctx *ctx;
    int result;

    pr_err("[%s] Called\n", __func__);

    result = -EINVAL;

    ctx = kzalloc(sizeof(*ctx), GFP_KERNEL);
    if(ctx == NULL){
        pr_err("[%s] Out of memory error\n", __func__);
        result = -ENOMEM;
        goto err_out;
    }

    this->name = "legacy";
    this->setup = TFFS3_LGCY_Setup;
    this->open = TFFS3_LGCY_Open;
    this->close = TFFS3_LGCY_Close;
    this->read = TFFS3_LGCY_Read;
    this->write = TFFS3_LGCY_Write;
    this->cleanup = TFFS3_LGCY_Cleanup;
    this->reindex = TFFS3_LGCY_Reindex;
    this->info = TFFS3_LGCY_Info;
    this->register_notify = TFFS3_LGCY_Register_Notify;
    this->remove_notify = TFFS3_LGCY_Remove_Notify;

    ctx->mtd_num[0] = mtd_num0;
    ctx->mtd_num[1] = mtd_num1;
    sema_init(&ctx->lock, 1);

    this->priv = ctx;

    result = 0;

err_out:
    return result;
}
EXPORT_SYMBOL(TFFS3_LGCY_Configure);
