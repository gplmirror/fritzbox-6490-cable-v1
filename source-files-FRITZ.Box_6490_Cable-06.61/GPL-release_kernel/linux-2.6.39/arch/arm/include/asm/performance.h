#ifndef _arch_arm_include_asm_performance_h_
#define _arch_arm_include_asm_performance_h_


union __performance_monitor_control {
    struct _performance_monitor_control {
        unsigned int reserved1 : 4;   /* 31:28 */
        unsigned int EvtCount0 : 8; /* 27:20 */
        unsigned int EvtCount1 : 8; /* 19:12 */
        unsigned int EventBusEnable : 1;    /* 11 */
        unsigned int CycleCounterOverflow : 1;  /* 10 */
        unsigned int Counter0_Overflow : 1; /* 9 */
        unsigned int Counter1_Overflow : 1; /* 8 */
        unsigned int reserved2 : 1;   /* 7 */
        unsigned int CycleCounterIrqEnable : 1; /* 6 */
        unsigned int Counter0_IrqEnable : 1; /* 5 */
        unsigned int Counter1_IrqEnable : 1; /* 4 */
        unsigned int CycleCounterDivider : 1; /* 3 */
        unsigned int CycleCounterReset : 1; /* 2 */
        unsigned int CounterReset : 1; /* 2 */
        unsigned int EnableCounters : 1;
    } Bits;
    volatile unsigned int Register;
};


/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static inline void write_p15_performance_monitor_control(unsigned int value) {
    __asm__ __volatile__ ("   MCR p15, 0, %0, c15, c12, 0\n" : : "r" (value));
    return;
}

static inline void write_p15_cycle_counter(unsigned int value) {
    __asm__ __volatile__ ("   MCR p15, 0, %0, c15, c12, 1\n" : : "r" (value));
    return;
}

static inline void write_p15_performance_counter_0(unsigned int value) {
    __asm__ __volatile__ ("   MCR p15, 0, %0, c15, c12, 2\n" : : "r" (value));
    return;
}

static inline void write_p15_performance_counter_1(unsigned int value) {
    __asm__ __volatile__ ("   MCR p15, 0, %0, c15, c12, 3\n" : : "r" (value));
    return;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static inline unsigned int read_p15_performance_monitor_control(void) {
    unsigned int value;
    __asm__ __volatile__ ("   MRC p15, 0, %0, c15, c12, 0\n" : "=r" (value));
    return value;
}

static inline unsigned int read_p15_cycle_counter(void) {
    unsigned int value;
    __asm__ __volatile__ ("   MRC p15, 0, %0, c15, c12, 1\n" : "=r" (value));
    return value;
}

static inline unsigned int read_p15_performance_counter_0(void) {
    unsigned int value;
    __asm__ __volatile__ ("   MRC p15, 0, %0, c15, c12, 2\n" : "=r" (value));
    return value;
}

static inline unsigned int read_p15_performance_counter_1(void) {
    unsigned int value;
    __asm__ __volatile__ ("   MRC p15, 0, %0, c15, c12, 3\n" : "=r" (value));
    return value;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static inline void write_secure_debug_enable_register(unsigned int suiden, unsigned int suniden) {
    unsigned int value = (suiden << 0) | (suniden << 1);
#if 0
    unsigned int wert, i;
    __asm__ __volatile__ ("   MRC p15, 0, %0, c1, c0, 0\n" : "=r" (wert));
    for(i = 0 ; i < 13 ; i++) {
        printk(KERN_ERR "[%s] Coprozessor %d: %s\n", __FUNCTION__, i, 
                ((wert >> (i << 1)) & 0x3) == 0 ? "Access denied" :
                ((wert >> (i << 1)) & 0x3) == 0 ? "Privileged access only" :
                ((wert >> (i << 1)) & 0x3) == 0 ? "reserved" : "Privileged and User access ");
    }
#endif
    __asm__ __volatile__ ("   MCR p15, 0, %0, c1, c1, 1\n" : : "r" (value));
    __asm__ __volatile__ ("   MRC p15, 0, %0, c1, c1, 1\n" : "=r" (value));
    if((value & 0x3) != ((suiden << 0) | (suniden << 1))) {
        printk(KERN_ERR "[%s] write failed 0x%x (written) <> 0x%x (read)\n", __FUNCTION__, (suiden << 0) | (suniden << 1), value);
    }
}
















#endif /*--- #ifndef _arch_arm_include_asm_performance_h_ ---*/
