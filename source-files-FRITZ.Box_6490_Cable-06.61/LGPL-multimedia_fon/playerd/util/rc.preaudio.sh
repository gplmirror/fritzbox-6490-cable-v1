#! /bin/sh

case $1 in

start)
    if [ -z "`ls /var/tmp/ffmpeg_mp3.tables 2> /dev/null`" ] ; then
        if [ -z "`pidof playerd_tables`" ] ; then
            playerd_tables &
        fi
    fi
    ;;

stop)
    rm /var/tmp/ffmpeg_mp3.tables* 2 > /dev/null
    ;;

esac
