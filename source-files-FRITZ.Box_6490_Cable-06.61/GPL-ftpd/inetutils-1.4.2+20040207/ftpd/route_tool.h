

int route_tool_get_dev(unsigned long ipaddr/*hostorder*/, /*OUT*/unsigned *pdev, /*OUT*/unsigned long *pgateway, /*OUT*/unsigned long *pdst);

#ifdef USE_IPV6
int route_tool_get_ipv6_dev(unsigned char *ipv6addr /* 16 byte */, unsigned *pdev, /*OUT*/unsigned char *gateway6, /*OUT*/unsigned char *dst6);
#endif
