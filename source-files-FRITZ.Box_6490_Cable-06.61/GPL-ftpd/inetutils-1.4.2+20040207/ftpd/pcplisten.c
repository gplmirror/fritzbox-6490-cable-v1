
#include <stdio.h>
#include <unistd.h>
#include <netinet/in.h>

#include "extern.h"

#include "pcplisten.h"

#define PCPLISTEN "/bin/pcplisten"


int pcplisten_avialable(void)
{
	return 0 == access(PCPLISTEN, X_OK);
}

int pcplisten_start(/*INOUT*/struct sockaddr_storage *addr, int timeout_sec)
{
	FILE *fp;
	char buf[256];
	char ipstr[256];
	unsigned port;
#ifdef FTPD_DEBUG
	int ret = 0;
#endif /* FTPD_DEBUG */
	int result = -2;

	snprintf(buf, sizeof(buf), "%s tcp %s %u %u ftp-data", PCPLISTEN, sockaddr_addr2string(addr), sockaddr_port(addr), timeout_sec);
	buf[sizeof(buf)-1] = '\0';

	fp = popen(buf, "r");
	if (!fp) return -1;

	Log(("%s getting output", __FUNCTION__));
	if (buf == fgets(buf, sizeof(buf), fp)) {
		buf[sizeof(buf)-1] = '\0';
		Log(("%s got '%s'", __FUNCTION__, buf));
		if (2 == sscanf(buf, "OK: %s %u", ipstr, &port) && port > 0 && port <= 65535) {
			int family =  AF_INET;
			if (strchr(ipstr, ':')) family = AF_INET6;
			struct sockaddr_storage *ss = numericaladdr2sockaddr_storage(family, ipstr);
			if (ss) {
				switch(ss->ss_family) {
					case AF_INET:  ((struct sockaddr_in *)ss)->sin_port = htons(port);
#ifdef USE_IPV6
					case AF_INET6: ((struct sockaddr_in6 *)ss)->sin6_port = htons(port);
#endif
				}
				memcpy(addr, ss, sizeof(*addr));
				free(ss);
				result = 0;
			}
		}
	}

	Log(("%s pclose", __FUNCTION__));
#ifdef FTPD_DEBUG
	ret = pclose(fp);
#else /* !FTPD_DEBUG */
	(void)pclose(fp);
#endif /* !FTPD_DEBUG */
	Log(("%s pclose ret=%d", __FUNCTION__, ret));
	return result;
}
